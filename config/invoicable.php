<?php

return [
    'default_currency' => 'GHS',
    'default_status' => 'concept',
    'locale' => 'en',
];