<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),         // Your GitHub Client ID
        'client_secret' => env('GOOGLE_CLIENT_SECRET'), // Your GitHub Client Secret
        'redirect' => 'http://careersearch.local/login/google/callback',
    ],
    'facebook' => [
        'client_id' => '540459942987598',         // Your GitHub Client ID
        'client_secret' => '354d40a1e0aabce58e6632f406b74e81', // Your GitHub Client Secret
        'redirect' => 'https://careersearch.local/login/facebook/callback',
    ],
    'twitter' => [
        'client_id' => 'Lir7nfmYVLNKMFvDNiGOup6Zs',         // Your GitHub Client ID
        'client_secret' => 'nLY3hZmtQdJTg3wnSvZdCkA2vk8RA6rBATTPZ0YyEKcZX4AljC', // Your GitHub Client Secret
        'redirect' => 'https://careersearch.local/login/twitter/callback',
    ],

];
