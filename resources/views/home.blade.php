@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>{{auth()->user()->lastname}}, {{auth()->user()->firstname}}!</h3>
                <span>We are glad to see you again!</span>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Dashboard</li>
                    </ul>
                </nav>
            </div>

            <!-- Fun Facts Container -->
            <div class="fun-facts-container">
                @role('employee')
                <div class="fun-fact" data-fun-fact-color="#36bd78">
                    <div class="fun-fact-text">
                        <span>Task Bids Won</span>
                        <h4>0</h4>
                    </div>
                    <div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>
                </div>
                <div class="fun-fact" data-fun-fact-color="#b81b7f">
                    <div class="fun-fact-text">
                        <span>Jobs Applied</span>
                        <h4>{{count(auth()->user()->applications)}}</h4>
                    </div>
                    <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
                </div>
                @endrole
                @role('employer')
                <div class="fun-fact" data-fun-fact-color="#36bd78">
                    <div class="fun-fact-text">
                        <span>Task Posted</span>
                        <h4>{{count(auth()->user()->projects)}}</h4>
                    </div>
                    <div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>
                </div>
                <div class="fun-fact" data-fun-fact-color="#b81b7f">
                    <div class="fun-fact-text">
                        <span>Jobs Posted</span>
                        <h4>{{count(auth()->user()->works)}}</h4>
                    </div>
                    <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
                </div>
                @endrole

                <div class="fun-fact" data-fun-fact-color="#efa80f">
                    <div class="fun-fact-text">
                        <span>Reviews</span>
                        <h4>{{count(auth()->user()->reviewcompany)}}</h4>
                    </div>
                    <div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
                </div>

                <!-- Last one has to be hidden below 1600px, sorry :( -->
                <div class="fun-fact" data-fun-fact-color="#2a41e6">
                    <div class="fun-fact-text">
                        <span>This Month Views</span>
                        <h4>{{$month_views}}</h4>
                    </div>
                    <div class="fun-fact-icon"><i class="icon-feather-trending-up"></i></div>
                </div>
            </div>

            <!-- Row -->
            <div class="row">
                <dashboard-profile-component months="{{$months}}" counts="{{$counts}}"></dashboard-profile-component>
                <dashboard-user-notes user-notes="{{$notes}}"></dashboard-user-notes>
            </div>
            <!-- Row / End -->

            <!-- Row -->
            {{--notifications--}}
            <div class="row">

                <!-- Dashboard Box -->
                <dashboard-notifications-component user="{{auth()->user()}}" notifications="{{auth()->user()->unreadNotifications}}"></dashboard-notifications-component>
                <!-- Dashboard Box -->
                <div class="col-xl-6">
                    <div class="dashboard-box">
                        <div class="headline">
                            <h3><i class="icon-material-outline-assignment"></i> Subscriptions</h3>
                        </div>
                        <div class="content">
                            <ul class="dashboard-box-list">
                                @if(count($subscriptions) > 0)
                                    @foreach($subscriptions as $subscription)
                                        <li>
                                            <div class="invoice-list-item">
                                                <strong>{{$subscription->plan->name}}</strong>
                                                <ul>
                                                    <li>
                                                        @if($subscription->expired)
                                                            <span class="unpaid">Expired</span>
                                                        @else
                                                            <span class="paid">Active</span>
                                                        @endif
                                                    </li>
                                                    <li>Invoice: {{$subscription->invoices[0]->reference}}</li>
                                                </ul>
                                            </div>
                                            <!-- Buttons -->
                                            <div class="buttons-to-right">
                                                <a href="{{route('invoice.all.details',['ref'=>$subscription->invoices[0]->reference])}}" class="button gray">View Invoice</a>
                                            </div>
                                        </li>
                                    @endforeach
                                    @else
                                    <li>
                                        <div class="invoice-list-item">
                                            <strong>No Subscriptions</strong>
                                        </div>
                                    </li>
                                    @endif
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
            @include('partials.auth.footer')
            <!-- Footer / End -->

        </div>
    </div>


@endsection