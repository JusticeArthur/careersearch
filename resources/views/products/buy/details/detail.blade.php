@extends('layouts.single')

@section('content')
    <div class="single-page-header" data-background-image="/images/single-job.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            @if(count($product->avatars) > 0)
                            <div class="header-image"><a href=""><img  src="{{$product->avatars[0]['name']}}" alt=""></a></div>
                            @endif
                            <div class="header-details">
                                <h3>{{$product->title}}</h3>
                                <h5>About the Product</h5>
                                <ul>
                                    <li><a href="#"><i class="icon-material-outline-business"></i> {{$product->negotiable?'Negotiable':''}}</a></li>
                                    <li><a href="#"><i class="icon-brand-skyatlas"></i> {{$product->condition === 'used'?'Used':'New'}}</a></li>
                                    <li><img  class="flag" src="/images/flags/gh.svg" alt=""> {{$product->country->name}}</li>
                                    <li><div class="verified-badge-with-title">Verified</div></li>
                                </ul>
                            </div>
                        </div>
                        <div class="right-side">
                            <div class="salary-box">
                                <div class="salary-type">Price</div>
                                <div class="salary-amount">{{$product->price}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">Product Description</h3>
                    <h4>Images</h4>
                    <div class="row">
                        @foreach($product->avatars as $avatar)
                        <div class="col-sm-4">
                            <div class="avatar-wrapper" data-tippy-placement="bottom" title="{{$product->title}}">
                                <img class="profile-pic" src="{{$avatar->name}}" alt="" />
                            </div>
                        </div>
                            @endforeach
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-md-4">
                            <h4>Contacts</h4>
                            <div class="numbered color filled">
                                <ol>
                                    @foreach($product->user->contacts as $contact)
                                    <li>{{$contact->contact}}</li>
                                        @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-page-section">
                    <h3 class="margin-bottom-30">Location</h3>
                    <div id="single-job-map-container">
                        <div id="singleListingMap" data-latitude="{{$product->lat}}" data-longitude="{{$product->lng}}" data-map-icon="im im-icon-Hamburger"></div>
                        <a href="#" id="streetView">Street View</a>
                    </div>
                </div>
                @if(count($similar) > 0)
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">Similar Products</h3>

                    <!-- Listings Container -->
                    <div class="listings-container grid-layout">
                        <!-- Job Listing -->
                        @foreach($similar as $item)
                            <a href="{{route('product.buy.spec',['slug'=>$item->slug])}}" class="job-listing">

                            <!-- Job Listing Details -->
                            <div class="job-listing-details">
                                <!-- Logo -->
                                <div class="job-listing-company-logo">
                                    @if(count($item->avatars) > 0)
                                       <img  src="{{$item->avatars[0]['name']}}" alt="{{$product->title}}">
                                    @else
                                        <img src="/images/company-logo-02.png" alt="{{$product->title}}">
                                    @endif
                                </div>

                                <!-- Details -->
                                <div class="job-listing-description">
                                    <h4 class="job-listing-company">{{$item->user->name}}</h4>
                                    <h3 class="job-listing-title">{{$item->title}}</h3>
                                </div>
                            </div>

                            <!-- Job Listing Footer -->
                            <div class="job-listing-footer">
                                <ul>
                                    <li><i class="icon-material-outline-location-on"></i> {{$item->location}}</li>
                                    @if($item->negotiable)
                                    <li><i class="icon-material-outline-business-center"></i> {{$item->negotiable?'Negotiable':''}}</li>
                                    @endif
                                    <li><i class="icon-material-outline-account-balance-wallet"></i> {{$item->price}}</li>
                                    <li><i class="icon-material-outline-access-time"></i> {{$item->human_diff}}</li>
                                </ul>
                            </div>
                        </a>
                        @endforeach
                    </div>
                    <!-- Listings Container / End -->

                </div>
                @endif
            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <product-buy-detail-components product="{{$product}}"></product-buy-detail-components>
            </div>

        </div>
    </div>
@endsection