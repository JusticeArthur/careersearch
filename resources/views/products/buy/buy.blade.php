@extends('layouts.dashboard')

@section('content')
    <buy-product-component log="{{$log}}" brands="{{$brands}}" categories="{{$categories}}" products="{{$products}}" setting="{{$settings}}">
    </buy-product-component>
@endsection