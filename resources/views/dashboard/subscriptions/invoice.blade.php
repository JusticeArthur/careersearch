<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Hireo Invoice</title>
    <link rel="stylesheet" href="{{asset('css/invoice.css')}}">
    <style>
        #logo img{
            max-height: unset;
        }
    </style>
</head>

<body>

<!-- Print Button -->
<div class="print-button-container">
    <a href="javascript:window.print()" class="print-button">Print this invoice</a>
</div>

<!-- Invoice -->
<div id="invoice">

    <!-- Header -->
    <div class="row">
        <div class="col-xl-6">
            <div id="logo"><img style="height: 65px !important;" src="https://drive.google.com/uc?export=view&id=1mjqQpSTNffw5_VLtY49929zzaXTQfseC" alt=""></div>
        </div>

        <div class="col-xl-6">

            <p id="details">
                <strong>Order:</strong> {{$invoice->reference}} <br>
                <strong>Issued:</strong> {{\Carbon\Carbon::parse($invoice->created_at)->toFormattedDateString()}}
            </p>
        </div>
    </div>


    <!-- Client & Supplier -->
    <div class="row">
        <div class="col-xl-12">
            <h2>Invoice</h2>
        </div>

        <div class="col-xl-6">
            <strong class="margin-bottom-5">Supplier</strong>
            <p>
                Careersearch Ltd. <br>
                P.O. Box 23 <br>
                +233243742088 <br>
                Kumasi, Ashanti Region, Ghana <br>
            </p>
        </div>

        <div class="col-xl-6">
            <strong class="margin-bottom-5">Customer</strong>
            <p>
                {{$user->name}} <br>
                {{$user->profile->mobile}} <br>
                {{$user->email}} <br>
                {{$user->profile->location}}
            </p>
        </div>
    </div>


    <!-- Invoice -->
    <div class="row">
        <div class="col-xl-12">
            <table class="margin-top-20">
                <tr>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Charges</th>
                    <th>Total</th>
                </tr>

                <tr>
                    <td>{{$plan->name}}</td>
                    @if(strtolower($subscription->plan_type) == 'month')
                        <td>GHS {{number_format($plan->monthly_amount,2)}}</td>
                        <td>GHS {{number_format(((float)$invoice->total - (float)$plan->monthly_amount),2)}}</td>
                        @else
                        <td>GHS {{number_format($plan->yearly_amount,2)}}</td>
                        <td>GHS {{number_format(((float)$invoice->total - (float)$plan->yearly_amount),2)}}</td>
                    @endif
                    <td>GHS {{number_format($invoice->total,2)}}</td>
                </tr>
            </table>
        </div>

        <div class="col-xl-4 col-xl-offset-8">
            <table id="totals">
                <tr>
                    <th>Total Payment</th>
                    <th><span>GHS {{number_format($invoice->total,2)}}</span></th>
                </tr>
            </table>
        </div>
    </div>


    <!-- Footer -->
    <div class="row">
        <div class="col-xl-12">
            <ul id="footer">
                <li><span>www.careersearch.com</span></li>
                <li>careersearch@tecunitgh.com</li>
                <li>(233) 24-374-2088</li>
            </ul>
        </div>
    </div>

</div>


</body>
</html>