@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <div class="row">
                <div class="col-md-12">

                    <div class="order-confirmation-page">
                        <div class="breathing-icon"><i class="icon-feather-check"></i></div>
                        <h2 class="margin-top-30">Thank you for your order!</h2>
                        <p>Your payment has been processed successfully.</p>
                        <a href="{{route('subscription.invoice',['slug'=>$slug,'type'=>$type])}}" class="button ripple-effect-dark button-sliding-icon margin-top-30">
                            View Invoice
                            <i class="icon-material-outline-arrow-right-alt">

                            </i>
                        </a>
                    </div>

                </div>
            </div>

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
        <!-- Footer / End -->

        </div>
    </div>
@endsection
@section('dialogs')
    <div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
            </ul>

            <div class="popup-tabs-container">

                <!-- Tab -->
                <div class="popup-tab-content" id="tab1">

                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>Change Review</h3>
                        <span>Rate <a href="#">Herman Ewout</a> for the project <a href="#">WordPress Theme Installation</a> </span>
                    </div>

                    <!-- Form -->
                    <form method="post" id="change-review-form">

                        <div class="feedback-yes-no">
                            <strong>Was this delivered on budget?</strong>
                            <div class="radio">
                                <input id="radio-rating-1" name="radio" type="radio" checked>
                                <label for="radio-rating-1"><span class="radio-label"></span> Yes</label>
                            </div>

                            <div class="radio">
                                <input id="radio-rating-2" name="radio" type="radio">
                                <label for="radio-rating-2"><span class="radio-label"></span> No</label>
                            </div>
                        </div>

                        <div class="feedback-yes-no">
                            <strong>Was this delivered on time?</strong>
                            <div class="radio">
                                <input id="radio-rating-3" name="radio2" type="radio" checked>
                                <label for="radio-rating-3"><span class="radio-label"></span> Yes</label>
                            </div>

                            <div class="radio">
                                <input id="radio-rating-4" name="radio2" type="radio">
                                <label for="radio-rating-4"><span class="radio-label"></span> No</label>
                            </div>
                        </div>

                        <div class="feedback-yes-no">
                            <strong>Your Rating</strong>
                            <div class="leave-rating">
                                <input type="radio" name="rating" id="rating-1" value="1" checked/>
                                <label for="rating-1" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-2" value="2"/>
                                <label for="rating-2" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-3" value="3"/>
                                <label for="rating-3" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-4" value="4"/>
                                <label for="rating-4" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-5" value="5"/>
                                <label for="rating-5" class="icon-material-outline-star"></label>
                            </div><div class="clearfix"></div>
                        </div>

                        <textarea class="with-border" placeholder="Comment" name="message" id="message" cols="7" required>Excellent programmer - helped me fixing small issue.</textarea>

                    </form>

                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="change-review-form">Save Changes <i class="icon-material-outline-arrow-right-alt"></i></button>

                </div>

            </div>
        </div>
    </div>
    <!-- Edit Review Popup / End -->


    <!-- Leave a Review for Freelancer Popup
    ================================================== -->
    <div id="small-dialog-2" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
            </ul>

            <div class="popup-tabs-container">

                <!-- Tab -->
                <div class="popup-tab-content" id="tab2">

                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>Leave a Review</h3>
                        <span>Rate <a href="#">Peter Valentín</a> for the project <a href="#">Simple Chrome Extension</a> </span>
                    </div>

                    <!-- Form -->
                    <form method="post" id="leave-review-form">

                        <div class="feedback-yes-no">
                            <strong>Was this delivered on budget?</strong>
                            <div class="radio">
                                <input id="radio-1" name="radio" type="radio" required>
                                <label for="radio-1"><span class="radio-label"></span> Yes</label>
                            </div>

                            <div class="radio">
                                <input id="radio-2" name="radio" type="radio" required>
                                <label for="radio-2"><span class="radio-label"></span> No</label>
                            </div>
                        </div>

                        <div class="feedback-yes-no">
                            <strong>Was this delivered on time?</strong>
                            <div class="radio">
                                <input id="radio-3" name="radio2" type="radio" required>
                                <label for="radio-3"><span class="radio-label"></span> Yes</label>
                            </div>

                            <div class="radio">
                                <input id="radio-4" name="radio2" type="radio" required>
                                <label for="radio-4"><span class="radio-label"></span> No</label>
                            </div>
                        </div>

                        <div class="feedback-yes-no">
                            <strong>Your Rating</strong>
                            <div class="leave-rating">
                                <input type="radio" name="rating" id="rating-radio-1" value="1" required>
                                <label for="rating-radio-1" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-radio-2" value="2" required>
                                <label for="rating-radio-2" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-radio-3" value="3" required>
                                <label for="rating-radio-3" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-radio-4" value="4" required>
                                <label for="rating-radio-4" class="icon-material-outline-star"></label>
                                <input type="radio" name="rating" id="rating-radio-5" value="5" required>
                                <label for="rating-radio-5" class="icon-material-outline-star"></label>
                            </div><div class="clearfix"></div>
                        </div>

                        <textarea class="with-border" placeholder="Comment" name="message2" id="message2" cols="7" required></textarea>

                    </form>

                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="leave-review-form">Leave a Review <i class="icon-material-outline-arrow-right-alt"></i></button>

                </div>

            </div>
        </div>
    </div>
@endsection