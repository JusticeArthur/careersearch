@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Messages</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Messages</li>
                    </ul>
                </nav>
            </div>

            <div class="messages-container margin-top-0">
                <real-time-chat-component chat="{{$chat}}" today="{{$today}}" audio="{{$audio}}" users="{{$users}}" authentic="{{$currentUser}}"></real-time-chat-component>
            </div>
            <!-- Messages Container / End -->




            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
            <!-- Footer / End -->

        </div>
    </div>
@endsection