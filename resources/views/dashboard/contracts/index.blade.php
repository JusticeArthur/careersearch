@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Contract Offers</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Contract Offers</li>
                    </ul>
                </nav>
            </div>

            <div class="row">
                <!-- Dashboard Box -->
                <contracts-index-component contracts="{{$contracts}}"></contracts-index-component>
            </div>
            <!-- Row -->
            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
        <!-- Footer / End -->

        </div>
    </div>
@endsection