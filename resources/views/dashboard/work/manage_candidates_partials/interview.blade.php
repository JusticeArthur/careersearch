<!-- Headline -->
<div class="headline">
    <h3>
        <i class="icon-material-outline-supervisor-account"></i>
        @php
            $count = 0;
            foreach($jobs->applications as $item){
                if(($item->status == 'interviewed' && $item->accepted) || $item->status == 'employed')
                    $count++;
            }
            echo $count;
        @endphp
        Candidate(s)
    </h3>
</div>

<div class="content ">
    <div>
        <ul class="dashboard-box-list">
            @foreach($jobs->applications as $job)
                @if(($job->status == 'interviewed' && $job->accepted) || $job->status == 'employed')
                    <li>
                        <!-- Overview -->
                        <div class="freelancer-overview manage-candidates">
                            <div class="freelancer-overview-inner">

                                <!-- Avatar -->
                                <div class="freelancer-avatar">
                                    <div class="verified-badge"></div>
                                    @if($job->user->profile->avatar != null)
                                        <a href="{{route('profile.user.in',['slug'=>$job->user->profile->url])}}">
                                            <img src="/avatars/profile/{{$job->user->profile->avatar}}" alt="">
                                        </a>
                                    @else
                                        <a href="{{route('profile.user.in',['slug'=>$job->user->profile->url])}}"><img src="/images/user-avatar-placeholder.png" alt=""></a>
                                    @endif
                                </div>

                                <!-- Name -->
                                <div class="freelancer-name">
                                    <h4><a href="#">{{$job->user->firstname}} {{$job->user->lastname}} <img class="flag" src="/images/flags/gh.svg" alt="" title="Ghana" data-tippy-placement="top"></a></h4>

                                    <!-- Details -->
                                    <span class="freelancer-detail-item"><a href="mailto: {{$job->user->email}}"><i class="icon-feather-mail"></i> {{$job->user->email}}</a></span>
                                    <span class="freelancer-detail-item"><i class="icon-feather-phone"></i> {{$job->user->profile->mobile}}</span>

                                    <!-- Rating -->
                                    <div class="freelancer-rating">
                                        <div class="star-rating" data-rating="{{$job->user->profile->overall_rating}}"></div>
                                    </div>
                                    <!-- Buttons -->
                                    <div class="always-visible margin-top-25 margin-bottom-5">
                                        <a href="{{route('candidate.application.details',['job_slug'=>$jobs->slug,'applicant_slug'=>$job->user->profile->url])}}" class="button ripple-effect" style="text-decoration:none"><i class="icon-feather-file-text"></i> Application Details</a>
                                        <a href="#small-dialog" class="popup-with-zoom-anim button dark ripple-effect"><i class="icon-feather-mail"></i> Send Message</a>
                                        <a href="#" class="button gray ripple-effect ico" title="Remove Candidate" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>