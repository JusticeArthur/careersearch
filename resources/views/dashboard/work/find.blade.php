@extends('layouts.dashboard')

@section('content')
    <filter-jobs categories="{{$categories}}" tags="{{$tags}}" jobs="{{$jobs}}" center="{{$center}}" markers="{{$markers}}"></filter-jobs>
@endsection