@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Interview Invite</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Jobs</a></li>
                        <li>Interview Invite</li>
                    </ul>
                </nav>
            </div>

            <!-- Row -->
            <apply-interview job="{{$job_slug}}" applicant="{{$applicant_slug}}"></apply-interview>
            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
            <!-- Footer / End -->

        </div>
    </div>

@endsection