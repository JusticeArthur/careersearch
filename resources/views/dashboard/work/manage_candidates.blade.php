@extends('layouts.dashboard')
@push('custom_css')
    <style>
        a{
            text-decoration: none !important;
        }
    </style>
    <link rel="stylesheet" href="{{asset('bootstap/css/bootstrap.min.css')}}">
    @endpush
@push('custom_js')
    @include('partials.alert')
    <script src="{{asset('bootstap/js/bootstrap.min.js')}}"></script>
    @endpush
@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Manage Candidates</h3>
                <span class="margin-top-7">Job Applications for <a href="#">{{$jobs->title}}</a></span>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Manage Candidates</li>
                    </ul>
                </nav>
            </div>

            <div class="row">

                <div class="col-xl-12">
                    <!-- Tabs Container -->
                    <div class="tabs">
                        <div class="tabs-header">
                            <ul>
                                <li class="active"><a href="#tab-1" data-tab-id="1">Applicants</a></li>
                                <li><a href="#tab-2" data-tab-id="2">Shortlisted</a></li>
                                <li><a href="#tab-3" data-tab-id="3">Interview</a></li>
                                <li><a href="#tab-4" data-tab-id="3">Employment</a></li>
                            </ul>
                            <div class="tab-hover"></div>
                            <nav class="tabs-nav">
                                <span class="tab-prev"><i class="icon-material-outline-keyboard-arrow-left"></i></span>
                                <span class="tab-next"><i class="icon-material-outline-keyboard-arrow-right"></i></span>
                            </nav>
                        </div>
                        <!-- Tab Content -->
                        <div class="tabs-content">
                            <div class="tab active" data-tab-id="1">
                                <div class="dashboard-box margin-top-0">
                                    @include('dashboard.work.manage_candidates_partials.application')
                                </div>
                            </div>
                            <div class="tab" data-tab-id="2">
                                <div class="dashboard-box margin-top-0">
                                    @include('dashboard.work.manage_candidates_partials.shorlisted')
                                </div>
                            </div>
                            <div class="tab" data-tab-id="3">
                                <div class="dashboard-box margin-top-0">
                                    @include('dashboard.work.manage_candidates_partials.interview')
                                </div>
                            </div>
                            <div class="tab" data-tab-id="4">
                                <div class="dashboard-box margin-top-0">
                                    @include('dashboard.work.manage_candidates_partials.employed')
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Tabs Container / End -->
                </div>
            </div>
                <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
            <!-- Footer / End -->

        </div>
    </div>
@endsection
@section('dialogs')
    <div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
                <li><a href="#tab">Send Message</a></li>
            </ul>

            <div class="popup-tabs-container">

                <!-- Tab -->
                <div class="popup-tab-content" id="tab">

                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>Direct Message To Sindy</h3>
                    </div>

                    <!-- Form -->
                    <form method="post" id="send-pm">
                        <textarea name="textarea" cols="10" placeholder="Message" class="with-border" required></textarea>
                    </form>

                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="send-pm">Send <i class="icon-material-outline-arrow-right-alt"></i></button>

                </div>

            </div>
        </div>
    </div>
    @endsection