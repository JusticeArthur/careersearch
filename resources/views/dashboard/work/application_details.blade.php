@extends('layouts.single')
@push('custom_js')
    @include('partials.alert')
    @endpush
@section('content')
    <div class="single-page-header freelancer-header" data-background-image="/images/single-freelancer.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image freelancer-avatar">
                                @if($profile->avatar != null)
                                    <img src="/avatars/profile/{{$profile->avatar}}" alt="">
                                @else
                                    <img src="/images/user-avatar-big-02.jpg" alt="">
                                @endif
                            </div>
                            <div class="header-details">
                                <h3>{{$profile->user->firstname}} {{$profile->user->lastname}} {{--<span>iOS Expert + Node Dev</span>--}}</h3>
                                <ul>
                                    <li><div class="star-rating" data-rating="{{$profile->overall_rating}}"></div></li>
                                    <li><img class="flag" src="/images/flags/gh.svg" alt=""> {{\App\Utilities\Utils::formatLocation($profile->location)}}</li>
                                    <li><div class="verified-badge-with-title">Verified</div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <!-- Page Content -->
                @if(isset($profile->introduction) && $profile->introduction != null)
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">About Me</h3>
                    <p>{!! $profile->introduction !!}</p>
                </div>
                @endif


            <!-- Boxed List -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-thumb-up"></i> Employment History and Feedback</h3>
                    </div>
                    <ul class="boxed-list-ul">
                        @if(count(json_decode($employment_history,true)) > 0)
                        @foreach(json_decode($employment_history,true) as $item)
                            <li>
                                <div class="boxed-list-item">
                                    <!-- Content -->
                                    <div class="item-content">
                                        <h4>{{$item['title']}}</h4>
                                        <div class="item-details margin-top-10">
                                            <div class="star-rating" data-rating="{{$item['rating']}}"></div>
                                            <div class="detail-item"><i class="icon-material-outline-date-range"></i> {{$item['date']}}</div>
                                        </div>
                                        <div class="item-description">
                                            <p>{{$item['comment']}} </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                            @else
                            <li>
                                <div class="boxed-list-item">
                                    <!-- Content -->
                                    <div class="item-content">
                                        <h4>No Employment History</h4>
                                    </div>
                                </div>
                            </li>
                            @endif
                    </ul>

                    {{--<!-- Pagination -->
                    <div class="clearfix"></div>
                    <div class="pagination-container margin-top-40 margin-bottom-10">
                        <nav class="pagination">
                            <ul>
                                <li><a href="#" class="ripple-effect current-page">1</a></li>
                                <li><a href="#" class="ripple-effect">2</a></li>
                                <li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="clearfix"></div>
                    <!-- Pagination / End -->--}}

                </div>
                <!-- Boxed List / End -->

            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">

                    <!-- Profile Overview -->
                      @if($applied->status == 'sent')
                        <a href="{{route('candidate.application.details.apply',['job_slug'=>$job_slug,'applicant_slug'=>$profile->url])}}" class="apply-now-button">Accept <i class="icon-material-outline-check"></i></a>
                          @elseif($applied->status == 'shortlisted')
                        <a href="{{route('candidate.application.details.interview',['job_slug'=>$job_slug,'applicant_slug'=>$profile->url])}}" class="apply-now-button">Invite for Interview <i class="icon-material-outline-check"></i></a>
                        @elseif($applied->status == 'interviewed' && $applied->accepted)
                        <a href="#small-dialog" class="popup-with-zoom-anim button apply-now-button">Negotiate Contract <i class="icon-material-outline-thumb-up"></i></a>
                        <a href="#" class="button apply-now-button" style="background-color: #b94a48">Sorry Next time <i class="icon-material-outline-thumb-down"></i></a>
                        @elseif($applied->status == 'employed')
                        <a href="#"  class="button apply-now-button">Application Complete</a>

                @endif
                    <!-- Button -->
                    <a href="{{$profile->cv != null ? route('profile.cv.download',['id'=>$profile->user->id]):''}}" class="apply-now-button">Download CV<i class="icon-feather-download"></i></a>

                    <!-- Freelancer Indicators -->
                    <div class="sidebar-widget">
                        <div class="freelancer-indicators">

                            <!-- Indicator -->
                            <div class="indicator">
                                <strong>{{$profile->job_success}}%</strong>
                                <div class="indicator-bar" data-indicator-percentage="{{$profile->job_success}}"><span></span></div>
                                <span>Job Success</span>
                            </div>

                            <!-- Indicator -->
                            <div class="indicator">
                                <strong>{{$profile->recommendation}}%</strong>
                                <div class="indicator-bar" data-indicator-percentage="{{$profile->recommendation}}"><span></span></div>
                                <span>Recommendation</span>
                            </div>

                        </div>
                    </div>

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Skills</h3>
                        @if(count($profile->skills) > 0)
                            <div class="task-tags">
                                @foreach($profile->skills as $skill)
                                    <span>{{$skill->skill}}</span>
                                @endforeach
                            </div>
                            @else
                            <strong>No Skills Available</strong>
                            @endif
                    </div>
                    <div class="sidebar-widget">
                        <h3>Bookmark or Share</h3>

                        <!-- Bookmark Button -->
                        <button class="bookmark-button margin-bottom-25">
                            <span class="bookmark-icon"></span>
                            <span class="bookmark-text">Bookmark</span>
                            <span class="bookmarked-text">Bookmarked</span>
                        </button>

                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>Interesting? <strong>Share It!</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                    <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <!-- Spacer -->
    <div class="margin-top-15"></div>
    <!-- Spacer / End-->
    <send-contract-component employer="{{$work->user->id}}" employee="{{$profile->user->id}}" work="{{$work->id}}"></send-contract-component>

@endsection


@section('dialogs')

    @endsection

