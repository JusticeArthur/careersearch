@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Manage Jobs</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Manage Jobs</li>
                    </ul>
                </nav>
            </div>

            <!-- Row -->
            <div class="row">
                <!-- Dashboard Box -->
                        <manage-jobs data="{{$jobs}}"></manage-jobs>
                </div>

            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
        <!-- Footer / End -->

        </div>
    </div>
@endsection