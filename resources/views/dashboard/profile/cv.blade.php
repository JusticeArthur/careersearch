@extends('layouts.dashboard')

@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Interview Postpones</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Interview Postpone</li>
                    </ul>
                </nav>
            </div>

            <!-- Row -->
           {{-- <profile-page-component></profile-page-component>--}}
            <form action="{{route('my.cv.parse')}}" enctype="multipart/form-data" method="post">
                <div class="input-group">
                    <input name="avatar" type="file" class="form-control">
                    <button class="btn btn-primary" type="submit">Go</button>
                </div>
                {{csrf_field()}}
            </form>
            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
        @include('partials.auth.footer')
        <!-- Footer / End -->

        </div>
    </div>
@endsection