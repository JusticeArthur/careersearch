@extends('layouts.dashboard')

@section('content')
    
<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >
        
        <!-- Dashboard Headline -->
        <div class="dashboard-headline">
            <h3>My Active Bids</h3>

            <!-- Breadcrumbs -->
            <nav id="breadcrumbs" class="dark">
                <ul>
                    <li><a href="#">Home</a></li>
                <li><a href="#">Dashboard</a></li>
                    <li>My Active Bids</li>
                </ul>
            </nav>
        </div>

        <!-- Row -->
    <active-bids application="{{$application}}"></active-bids>
        <!-- Row / End -->

        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
    @include('partials.auth.footer')
        <!-- Footer / End -->

    </div>
</div>

@endsection