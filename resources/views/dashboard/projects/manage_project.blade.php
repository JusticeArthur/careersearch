@extends('layouts.dashboard')

@section('content')

<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >
        
        <!-- Dashboard Headline -->
        <div class="dashboard-headline">
            <h3>Manage Tasks</h3>

            <!-- Breadcrumbs -->
            <nav id="breadcrumbs" class="dark">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Dashboard</a></li>
                    <li>Manage Tasks</li>
                </ul>
            </nav>
        </div>

        <!-- Row -->
    <manage-task project="{{$project}}"></manage-task>
        <!-- Row / End -->

        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
    @include('partials.auth.footer')
        <!-- Footer / End -->

    </div>
</div>    

@endsection