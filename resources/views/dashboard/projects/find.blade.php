@extends('layouts.single')

@section('content')
<filter-projects project="{{$project}}" jobcategory="{{$category}}"></filter-projects>
@endsection