@extends('layouts.dashboard')

@section('content')
   
<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >
        
        <!-- Dashboard Headline -->
        <div class="dashboard-headline">
            <h3>Manage Bidders</h3>
        <span class="margin-top-7">Bids for <a href="#">{{$project->name}}</a></span>

            <!-- Breadcrumbs -->
            <nav id="breadcrumbs" class="dark">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Dashboard</a></li>
                    <li>Manage Bidders</li>
                </ul>
            </nav>
        </div>

        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                    <h3><i class="icon-material-outline-supervisor-account"></i>{{$project->applications->count()}} Bidders</h3>
                        <div class="sort-by">
                            <select class="selectpicker hide-tick">
                                <option>Highest First</option>
                                <option>Lowest First</option>
                                <option>Fastest First</option>
                            </select>
                        </div>
                    </div>
<manage-bidders project="{{$project->applications}}"></manage-bidders>
                    
                </div>
            </div>

    </div>
    
        <!-- Row / End -->

        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
    @include('partials.auth.footer')
        <!-- Footer / End -->

    </div>
</div>

@endsection