@extends('layouts.dashboard')

@section('content')
<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >
        
        <!-- Dashboard Headline -->
        <div class="dashboard-headline">
            <h3>Post a Task</h3>

            <!-- Breadcrumbs -->
            <nav id="breadcrumbs" class="dark">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Dashboard</a></li>
                    <li>Post a Task</li>
                </ul>
            </nav>
        </div>

        <!-- Row -->
    <create-task categories="{{$categories}}"></create-task>
        <!-- Row / End -->

        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
    @include('partials.auth.footer')
        <!-- Footer / End -->

    </div>
</div>
@endsection