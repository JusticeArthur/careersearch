@extends('layouts.single')

@section('content')
    <!-- Content
================================================== -->
    <div id="titlebar" class="white margin-bottom-30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Blog</h2>
                    @if(count($featured) > 0)
                    <span>Featured Posts</span>
                    @endif
                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{route('user.home')}}">Home</a></li>
                            <li>Blog</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <blogs-all-component featured="{{$featured}}" tag="{{$tags}}"></blogs-all-component>
    <signin-component></signin-component>

@endsection