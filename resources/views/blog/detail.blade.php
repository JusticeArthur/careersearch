@extends('layouts.single')

@section('content')
    <!-- Content
================================================== -->
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Blog</h2>
                    <span>Blog Post Page</span>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Blog</a></li>
                            <li>Blog Post</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- Post Content -->
    <div class="container">
        <div class="row">
            <!-- Inner Content -->
            <div class="col-xl-8 col-lg-8">
                <!-- Blog Post -->
                <div class="blog-post single-post">
                    <!-- Blog Post Thumbnail -->
                    <div class="blog-post-thumbnail">
                        <div class="blog-post-thumbnail-inner">
                            <span class="blog-item-tag">{{$post->tag->name}}</span>
                            @if($post->avatar)
                                <img src="{{$post->avatar}}" alt="">
                            @else
                            <img src="/images/blog-04.jpg" alt="">
                                @endif
                        </div>
                    </div>

                    <!-- Blog Post Content -->
                    <div class="blog-post-content">
                        <h3 class="margin-bottom-10">{!! $post->title !!}</h3>

                        <div class="blog-post-info-list margin-bottom-20">
                            <a href="#" class="blog-post-info">{{$post->posted_on}}</a>
                            <a href="#"  class="blog-post-info" id="dynamic-update-comments">{{count($post->comments)}} Comment(s)</a>
                        </div>

                        {!! $post->body !!}

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>Interesting? <strong>Share It!</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                    <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Blog Post Content / End -->

                <!-- Blog Nav -->
                <ul id="posts-nav" class="margin-top-0 margin-bottom-40">
                    @if($next)
                    <li class="next-post">
                        <a href="{{route('blog.post.detail',$next->slug)}}">
                            <span>Next Post</span>
                            <strong>{{$next->title}}</strong>
                        </a>
                    </li>
                    @endif
                    @if($previous)
                    <li class="prev-post">
                        <a href="{{route('blog.post.detail',$previous->slug)}}">
                            <span>Previous Post</span>
                            <strong>{{$previous->title}}</strong>
                        </a>
                    </li>
                        @endif
                </ul>

                <!-- Related Posts -->
                <div class="row">

                    <!-- Headline -->
                    <div class="col-xl-12">
                        <h3 class="margin-top-40 margin-bottom-35">Related Posts</h3>
                    </div>
                @foreach($post->relatedPosts as $relatedPost)
                    <!-- Blog Post Item -->
                    <div class="col-xl-6">
                        <a href="{{route('blog.post.detail',$relatedPost->slug)}}" class="blog-compact-item-container">
                            <div class="blog-compact-item">
                                @if($relatedPost->avatar)
                                    <img src="{{$relatedPost->avatar}}" alt="">
                                @else
                                    <img src="/images/blog-03a.jpg" alt="">
                                @endif
                                <span class="blog-item-tag">{{$relatedPost->tag->name}}</span>
                                <div class="blog-compact-item-content">
                                    <ul class="blog-post-tags">
                                        <li>{{$relatedPost->posted_on}}</li>
                                    </ul>
                                    <h3>{{$relatedPost->title}}</h3>
                                    <p>{!! $relatedPost->short_cut !!}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                    <!-- Blog post Item / End -->
                </div>
                <!-- Related Posts / End -->

                <blog-comments-component post="{{$post}}" check="{{auth()->check()?1:0}}"></blog-comments-component>


            </div>
            <!-- Inner Content / End -->


            <div class="col-xl-4 col-lg-4 content-left-offset">
                <div class="sidebar-container">

                    <!-- Location -->
                    <div class="sidebar-widget margin-bottom-40">
                        <div class="input-with-icon">
                            <input id="autocomplete-input" type="text" placeholder="Search">
                            <i class="icon-material-outline-search"></i>
                        </div>
                    </div>

                    <!-- Widget -->
                    <div class="sidebar-widget">

                        <h3>Trending Posts</h3>
                        <ul class="widget-tabs">

                            <!-- Post #1 -->
                            <li>
                                <a href="#" class="widget-content active">
                                    <img src="/images/blog-02a.jpg" alt="">
                                    <div class="widget-text">
                                        <h5>How to "Woo" a Recruiter and Land Your Dream Job</h5>
                                        <span>29 June 2018</span>
                                    </div>
                                </a>
                            </li>

                            <!-- Post #2 -->
                            <li>
                                <a href="#" class="widget-content">
                                    <img src="/images/blog-07a.jpg" alt="">
                                    <div class="widget-text">
                                        <h5>What It Really Takes to Make $100k Before You Turn 30</h5>
                                        <span>3 June 2018</span>
                                    </div>
                                </a>
                            </li>
                            <!-- Post #3 -->
                            <li>
                                <a href="#" class="widget-content">
                                    <img src="/images/blog-04a.jpg" alt="">
                                    <div class="widget-text">
                                        <h5>5 Myths That Prevent Job Seekers from Overcoming Failure</h5>
                                        <span>5 June 2018</span>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                    <!-- Widget / End-->


                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Social Profiles</h3>
                        <div class="freelancer-socials margin-top-25">
                            <ul>
                                <li><a href="#" title="Dribbble" data-tippy-placement="top"><i class="icon-brand-dribbble"></i></a></li>
                                <li><a href="#" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                <li><a href="#" title="Behance" data-tippy-placement="top"><i class="icon-brand-behance"></i></a></li>
                                <li><a href="#" title="GitHub" data-tippy-placement="top"><i class="icon-brand-github"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Tags</h3>
                        <div class="task-tags">
                            @foreach($tags as $tag)
                            <a href="#"><span>{{strtolower($tag->name)}}</span></a>
                                @endforeach
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- Spacer -->
    <div class="padding-top-40"></div>
    <!-- Spacer -->
    <signin-component></signin-component>
@endsection