@php
$segment = Request::segment(2);
$segment3 = Request::segment(3);
$segment4 = Request::segment(4);
@endphp
<nav class="side-navbar box-scroll sidebar-scroll">
    <!-- Begin Main Navigation -->
    <ul class="list-unstyled">
        <li class="{{$segment == 'dashboard'?'active':''}}"><a href="{{route('admin.dashboard')}}">
                <i class="la la-columns"></i>
                <span>Dashboard</span></a></li>
        <li class="{{$segment == 'job-categories'?'active':''}}">
            <a href="#dropdown-db" aria-expanded="{{$segment == 'job-categories'?'true':'false'}}" data-toggle="collapse"><i class="la la-bars"></i>
                <span>Job Settings</span>
            </a>
            <ul id="dropdown-db" class="collapse  {{$segment == 'job-categories'?'list-unstyled show pt-0':''}} ">
                <li ><a class="{{$segment == 'job-categories'?'active show':''}}" href="{{route('job-categories.index')}}">Job Categories</a></li>
    
                <li ><a href="{{route('users.index')}}">Users</a></li>

            </ul>
        </li>
        <li class="{{$segment == 'item-categories' || $segment == 'models' || $segment == 'item-sub-categories' || $segment == 'brands'?'active':''}}">
            <a href="#dropdown-app" aria-expanded="{{$segment == 'item-categories' ||
              $segment == 'brands' || $segment == 'models' || $segment == 'item-sub-category-model'
              || $segment == 'item-sub-categories'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-money"></i><span>Buying / Selling</span></a>
            <ul id="dropdown-app"
                    class="collapse {{$segment == 'item-categories' || $segment == 'item-sub-category-model' ||
                     $segment == 'models' || $segment == 'brands' || $segment == 'item-sub-categories'?'list-unstyled show pt-0':''}}">
                <li>
                    <a class="{{$segment == 'item-categories'?'active show':''}}" href="{{route('item-categories.index')}}">Item Categories</a>
                </li>
                <li>
                    <a class="{{$segment == 'item-sub-categories'?'active show':''}}" href="{{route('item-sub-categories.index')}}">Item Sub Categories</a>
                </li>
                <li>
                    <a class="{{$segment == 'brands'?'active show':''}}" href="{{route('brands.index')}}">Item Brands</a>
                </li>
                <li>
                    <a class="{{$segment == 'models'?'active show':''}}" href="{{route('models.index')}}">Item Models</a>
                </li>
                <li>
                    <a class="{{$segment == 'item-sub-category-model'?'active show':''}}" href="{{route('item-sub-category-model.index')}}">Item Category Brand</a>
                </li>
            </ul>
        </li>
        {{--<li><a href="components-widgets.html"><i class="la la-spinner"></i><span>Widgets</span></a></li>--}}
        {{--//users--}}
        <li class="{{$segment == 'manage-users'?'active':''}}">
            <a href="#dropdown-users" aria-expanded="{{$segment == 'manage-users'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-user-plus"></i><span>Manage Users</span></a>
            <ul id="dropdown-users"
                class="collapse {{$segment == 'manage-users'?'list-unstyled show pt-0':''}}">
                <li>
                    <a class="{{$segment3 == 'plans'?'active show':''}}" href="{{route('plans.index')}}">Plans</a>
                </li>
                <li>
                    <a class="{{$segment3 == 'plan-features'?'active show':''}}" href="{{route('plan-features.index')}}">Plans Features</a>
                </li>
                <li ><a class="{{$segment3 == 'users'?'active show':''}}" href="{{route('users.index')}}">Users</a></li>
            </ul>
        </li>
        {{--//manage companies--}}
        <li class="{{$segment == 'manage-companies'?'active':''}}">
            <a href="#dropdown-companies" aria-expanded="{{$segment == 'manage-companies'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-group"></i><span>Manage Companies</span></a>
            <ul id="dropdown-companies"
                class="collapse {{$segment == 'manage-companies'?'list-unstyled show pt-0':''}}">
                <li>
                    <a class="{{$segment3 == 'verify'?'active show':''}}" href="{{route('manage.companies.verify')}}">Verify
                        {{--<span id="verification-badge" class="nb-new badge-rounded info badge-rounded-small">2</span>--}}
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{$segment == 'manage-users'?'active':''}}">
            <a href="#dropdown-products" aria-expanded="{{$segment == 'products'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-magnet"></i><span>Manage Products</span></a>
            <ul id="dropdown-products"
                class="collapse {{$segment == 'products'?'list-unstyled show pt-0':''}}">
                <li ><a class="{{$segment3 == 'approve'?'active show':''}}" href="{{route('admin.products.approve')}}">Approve</a></li>
            </ul>
        </li>
        <li class="{{$segment == 'settings'?'active':''}}">
            <a href="#dropdown-settings" aria-expanded="{{$segment == 'settings'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-cog"></i><span>Settings</span></a>
            <ul id="dropdown-settings"
                class="collapse {{$segment == 'settings'?'list-unstyled show pt-0':''}}">
                <li>
                    <a class="{{$segment3 == 'site'?'active show':''}}" href="{{route('site.index')}}">Site</a>
                </li>
            </ul>
        </li>
        <li class="{{$segment == 'posts'?'active':''}}">
            <a href="#dropdown-posts" aria-expanded="{{$segment == 'blog'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-magnet"></i><span>Posts</span></a>
            <ul id="dropdown-posts"
                class="collapse {{$segment == 'blog'?'list-unstyled show pt-0':''}}">
                <li ><a class="{{$segment3 == 'posts'?'active show':''}}" href="{{route('posts.index')}}">All</a></li>
                <li ><a class="{{$segment4 == 'create'?'active show':''}}" href="{{route('posts.create')}}">Create</a></li>
            </ul>
        </li>
        <li class="{{$segment == 'reports'?'active':''}}">
            <a href="#dropdown-reports" aria-expanded="{{$segment == 'reports'?'true':'false'}}" data-toggle="collapse">
                <i class="la la-database"></i><span>Reports</span></a>
            <ul id="dropdown-reports"
                class="collapse {{$segment == 'reports'?'list-unstyled show pt-0':''}}">
                <li ><a class="{{$segment3 == 'jobs'?'active show':''}}" href="{{route('admin.reports.index')}}">Jobs</a></li>
            </ul>
        </li>
        <li class="{{$segment == 'profile'?'active':''}}"><a href="{{route('admin.profile.index')}}">
                <i class="la la-user"></i>
                <span>Profile</span></a></li>

    </ul>
    <!-- End Main Navigation -->
</nav>
