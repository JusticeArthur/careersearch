<footer class="main-footer">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-center">
            <p class="text-gradient-02">CareerSearch</p>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-end justify-content-lg-end justify-content-md-end justify-content-center">
            {{--<ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="">Documentation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Changelog</a>
                </li>
            </ul>--}}
        </div>
    </div>
</footer>