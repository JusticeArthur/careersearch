@if(Session::has('success'))
    <script type="text/javascript">
        var message ="{!! Session::get('success') !!}";
        Snackbar.show(
            {
                text: message,
                pos:'top-center',
                showAction:true,
                backgroundColor:'#0f9b1f',
                actionTextColor:'#ffffff',
                actionText:'Dismiss',
                duration:4000
            }
        );
    </script>
@elseif(Session::has('error'))
    <script type="text/javascript">
        var message = "{!! Session::get('error') !!}";
        Snackbar.show(
            {
                text: message,
                pos:'top-center',
                showAction:true,
                backgroundColor:'#c93028',
                actionTextColor:'#ffffff',
                actionText:'Dismiss',
                duration:4000
            }
        );
    </script>
@elseif(Session::has('message'))
    <script type="text/javascript">
        var message = "{!! Session::get('message') !!}";
        Snackbar.show(
            {
                text: message,
                pos:'top-center',
                showAction:true,
                backgroundColor:'#e5ac39',
                actionTextColor:'#ffffff',
                actionText:'Dismiss',
                duration:4000
            }
        );
    </script>
@endif




