@php
    $segment = Request::segment(1);
    $segment2 = Request::segment(2);
@endphp
<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Start">
                        <li class="{{$segment == 'dashboard'?'active':''}}"><a href="{{route('home')}}">
                                <i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                        <li class="{{$segment == 'messages'?'active':''}}"><a href="{{route('dashboard.messages')}}">
                                <i class="icon-material-outline-question-answer"></i> Messages <span id="realtime-messages" class="nav-tag">{{count(auth()->user()->unreadMessages())}}</span></a></li>
                        <li class="{{$segment == 'bookmarks'?'active':''}}"><a href="{{route('dashboard.bookmarks')}}">
                                <i class="icon-material-outline-star-border"></i> Bookmarks</a></li>
                        <li class="{{$segment == 'reviews'?'active':''}}"><a href="{{route('dashboard.reviews')}}">
                                <i class="icon-material-outline-rate-review"></i> Reviews</a></li>
                        <li class="{{$segment == 'subscriptions'?'active':''}}"><a href="{{route('subscriptions.index')}}">
                                <i class="icon-material-outline-money"></i> Subscriptions</a></li>
                        @role('employee')
                        <li class="{{$segment == 'interviews'?'active':''}}"><a href="{{route('applicants.interview')}}">
                            <i class="icon-material-outline-assessment"></i> Interview Invites <span class="nav-tag">
                                    {{\App\Utilities\Utils::interviewCount()}}
                                </span></a></li>
                        <li class="{{$segment == 'contracts'?'active':''}}"><a href="{{route('dashboard.contracts')}}">
                                <i class="icon-material-outline-thumb-up"></i> Contracts</a></li>
                        @endrole
                        @role('employer')
                        <li class="{{$segment == 'interview-postpones'?'active':''}}"><a href="{{route('employee.interview.postpone')}}">
                                <i class="icon-material-outline-autorenew"></i> Postpone Requests</a></li>
                        @endrole
                    </ul>
                    <ul data-submenu-title="Find Something">
                        <li class="{{--{{$segment2 == 'buy' /*|| $segment2 == 'my-posts'*/?'active-submenu':''}}--}}">
                            <a href="#"><i class="icon-feather-search"></i>Browse Stuff</a>
                            <ul>
                                @role('employee')
                                <li ><a href="{{route('find_jobs.all')}}">Find Job</a></li>
                                {{--<li ><a href="{{route('find_projects.all')}}">Find Task</a></li>--}}
                                <li ><a href="{{route('companies.all.index')}}">Find Company</a></li>
                                @endrole
                                @role('employer')
                                <li ><a href="{{route('find_employees.all')}}">Find Employees</a></li>
                                @endrole
                            </ul>
                        </li>
                    </ul>

                    <ul data-submenu-title="Organize and Manage">
                        <li class="{{$segment2 == 'buy' /*|| $segment2 == 'my-posts'*/?'active-submenu':''}}">
                            <a href="#"><i class="icon-brand-product-hunt"></i>Buy Products</a>
                            <ul>
                                <li class="{{$segment2 == 'buy'?'active':''}}"><a href="{{route('products.buy.index')}}">Buy
                                    </a></li>
                            </ul>
                        </li>
                        <li class="{{$segment2 == 'sell' || $segment2 == 'my-posts'?'active-submenu':''}}">
                            <a href="#"><i class="icon-brand-product-hunt"></i>Sell Products</a>
                            <ul>
                                <li class="{{$segment2 == 'sell'?'active':''}}"><a href="{{route('sell.index')}}">Sell
                                    </a></li>
                                <li class="{{$segment2 == 'my-posts'?'active':''}}"><a href="{{route('products.sell.my_posts')}}">My Posts  <span class="nav-tag">
                                            {{count(auth()->user()->products)}}</span></a></li>
                            </ul>
                        </li>
                        @role(['employer','admin'])
                        <li class="{{$segment == 'jobs'  || $segment == 'manage-jobs' || $segment == 'manage-candidates'?'active-submenu':''}}"><a href="#"><i class="icon-material-outline-business-center"></i> Jobs</a>
                            <ul>
                                <li class="{{$segment == 'manage-jobs' || $segment == 'manage-candidates'?'active':''}}"><a href="{{route('manage_jobs.all')}}">Manage Jobs <span class="nav-tag">{{count(auth()->user()->works)}}</span></a></li>
                                <li class="{{$segment == 'jobs'?'active':''}}"><a href="{{route('jobs.index')}}">Post a Job</a></li>
                            </ul>
                        </li>
                       {{-- <li><a href="#"><i class="icon-material-outline-assignment"></i> Tasks</a>
                            <ul>
                                <li><a href="{{route('managetask')}}">Manage Tasks <span class="nav-tag">{{count(auth()->user()->projects)}}</span></a></li>
                                <li><a href="{{route('activebids')}}">My Active Bids <span class="nav-tag">4</span></a></li>
                                <li ><a href="{{route('project.index')}}">Post a Task</a></li>
                            </ul>
                        </li>--}}
                        @endrole
                    </ul>

                    <ul data-submenu-title="Account">
                        <li class="{{$segment == 'settings'?'active':''}}"><a href="{{route('dashboard.settings')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                        <li><a href="{{route('my.profile.index',['slug'=>auth()->user()->profile->url])}}"><i class="icon-material-outline-person-pin"></i> Profile</a></li>
                        <li><a href="{{route('user.logout')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>




