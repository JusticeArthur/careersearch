<header id="header-container" class="fullwidth dashboard-header not-sticky">

    <!-- Header -->
    <div id="header">
        <div class="container">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                    <a href="{{route('home')}}">
                        <img src="{{asset('logo/career search/career search blue/3x/career search blue@3x.png')}}" alt="">
                    </a>
                </div>

                <!-- Main Navigation -->
                @php
                $url = Request::segment(1);
                @endphp
                <nav id="navigation">
                    <ul id="responsive">
                        @auth
                        <li><a href="{{route('home')}}">Home</a>

                        </li>
                            <li><a href="{{route('blog.all.index')}}">Blog</a>

                            </li>
                        @endauth
                        @guest
                        <li><a href="{{route('blog.all.index')}}">Blog</a>

                        </li>
                            @endguest
                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->


            <!-- Right Side Content / End -->
            <div class="right-side">
                @auth
                <!--  User Notifications -->
                <div class="header-widget hide-on-mobile">

                    <!-- Notifications -->
                    <all-notifications :user="{{auth()->user()->id}}" :unread="{{auth()->user()->unreadNotifications}}" ></all-notifications>
                    <!-- Messages -->
                    <all-messages-component :user="{{auth()->user()->id}}" :message="{{auth()->user()->unreadMessages()}}"></all-messages-component>

                </div>
                <!--  User Notifications / End -->
                <!-- User Menu -->
                <div class="header-widget">

                    <!-- Messages -->
                    <div class="header-notifications user-menu">
                        <div class="header-notifications-trigger">
                            <a href="#"><div class="user-avatar {{auth()->user()->online()? 'status-online':''}}">
                                    <img src="/images/user-avatar-small-01.jpg" alt=""></div></a>
                        </div>

                        <!-- Dropdown -->
                        <div class="header-notifications-dropdown">

                            <!-- User Status -->
                            <div class="user-status">

                                <!-- User Name / Avatar -->
                                <div class="user-details">
                                    <div class="user-avatar {{auth()->user()->online()? 'status-online':''}}"><img src="/images/user-avatar-small-01.jpg" alt=""></div>
                                    <div class="user-name">
                                        {{Auth::user()->firstname}} {{Auth::user()->lastname}} <span>Freelancer</span>
                                    </div>
                                </div>

                                <!-- User Status Switcher -->
                                <div class="status-switch" id="snackbar-user-status">
                                    <label class="user-online current-status">Online</label>
                                    <label class="user-invisible">Invisible</label>
                                    <!-- Status Indicator -->
                                    <span class="status-indicator" aria-hidden="true"></span>
                                </div>
                            </div>

                            <ul class="user-menu-small-nav">
                                @role(['employer','employee'])
                                <li><a href="{{route('home')}}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{route('dashboard.settings')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                                @endrole
                                <li><a href="{{route('user.logout')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                            </ul>

                        </div>
                    </div>

                </div>
                <!-- User Menu / End -->
                @endauth
                <!-- Mobile Navigation Button -->
                <span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

            </div>
            <!-- Right Side Content / End -->

        </div>
    </div>
    <!-- Header / End -->

</header>