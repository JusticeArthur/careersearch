<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'careersearch') }}</title>
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/font-awesome.min.css')}}">
    <link rel="icon" type="image/ico" href="{{asset('logo/fav.png')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/colors/blue.css')}}">
    <link rel="stylesheet" href="{{asset('css/loading.css')}}">
    <link rel="stylesheet" href="{{asset('css/loading-btn.css')}}">
@stack('custom_css')
</head>
<body class="gray">

    <!-- Wrapper -->
     <div id="wrapper">

         <div id="app">
             <!-- Header Container
         ================================================== -->
             @include('partials.header')
             <div class="clearfix"></div>
             <!-- Header Container / End -->


             <!-- Dashboard Container -->
             <div class="dashboard-container">

                 <!-- Dashboard Sidebar
                 ================================================== -->
                 @if(Request::segment(2) !== 'buy' && Request::segment(1) !== 'find-employees' && Request::segment(1) !== 'cs' && Request::segment(1) !== 'find-jobs' && Request::segment(1) != 'search')
                     @include('partials.auth.sidebar')
                     @endif
                 <!-- Dashboard Sidebar / End -->


                 <!-- Dashboard Content
                 ================================================== -->
             @yield('content')
             <!-- Dashboard Content / End -->

             </div>
             <!-- Dashboard Container / End -->
             {{--<Adsense
                     data-ad-client="ca-pub-5772331186390920"
                     data-ad-slot="1234567890">
             </Adsense>--}}
         </div>
     </div>
     <!-- Wrapper / End -->


     <!-- Apply for a job popup
     ================================================== -->
    @yield('dialogs')
    <!-- Apply for a job popup / End -->

    <!-- Scripts
    </div>
    ================================================== -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{asset('js/mmenu.min.js')}}"></script>
<script src="{{asset('js/tippy.all.min.js')}}"></script>
<script src="{{asset('js/simplebar.min.js')}}"></script>
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('js/snackbar.js')}}"></script>
<script src="{{asset('js/clipboard.min.js')}}"></script>
<script src="{{asset('js/counterup.min.js')}}"></script>
<script src="{{asset('js/magnific-popup.min.js')}}"></script>
<script src="{{asset('js/slick.min.js')}}"></script>
    @include('partials.alert')
    @stack('custom_js')
<script src="{{asset('js/custom.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function() {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>


    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5bf0fa4279ed6453cca9edbf/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
</body>
</html>
