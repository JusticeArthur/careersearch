<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Careersearch | @yield('title')</title>
    <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/ico" href="{{asset('logo/fav.png')}}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/vendors/css/base/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/base/elisyam-1.5.css')}}">
    <link rel="stylesheet" href="{{asset('css/loading.css')}}">
    <link rel="stylesheet" href="{{asset('css/loading-btn.css')}}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body class="bg-fixed-02">
<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        <img src="{{asset('all_assets/LOGOV1.svg')}}" alt="logo" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>
<!-- End Preloader -->
<!-- Begin Container -->
    <div class="container-fluid h-100 overflow-y">
        <div class="row flex-row h-100">
            <div class="col-12 my-auto">
                <div class="password-form mx-auto">
                    <div class="logo-centered">
                        <a href="db-default.html">
                            <img src="{{asset('all_assets/logo and text.svg')}}" alt="logo">
                        </a>
                    </div>
                    <h3>Password Recovery</h3>
                    <div id="app">
                        @yield('content')
                    </div>

                </div>
            </div>
            <!-- End Col -->
        </div>
        <!-- End Row -->
    </div>
<!-- End Container -->
<!-- Begin Vendor Js -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('assets/vendors/js/base/core.min.js')}}"></script>
<!-- End Vendor Js -->
<!-- Begin Page Vendor Js -->
<script src="{{asset('assets/vendors/js/app/app.min.js')}}"></script>
<!-- End Page Vendor Js -->
<script src="{{asset('assets/vendors/js/noty/noty.min.js')}}"></script>
<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
<script src="{{asset('assets/js/components/notifications/notifications.min.js')}}"></script>
</body>
</html>
