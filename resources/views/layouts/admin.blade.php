<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="CareerSearch">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/ico" href="{{asset('logo/fav.png')}}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/base/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/base/elisyam-1.5.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl-carousel/owl.theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/loading.css')}}">
    <link rel="stylesheet" href="{{asset('css/loading-btn.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/datatables/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-select/bootstrap-select.min.css')}}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <style>
        .avatar-wrapper {
            position: relative;
            width: 150px;
            height: 150px;
            border-radius: 4px;
            overflow: hidden;
            box-shadow: none;
            margin: 0 10px 30px 0;
            transition: all .3s ease;
        }
        .avatar-wrapper:hover {
            transform: scale(1.05);
            cursor: pointer;
        }

        .avatar-wrapper .profile-pic {
            height: 100%;
            width: 100%;
            transition: all .3s ease;
            object-fit: cover;
        }

        .avatar-wrapper .profile-pic:after {
            font-family: Feather-Icons;
            content: "\e9f1";
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            line-height: 120px;
            position: absolute;
            font-size: 60px;
            background: #f0f0f0;
            color: #aaa;
            text-align: center;
        }

        .avatar-wrapper .upload-button {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
        }

        .avatar-wrapper .file-upload {
            opacity: 0;
            pointer-events: none;
            position: absolute;
        }
    </style>
</head>
<body id="page-top">
<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        <img src="{{asset('logo/footer.png')}}" alt="logo" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>
<!-- End Preloader -->
<div id="app">
    @yield('contents')
</div>
<!-- End Modal -->
<!-- Begin Vendor Js -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('assets/vendors/js/base/core.min.js')}}"></script>
<!-- End Vendor Js -->
<!-- Begin Page Vendor Js -->
{{--<script src="{{asset('assets/vendors/js/datatables/datatables.min.js')}}"></script>--}}
<script src="{{asset('assets/vendors/js/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/datatables/jszip.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/vendors/js/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/nicescroll/nicescroll.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/chart/chart.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/progress/circle-progress.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/calendar/moment.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/calendar/fullcalendar.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/app/app.js')}}"></script>
<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
<script src="{{asset('assets/js/dashboard/db-default.js')}}"></script>
<script src="{{asset('assets/vendors/js/noty/noty.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/datepicker/daterangepicker.js')}}"></script>

<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
<script src="{{asset('assets/js/components/notifications/notifications.min.js')}}"></script>
<script src="{{asset('assets/js/components/tables/tables.js')}}"></script>
@include('partials.alert')
<!-- End Page Snippets -->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5bf0fa4279ed6453cca9edbf/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
</body>
</html>

<!-- End Vendor Js -->
<!-- Begin Page Vendor Js -->

<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
