@extends('layouts.dashboard')

@section('content')
    <home-search-filters words="{{$keywords}}" product="{{$products}}" work="{{$works}}" authentication="{{auth()->check()?1:0}}"></home-search-filters>
@endsection