@extends('layouts.app')

@section('content')
@include('partials.header')
        <div class="clearfix"></div>
        <!-- add class "disable-gradient" to enable consistent background overlay -->
        <div class="intro-banner" data-background-image="/images/background.jpg">
            <div class="container">

                <!-- Intro Headline -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner-headline">
                            <h3>
                                <strong>Hire experts or be hired for any job, any time.</strong>
                                <br>
                                <span>Thousands of small businesses use <strong class="color">Careersearch</strong> to turn their ideas into reality.</span>
                            </h3>
                        </div>
                    </div>
                </div>

                <!-- Search Bar -->
                <home-search></home-search>
                <!-- Stats -->
                <div class="row">
                    <div class="col-md-12">
                        <ul class="intro-stats margin-top-45 hide-under-992px">
                            <li>
                                <strong class="counter">{{$jobs}}</strong>
                                <span>Jobs Posted</span>
                            </li>
                            <li>
                                <strong class="counter">{{$employees}}</strong>
                                <span>Job Seekers</span>
                            </li>
                            <li>
                                <strong class="counter">{{$employers}}</strong>
                                <span>Companies</span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>


        <!-- Content
        ================================================== -->
        <!-- Category Boxes -->
    @if(count($products) > 0)
        <div class="section margin-top-65">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">

                        <div class="section-headline centered margin-bottom-15">
                            <h3>Latest Products on Sale</h3>
                        </div>

                        <!-- Category Boxes Container -->
                        <div class="categories-container">

                            <!-- Category Box -->
                            @foreach($products as $product)
                            <a href="@guest #sign-in-dialog @endguest @auth {{route('product.buy.spec',['slug'=>$product->slug])}} @endauth"
                               class="@guest popup-with-zoom-anim @endguest category-box" style="align-items: center"
                               @guest onclick="sessionStorage.returnUrl='{{route('product.buy.spec',['slug'=>$product->slug])}}'" @endguest
                            >
                                    @if(count($product->avatars) > 0)
                                    <div class="avatar-wrapper" data-tippy-placement="bottom" title="{{$product->title}}">
                                        <img class="profile-pic" src="{{$product->avatars[0]['name']}}" alt="" />
                                    </div>
                                        @else
                                    <div class="avatar-wrapper" data-tippy-placement="bottom">
                                        <img class="profile-pic" src="{{asset('images/company-logo-01.png')}}" alt="" />
                                    </div>
                                        @endif
                                <div class="category-box-content">
                                    <h3>{{$product->title}}</h3>
                                    <h4>{{$product->price}}</h4>
                                    <p>{{$product->location}}</p>
                                </div>
                            </a>
                                @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Category Boxes / End -->
    @endif
        @if(count($featured_jobs) > 0)
        <!-- Features Jobs -->
        <div class="section gray margin-top-45 padding-top-65 padding-bottom-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">

                        <!-- Section Headline -->
                        <div class="section-headline margin-top-0 margin-bottom-35">
                            <h3>Featured Jobs</h3>
                            <a href="" class="headline-link">Browse All Jobs</a>
                        </div>

                        <!-- Jobs Container -->
                        <div class="listings-container compact-list-layout margin-top-35">

                            <!-- Job Listing -->
                            @foreach($featured_jobs as $job)
                                <a href="@guest #sign-in-dialog @endguest @auth {{route('jobs_apply.local',['slug'=>$job->slug])}} @endauth" class="@guest popup-with-zoom-anim @endguest job-listing with-apply-button"

                                >

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Logo -->
                                        <div class="job-listing-company-logo">
                                            <img src="{{$job->company->logo}}" alt="">
                                        </div>

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title">{{$job->title}}</h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-material-outline-business"></i> {{$job->company->name}} <div class="verified-badge" title="Verified Employer" data-tippy-placement="top"></div></li>
                                                    <li><i class="icon-material-outline-location-on"></i> {{$job->location}}</li>
                                                    <li><i class="icon-material-outline-business-center"></i> {{title_case(str_replace('_',' ',$job->type))}}</li>
                                                    <li><i class="icon-material-outline-access-time"></i>{{\App\Utilities\Utils::humanDate($job->created_at) }}</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <!-- Apply Button -->
                                        <span class="list-apply-button ripple-effect"
                                              @guest onclick="sessionStorage.returnUrl='{{route('jobs_apply.local',['slug'=>$job->slug])}}'" @endguest
                                        >Apply Now</span>
                                    </div>
                                </a>
                                @endforeach

                        </div>
                        <!-- Jobs Container / End -->

                    </div>
                </div>
            </div>
        </div>
        <!-- Featured Jobs / End -->
@endif
@if(count($featured_states) > 0)
        <!-- Features Cities -->
        <div class="section margin-top-65 margin-bottom-65">
            <div class="container">
                <div class="row">

                    <!-- Section Headline -->
                    <div class="col-xl-12">
                        <div class="section-headline centered margin-top-0 margin-bottom-45">
                            <h3>Featured Regions</h3>
                        </div>
                    </div>

                    @foreach($featured_states as $key=>$state)
                        @if(count($state['jobs']) > 0)
                                <div class="col-xl-3 col-md-6">
                                    <!-- Photo Box -->
                                    <a href="#" class="photo-box" data-background-image="{{asset('images/city.jpg')}}">
                                        <div class="photo-box-content">
                                            <h3>{{$state['name']}}</h3>
                                            <span>{{count($state['jobs'])}} Jobs</span>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach

                </div>
            </div>
        </div>
        <!-- Features Cities / End -->
@endif
@if(count($rated_employees))
    <!-- Highest Rated Freelancers -->
    <div class="section gray padding-top-65 padding-bottom-70 full-width-carousel-fix">
        <div class="container">
            <div class="row">

                <div class="col-xl-12">
                    <!-- Section Headline -->
                    <div class="section-headline margin-top-0 margin-bottom-25">
                        <h3>Highest Rated Job Seekers</h3>
                        <a href="#" class="headline-link">Browse All Job Seekers</a>
                    </div>
                </div>

                <div class="col-xl-12">
                    <div class="default-slick-carousel freelancers-container freelancers-grid-layout">
                        @foreach($rated_employees as $employee)
                        <!--Freelancer -->
                        <div class="freelancer">

                            <!-- Overview -->
                            <div class="freelancer-overview">
                                <div class="freelancer-overview-inner">

                                    <!-- Bookmark Icon -->
                                    {{--<span class="bookmark-icon"></span>--}}

                                    <!-- Avatar -->
                                    <div class="freelancer-avatar">
                                        <div class="verified-badge"></div>
                                        <a href="#">
                                            @if($employee->profile->logo)
                                                <img src="{{$employee->profile->logo}}" alt="">
                                            @else
                                            <img src="images/user-avatar-big-01.jpg" alt="">
                                            @endif
                                        </a>
                                    </div>

                                    <!-- Name -->
                                    <div class="freelancer-name">
                                        <h4><a href="#">{{$employee->name}} <img class="flag" src="images/flags/gh.svg" alt="" title="Ghana" data-tippy-placement="top"></a></h4>
                                    </div>

                                    <!-- Rating -->
                                    <div class="freelancer-rating">
                                        <div class="star-rating" data-rating="{{$employee->profile->overall_rating}}"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Details -->
                            <div class="freelancer-details">
                                <div class="freelancer-details-list">
                                    <ul>
                                        <li>Location <strong><i class="icon-material-outline-location-on"></i> {{\App\Utilities\Utils::formatLocation($employee->profile->location)}}</strong></li>
                                        <li>Rate <strong>₵{{$employee->profile->rate}} / hr</strong></li>
                                        <li>Job Success <strong>{{$employee->profile->job_success}}%</strong></li>
                                    </ul>
                                </div>
                                <a href="@guest #sign-in-dialog @endguest @auth {{route('my.profile.index',['slug'=>$employee->profile->url])}} @endauth"
                                   class="@guest popup-with-zoom-anim @endguest button button-sliding-icon ripple-effect"
                                   @guest onclick="sessionStorage.returnUrl='{{route('my.profile.index',['slug'=>$employee->profile->url])}}'" @endguest
                                >
                                    View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>

                              {{--  <a href="@guest #sign-in-dialog @endguest @auth {{route('product.buy.spec',['slug'=>$product->slug])}} @endauth"
                                   class="@guest popup-with-zoom-anim @endguest category-box" style="align-items: center"
                                   @guest onclick="sessionStorage.returnUrl='{{route('product.buy.spec',['slug'=>$product->slug])}}'" @endguest
                                >--}}
                            </div>
                        </div>
                        <!-- Freelancer / End -->
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Highest Rated Freelancers / End-->

@endif

<!-- Membership Plans -->
        <div class="section padding-top-60 padding-bottom-75">
            <div class="container">
                <div class="row">

                    <div class="col-xl-12">
                        <!-- Section Headline -->
                        <div class="section-headline centered margin-top-0 margin-bottom-35">
                            <h3>Membership Plans</h3>
                        </div>
                    </div>


                    <div class="col-xl-12">

                        <!-- Billing Cycle  -->
                        <div class="billing-cycle-radios margin-bottom-70">
                            <div class="radio billed-monthly-radio">
                                <input id="radio-5" name="radio-payment-type" type="radio" checked>
                                <label for="radio-5"><span class="radio-label"></span> Billed Monthly</label>
                            </div>

                            <div class="radio billed-yearly-radio">
                                <input id="radio-6" name="radio-payment-type" type="radio">
                                <label for="radio-6"><span class="radio-label"></span> Billed Yearly <span class="small-label">Save 10%</span></label>
                            </div>
                        </div>

                        <!-- Pricing Plans Container -->
                        <div class="pricing-plans-container">
                            <!-- Plan -->
                           @foreach($plans as $plan)
                                   <div class="pricing-plan {{strtolower($plan->name) == 'basic plan'?'recommended':''}}">
                                       @if(strtolower($plan->name) == 'basic plan')
                                           <div class="recommended-badge">Recommended</div>
                                       @endif
                                       <h3>{{$plan->name}}</h3>
                                       <p class="margin-top-10">One time fee for one listing or task highlighted in search results.</p>
                                       <div class="pricing-plan-label billed-monthly-label"><strong>GHS {{$plan->monthly_amount}}</strong>/ monthly</div>
                                       <div class="pricing-plan-label billed-yearly-label"><strong>GHS {{$plan->yearly_amount}}</strong>/ yearly</div>
                                       <div class="pricing-plan-features">
                                           <strong>Features of {{$plan->name}}</strong>
                                           <ul>
                                               @foreach($plan->features as $feature)
                                                   <li>{{$feature->name}}</li>
                                               @endforeach
                                           </ul>
                                       </div>
                                       <a href="@guest #sign-in-dialog @endguest @auth {{route('subscriptions.index')}} @endauth"
                                          class="@guest popup-with-zoom-anim @endguest button full-width margin-top-20" style="align-items: center"
                                          @guest onclick="sessionStorage.returnUrl='{{route('subscriptions.index')}}'" @endguest
                                       >
                                           @if(strtolower($plan->name) == 'free plan')
                                               Start Free Trial
                                               @else
                                               Buy Now
                                           @endif
                                       </a>
                                   </div>
                               @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Membership Plans / End-->
    @include('partials.footer')
    <signin-component></signin-component>
    @endsection
@push('custom_js')
    <!--Start of Tawk.to Script-->

    <!--End of Tawk.to Script-->
    @endpush