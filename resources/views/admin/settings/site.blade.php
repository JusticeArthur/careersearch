@extends('admin.dashboard')

@push('css')

@endpush


@section('title','')


@section('page_contents')
    <div class="content-inner search-results">
        <div class="container-fluid">
            <!-- Begin Page Header-->
            <div class="row">
                <div class="page-header">
                    <div class="d-flex align-items-center">
                        <h2 class="page-header-title">Site Settings</h2>
                        <div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href=""><i class="ti ti-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Site Settings</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Header -->
            <site-settings-component currencies="{{$currencies}}" settings="{{$settings}}" countries="{{$countries}}">

            </site-settings-component>
        </div>

        <!-- End Container -->
        <!-- Begin Page Footer-->
    @include('partials.admin.footer')
    <!-- End Page Footer -->
        <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>
    </div>
@endsection

