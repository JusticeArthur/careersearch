@extends('admin.dashboard')

@push('css')

@endpush


@section('title','')


@section('page_contents')
    <div class="content-inner search-results">
        <div class="container-fluid">
            <!-- Begin Page Header-->
            <div class="row">
                <div class="page-header">
                    <div class="d-flex align-items-center">
                        <h2 class="page-header-title">Job Categories</h2>
                        <div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href=""><i class="ti ti-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Job Categories</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="widget has-shadow">
                        <div class="widget-header">
                            Add Job Category
                        </div>
                        <job-category-add></job-category-add>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget has-shadow">
                        <div class="widget-header">
                            All Job Categories
                        </div>
                        <div class="widget-body">
                            <div class="table-responsive table-scroll padding-right-10" style="max-height:520px;">
                                <job-category-all data="{{$data}}"></job-category-all>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Container -->
        <!-- Begin Page Footer-->
        @include('partials.admin.footer')
        <!-- End Page Footer -->
        <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>
    </div>
@endsection
