@extends('admin.dashboard')

@push('css')

@endpush


@section('title','')


@section('page_contents')
    <admin-dashboard-component users="{{$users_count}}" top-ratings="{{$top_ratings}}" plans="{{$plans}}" job-overview="{{$jobOverview}}"></admin-dashboard-component>
@endsection

