@extends('layouts.dashboard')
@push('custom_js')
    @include('partials.alert')
    @endpush
@section('content')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Interview Invites</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Interview Invites</li>
                    </ul>
                </nav>
            </div>

            <!-- Row -->
            <div class="row">

                <!-- Dashboard Box -->
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-material-outline-supervisor-account"></i>
                              {{count($interviews)}}  Interview(s) Pending</h3>
                        </div>

                        <div class="content">
                            <ul class="dashboard-box-list">
                                    <applicant-interview interviews="{{$interviews}}"></applicant-interview>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
            @include('partials.auth.footer')
            <!-- Footer / End -->

        </div>
    </div>
@endsection
