@extends('layouts.single')


@section('title','')


@section('content')
    <cs-my-profile-component jobs-done="{{$jobs_done}}" employments="{{$employments}}" current="{{$currentUser}}" user="{{$profile}}"></cs-my-profile-component>
    <!-- Spacer -->
    <div class="margin-top-15"></div>
    <!-- Spacer / End-->
@endsection

