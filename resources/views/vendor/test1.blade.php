<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Example 2</title>
    <style>
        @font-face {
            font-family: SourceSansPro;
            src: url("/SourceSansPro-Regular.ttf");
        }
        @page {
            margin: 0px;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-family: SourceSansPro;

        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
            margin-top: 8px;
        }

        #logo img {
            height: 70px;
        }

        #company {
            float: right;
            text-align: left;
        }


        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            float: right;
            text-align: left;
        }

        #invoice h1 {
            color: #0087C3;
            font-size: 1.4em;
            line-height: 1em;
            font-weight: bolder;
            margin: 0  0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table th,
        table td {
            padding: 20px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: right;
        }
        
        table td h3{
            color: #2E6BFD;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

        table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #2E6BFD;
        }

        table .desc {
            text-align: left;
        }

        table .unit {
            background: #DDDDDD;
        }

        table .qty {
        }

        table .total {
            background: #2E6BFD;
            color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr:last-child td {
            color: #2E6BFD;
            font-size: 1.4em;
            border-top: 1px solid #2E6BFD;

        }

        table tfoot tr td:first-child {
            border: none;
        }

        #thanks{
            font-size: 2em;
            margin-bottom: 50px;
        }

        #notices{
            padding-left: 6px;
            border-left: 6px solid #0087C3;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img src="https://drive.google.com/uc?export=view&id=12bas_mkygrnYGS43awEDCvLTS-y3To_8" alt="">
    </div>
    <div id="company">
        <h2 class="name">Careersearch</h2>
        <div>P.O. Box 23</div>
        <div>(233) 24-374-2088</div>
        <div><a href="mailto:justice@tecunitgh.com">justice@tecunitgh.com</a></div>
    </div>
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">INVOICE TO:</div>
            <h2 class="name">{{$user->name}}</h2>
            <div class="address">{{$user->profile->location}}</div>
            <div class="email"><a href="mailto:john@example.com">{{$user->email}}</a></div>
        </div>
        <div id="invoice">
            <h1>INVOICE {{$invoice->reference}}</h1>
            <div class="date">Date of Invoice: {{\Carbon\Carbon::parse($invoice->created_at)->toFormattedDateString()}}</div>
            <div class="date">Due Date: {{\Carbon\Carbon::parse($invoice->created_at)->toFormattedDateString()}}</div>
        </div>
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
            <th class="unit">PRICE</th>
            <th class="qty">CHARGES</th>
            <th class="total">TOTAL</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="no">01</td>
            <td class="desc"><h3>{{$plan->name}}</h3>Subscription to Careersearch {{$plan->name}}</td>
            @if(strtolower($subscription->plan_type) == 'month')
                <td class="unit">GHS {{number_format($plan->monthly_amount,2)}}</td>
                <td class="qty">GHS {{number_format(((float)$invoice->total - (float)$plan->monthly_amount),2)}}</td>
            @else
                <td class="unit">GHS {{number_format($plan->yearly_amount,2)}}</td>
                <td class="qty">GHS {{number_format(((float)$invoice->total - (float)$plan->yearly_amount),2)}}</td>
            @endif
            <td class="total">GHS {{number_format($invoice->total,2)}}</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">SUBTOTAL</td>
            @if(strtolower($subscription->plan_type) == 'month')
                <td>GHS {{number_format($plan->monthly_amount,2)}}</td>
            @else
                <td>GHS {{number_format($plan->yearly_amount,2)}}</td>
            @endif
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">CHARGES</td>
            @if(strtolower($subscription->plan_type) == 'month')
                <td>GHS {{number_format(((float)$invoice->total - (float)$plan->monthly_amount),2)}}</td>
            @else
                <td>GHS {{number_format(((float)$invoice->total - (float)$plan->yearly_amount),2)}}</td>
            @endif
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL</td>
            <td>GHS {{number_format($invoice->total,2)}}</td>
        </tr>
        </tfoot>
    </table>
    <div id="thanks">Thank you!</div>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">We really appreciate your trust in us.</div>
    </div>
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>