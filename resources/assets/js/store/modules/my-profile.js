const state = {
    profile:[]
};
const getters = {
    profileIntroduction: state =>{
        return state.profile.introduction;
    },
    profileName:state=>{
        return state.profile.user.firstname +' ' +state.profile.user.lastname;
    },
    profileSkills:state=>{
        return state.profile.skills;
    },
    profileId:state=>{
        return state.profile.id
    },
    profileCountry:state=>{
        return state.profile.state.ucountry.name;
    },
    profileLogo:state=>{
        return state.profile.logo;
    },
    profileRate:state=>{
        return state.profile.rate
    },
    profileRating:state=>{
        return state.profile.overall_rating
    },
    profileJobSuccess:state=>{
        return state.profile.job_success;
    },
    profileRecommendation:state=>{
        return state.profile.recommendation;
    },
    profileUserId:state=>{
        return state.profile.user.id;
    }

};
const mutations ={
    setProfile : (state,payload) =>{
        state.profile = payload;
    },
    setProfileIntroduction:(state,payload)=>{
        state.profile.introduction = payload;
    }
};
const actions = {
    setProfile :(context,payload)=>{
        context.commit('setProfile',payload);
    },
    updateProfileIntroduction:(context,payload)=>{
        axios.post('/cs/update-profile-intro',{id:context.getters.profileId,data:payload})
            .then(resp=>{
                context.commit('setProfileIntroduction',payload)
            })
            .catch(error=>{

            })
    }
};
export default {
    state,
    getters,
    mutations,
    actions
}