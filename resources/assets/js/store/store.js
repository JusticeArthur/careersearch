import Vue from 'vue';
import  Vuex from 'vuex';
import profile from './modules/my-profile'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        itemCategories:[],
        itemSubCategories:[],
        jobs:[],
        jobCategories:[],
        tags:[],
        brands:[],
        models:[],
        plans:[],
        planFeatures:[],
        countries:[],
        states:[],
        currencies:[],
        feature:[],
        cat:[],
        item:[],
        products:[],
        noFilters:[],
        log:{},
        center:{},
        markers:[],
        chats:[],
        currentMessage:{},
        activeChat:'',
        keywords:[],
        users:[]
    },
    modules:{
        profile
    }
});