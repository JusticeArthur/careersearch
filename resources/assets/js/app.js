
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
//window.ResumeParser = require('resume-parser');
import VueTelInput from 'vue-tel-input';
Vue.use(VueTelInput);
import {store} from "./store/store";
import ElementUI from 'element-ui';
Vue.use(ElementUI);
import VueDataTables from 'vue-data-tables';
Vue.use(VueDataTables);

import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);
locale.use(lang);
import * as VueGoogleMaps from 'vue2-google-maps';
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyA8f0RDMdrDNSM9hBNQeAi1Vl7wbw6uals',
        libraries: 'places', // This is required if you use the Autocomplete plugin
    }})
import VueChatScroll from 'vue-chat-scroll';
Vue.use(VueChatScroll);
import Datetime from 'vue-datetime'
import 'vue-slider-component/theme/default.css'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'

Vue.use(Datetime)

//working with adversitements
import Ads from 'vue-google-adsense'

Vue.use(require('vue-script2'))

Vue.use(Ads.Adsense)
Vue.use(Ads.InArticleAdsense)
Vue.use(Ads.InFeedAdsense)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('signin-component', require('./components/signin.vue'));
Vue.component('settings-component', require('./components/settings.vue'));
Vue.component('create-job', require('./components/jobs/post_job.vue'));
Vue.component('edit-job-component', require('./components/jobs/edit-job.vue'));
Vue.component('manage-jobs', require('./components/jobs/manage_jobs.vue'));
Vue.component('filter-jobs', require('./components/jobs/find/index.vue'));
Vue.component('filter-projects', require('./components/projects/filters.vue'));
Vue.component('create-task', require('./components/projects/post_task.vue'));
Vue.component('apply-project', require('./components/projects/apply.vue'));
Vue.component('manage-task', require('./components/projects/manage_task.vue'));
Vue.component('reviewjob-component', require('./components/review/reviewjob.vue'));
Vue.component('review-company-component', require('./components/companies/review.vue'));
Vue.component('active-bids', require('./components/projects/active_bids.vue'));
Vue.component('manage-bidders', require('./components/projects/manage_bidders.vue'));
Vue.component('all-users', require('./components/admin/users/users.vue'));
Vue.component('add-users', require('./components/admin/users/add_users.vue'));
Vue.component('apply-job', require('./components/jobs/apply.vue'));
Vue.component('apply-interview', require('./components/jobs/interview.vue'));
Vue.component('applicant-interview', require('./components/applicant/interview.vue'));
Vue.component('admin-signin', require('./components/admin/signin.vue'));
Vue.component('job-category-add', require('./components/admin/job_categories/add.vue'));
Vue.component('job-category-all', require('./components/admin/job_categories/all.vue'));
Vue.component('real-time-chat-component', require('./components/chats/realChat.vue'));
Vue.component('real-time-composer-component', require('./components/chats/chatComposer.vue'));
Vue.component('real-time-user-component', require('./components/chats/chat-users.vue'));
Vue.component('real-time-messages-all-component', require('./components/chats/chatMessageContainer.vue'));
Vue.component('reset-component', require('./components/passwordReset.vue'));
Vue.component('password-component', require('./components/showPasswordReset.vue'));
Vue.component('item-category-component',require('./components/admin/items/categories/index.vue'));
Vue.component('item-sub-category-component',require('./components/admin/items/sub/index.vue'));
Vue.component('sell-product-component',require('./components/products/sell/index.vue'));
Vue.component('edit-product-component',require('./components/products/sell/edit-post.vue'))
Vue.component('brands-component',require('./components/admin/brand/index.vue'));
Vue.component('models-component',require('./components/admin/models/index.vue'));
Vue.component('item-category-brand-component',require('./components/admin/itemCategoryBrand/index.vue'));
//manage-users and plans
Vue.component('user-plan-component',require('./components/admin/manage-users/plans/index.vue'));
Vue.component('user-plan-feature-component',require('./components/admin/manage-users/plan-feature/index.vue'));
//subscriptions
Vue.component('user-subscriptions',require('./components/subscriptions/index.vue'));
Vue.component('checkout-component',require('./components/subscriptions/checkout.vue'));
//settings
Vue.component('site-settings-component',require('./components/admin/settings/site.vue'));
Vue.component('feature-component',require('./components/admin/items/feature/all.vue'));
Vue.component('item-feature-component',require('./components/admin/items/item_feature/all.vue'));
///products
Vue.component('approve-product-component',require('./components/admin/items/approve/index.vue'));
//buy products
Vue.component('buy-product-component',require('./components/products/buy/index.vue'));
//user posts
Vue.component('my-products-components',require('./components/products/sell/posts.vue'));
Vue.component('product-buy-detail-components',require('./components/products/buy/details/detail.vue'));
//product bookmarks
//contract components
Vue.component('send-contract-component',require('./components/contracts/send.vue'));
Vue.component('bookmarks-component',require('./components/bookmarks/index.vue'));

//admin manage users
Vue.component('users-all-component',require('./components/admin/manage-users/users/index.vue'));

//dashboard components
Vue.component('dashboard-notifications-component',require('./components/dashboard/notifications.vue'));
Vue.component('dashboard-profile-component',require('./components/dashboard/profile-view.vue'));
Vue.component('dashboard-user-notes',require('./components/dashboard/user-notes.vue'));
//interview postpone
Vue.component('interview-postpone-component',require('./components/postpone/index.vue'));

//contracts
Vue.component('contracts-index-component',require('./components/contracts/index.vue'));

//notificaitons
Vue.component('all-notifications',require('./components/notifications.vue'));

//realtime messages
Vue.component('all-messages-component',require('./components/messages.vue'))
//companies
Vue.component('all-companies-component',require('./components/companies/index.vue'));
//blog post creating
Vue.component('blog-post-create',require('./components/admin/blog/create.vue'));
Vue.component('blog-post-index',require('./components/admin/blog/index.vue'));
Vue.component('blogs-all-component',require('./components/blog/index.vue'));
Vue.component('blog-comments-component',require('./components/blog/comments.vue'));
Vue.component('profile-page-component',require('./components/me/profile.vue'));
//homepage searches
Vue.component('home-search',require('./components/welcome/search.vue'));
Vue.component('home-search-filters',require('./components/search/search.vue'));
Vue.component('app-footer-component',require('./components/products/buy/footer.vue'));
//manage companies
Vue.component('app-admin-manage-companies',require('./components/admin/companies/verify.vue'));
//cs-my-profile-template
Vue.component('cs-my-profile-component',require('./components/profile/index.vue'));
Vue.component('app-find-employee',require('./components/profile/find/index.vue'));
//admin reports
Vue.component('admin-job-reports',require('./components/admin/reports/jobs/index.vue'));
//admin dashboard
Vue.component('admin-dashboard-component',require('./components/admin/dashboard/index.vue'));
//admin notifications
Vue.component('admin-notifications-component',require('./components/admin/notifications/index.vue'));
Vue.component('admin-profile-component',require('./components/admin/profile/index.vue'));

const app = new Vue({
    el: '#app',
    store
});
