<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 17/10/2018
 * Time: 12:18
 */

namespace App\Utilities;


use App\Models\Interview;
use App\Models\WorkApplication;
use Carbon\Carbon;

class Utils
{
    public static function humanDate($date)
    {
        $dat = Carbon::parse($date);
        return $dat->diffForHumans();
    }
    public static function formatAmount($amount){
        if($amount >= 1000)
            return 'GHS '.$amount/1000 . 'K';
        return 'GHS '.$amount;
    }
    public static function formatDate($date){
        $dat = Carbon::parse($date);
        //return $dat->isoFormat('MMMM Do YYYY, h:mm:ss a');
        return $dat->format('M D Y, h:m a');

    }
    public static function interviewCount(){
        return count(Interview::where('user_id',auth()->id())->where('status','pending')->get());
    }
    public static function formatLocation($location){
        $loc = explode(',',$location);
        if(count($loc) > 0){
            if(count($loc) > 2){
                return $loc[1];
            }else{
                $new_loc = explode(' ',$loc[0]);
                if(count($new_loc) > 0){
                    return $new_loc[0];
                }else{
                    return $loc[0];
                }
            }
        }else{
            return '<unspecified>';
        }
    }
}
