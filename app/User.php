<?php

namespace App;

use App\Model\UserChat;
use App\Models\Chat;
use App\Models\JobCategories;
use App\Models\LoginLog;
use App\Models\Product;
use App\Models\Profile;
use App\Models\Review;
use App\Models\UserContact;
use App\Models\UserNote;
use App\Models\VerifyUser;
use App\Models\Work;
use App\Models\WorkApplication;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Traits\SubscriptionTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait,SubscriptionTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','firstname','lastname','status','slug','role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $with = ['profile','categories','verifyUser','contacts'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function profile(){
        return $this->hasOne(Profile::class);
    }
    public function notes(){
        return $this->hasMany(UserNote::class)->orderBy('priority','desc');
    }
    public function works(){
        return $this->hasMany(Work::class)->latest();
    }
    public function online(){
        return Cache::has('online-'.$this->id);
    }
    public function categories(){
        return $this->belongsToMany(JobCategories::class, 'user_categories', 'user_id', 'job_category_id');
    }
    public function allApplications(){
        return $this->hasMany(WorkApplication::class);
    }
    public static function checkOnlineStatus($id){
        return Cache::has('online-'.$id);
    }
    public function getOnlineStatusAttribute(){
        return Cache::has('online-'.$this->id);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applications(){
        return $this->hasMany(WorkApplication::class);
    }
    public function isAdmin(){
        return $this->hasRole('admin');
    }
    public function verifyUser(){
        return $this->hasOne(VerifyUser::class);
    }
    public function projects(){
        return $this->hasMany('App\Models\Project\Project');
    }

    public function reviewcompany(){
        return $this->hasMany('App\Models\ReviewCompany');
    }

    public function project_application(){
        return $this->hasMany('App\Models\Project\ProjectApplication');
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
    public function getNameAttribute(){
        return $this->firstname .' '.$this->lastname;
    }
    public function contacts(){
        return $this->hasMany(UserContact::class);
    }
    public function log(){
        return $this->hasOne(LoginLog::class);
    }
    public function chats(){
        return $this->hasMany(UserChat::class)
            ->orderBy('updated_at','desc');
    }
    public function receivesBroadcastNotificationsOn()
    {
        return 'users.'.$this->id;
    }
    public function unreadMessages(){
        return Chat::whereNull('read_at')
            ->where('receiver_id',$this->id)
            ->orderBy('created_at','desc')
            ->get();
    }

    protected $appends = ['name','online_status'];

    //reviews
    public function reviews(){
        return $this->morphMany(Review::class,'reviewable');
    }
}
