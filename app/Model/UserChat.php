<?php

namespace App\Model;

use App\Models\Chat;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserChat extends Model
{
    protected $appends = ['unread_messages','last_chat'];
    public function user(){
        return $this->belongsTo(User::class,'chat_id')
            ->select(['id','slug','email','firstname','lastname']);
    }
    protected $with = ['user'];
    public function getLastChatAttribute(){
        return Carbon::parse($this->updated_at)->diffForHumans();
    }
    public function getLastMessageAttribute($value){
        if(!$value){
            return $value;
        }else{
            $end = ' ...';
            $max_length = 25;
            $value = str_replace('<br />',' ',$value);
            if (strlen($value) > $max_length || $value == '') {
                $value = str_replace('<br/>','',$value);
                $words = preg_split('/\s/', $value);
                $output = '';
                $i      = 0;
                while (1) {
                    $length = strlen($output)+strlen($words[$i]);
                    if ($length > $max_length) {
                        break;
                    }
                    else {
                        $output .= " " . $words[$i];
                        ++$i;
                    }
                }
                $output .= $end;
            }
            else {
                $output = $value;
            }
            return $output;
        }
    }
    public function getUnreadMessagesAttribute(){
        //$user = auth()->user();
        $chats  = Chat::whereNull('read_at')
            ->where('sender_id',$this->chat_id)
            ->where('receiver_id',$this->user_id)
            ->orderBy('created_at','asc')
            ->get();
        return count($chats);
    }

}
