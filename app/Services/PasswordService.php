<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 23/11/2018
 * Time: 07:47
 */

namespace App\Services;


class PasswordService
{
    public static function generateRandomString($length = 15)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}