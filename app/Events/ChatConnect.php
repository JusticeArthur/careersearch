<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatConnect implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public  $message;
    public $sender;
    public $receiver;
    public $sender_id;
    public $message_id;
    public $receiver_details;
    public function __construct($chat,$sender,$receiver)
    {
        $this->message = $chat->chat;
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->sender_id = $sender->id;
        $this->message_id = $chat->id;
        $this->receiver_details = User::with(['chats'=>function($query) use($receiver){
            $query->where('chat_id','<>',$receiver);
        }])->where('id',$receiver)
            ->get()->pluck('chats');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chatConnect.'.$this->receiver);
    }
}
