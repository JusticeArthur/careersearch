<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SellProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_category_id'=>'required',
            'item_sub_category_id'=>[
                function($attribute, $value, $fail){
                    return $fail('Item Sub Category','The Item Sub Category Field is required');
                }
            ]
        ];
    }
    public function messages()
    {
        return[
            'item_category_id.required'=>'The Item Category Field is required'
        ];
    }
}
