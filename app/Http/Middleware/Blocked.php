<?php

namespace App\Http\Middleware;

use Closure;

class Blocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->blocked){
            auth()->logout();
            return redirect('/')->with(['error'=>'You have been blocked please contact the admin']);
        }else{
            return $next($request);
        }
    }
}
