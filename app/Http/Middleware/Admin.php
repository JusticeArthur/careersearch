<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if(auth()->user()->hasRole('admin'))
                return $next($request);
            return redirect()->back()->with([
                'message'=>'Please You are not an Administrator'
            ]);
        }else{
            return redirect()->route('admin.getLogin');
        }
    }
}
