<?php

namespace App\Http\Middleware;

use Closure;

class SubscriptionPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->current_subscription){
            return redirect()->back()->with(['error'=>'Please you have to subscribe to a plan to access this area']);
        }else{
            return $next($request);
        }
    }
}
