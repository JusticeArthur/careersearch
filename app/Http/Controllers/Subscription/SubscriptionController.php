<?php

namespace App\Http\Controllers\Subscription;

use App\Jobs\SubscriptionJob;
use App\Models\MobileOperator;
use App\Models\Plan;
use App\Models\SiteSetting;
use App\Models\SubscriptionTransaction;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::with(['features'])->get();
        return view('dashboard.subscriptions.subscriptions',[
            'plans'=>$plans
        ]);
    }
    public function checkout($slug,$type){
        /*dd([
            'slug'=>$slug,
            'type'=>$type
        ]);*/
        $plan = Plan::where('slug',$slug)->first();
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        $mobileOps = MobileOperator::with('codes')->get();
        if(isset($plan) && in_array($type,['month','year'])){
            return view('dashboard.subscriptions.checkout',[
                'plan'=>$plan,
                'type'=>$type,
                'settings'=>$settings,
                'operators'=>$mobileOps
            ]);
        }
        return redirect()->back();
    }
    public function processOrder($slug,$type){
        //make api call here
        //all payments are processed here
        //after a successful request, set $user->subscribed = true;
        return view('dashboard.subscriptions.confirmation',[
            'slug'=>$slug,
            'type'=>$type
        ]);
    }
    public function processPayment(Request $request){
        $url = 'https://client.teamcyst.com/api_call.php';
        $additional_headers = array(
            'Content-Type: application/json'
        );
        $sender = str_replace(' ','',$request->tel);
       // dd($request);
        $orderId = str_replace('-','',Str::uuid());
        $data = array(
            "price"=> number_format((float)$request->amount,2),
            //"price"=> 1,
            "network"=> $request->operator,
            "recipient_number"=> "0544900856",
            "sender"=> $sender,
            "option"=> "rmtm",
            "apikey"=> "85f34594ed980058a11497dc9f1ac7e6d09bf015",
            "orderID"=>$orderId
        );
      //  dd($data);

        $data = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // $data is the request payload in JSON format
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);

        $server_output = curl_exec($ch);
        //site settings
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        //get mobile operator
        $mobile_operator = MobileOperator::where('value',$request->operator)->first();
        //create a new subscription transaction
        $currentUser = auth()->user();
        $transaction = new SubscriptionTransaction();
        $transaction->user_id = $currentUser->id;
        $transaction->order_id = $orderId;
        $transaction->amount = number_format((float)$request->amount,2);
        $transaction->tel = $sender;
        $transaction->mobile_operator_id = $mobile_operator->id;
        $transaction->charges = (float)$settings->vat;
        $transaction->plan_id = $request->plan_id;
        $transaction->plan_type = $request->type;
        $transaction->save();
        return $server_output;
    }
    //after that get the invoice
    public function invoice($slug,$type){
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        $plan = Plan::where('slug',$slug)->first();
        $amount = 0;
        if($type == 'amount'){
            $amount = $plan->monthly_amount;
        }else{
            $amount = $plan->yearly_amount;
        }
        $vat = number_format((((float)$settings->vat / 100)) * $amount,2);
        return view('dashboard.subscriptions.invoice',[
            'plan'=>$plan,
            'amount'=>$amount,
            'type'=>$type,
            'vat'=>$vat,
            'total'=> number_format($amount + $vat,2),
            'settings'=>$settings
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
