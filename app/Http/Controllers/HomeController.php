<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use App\Models\InterviewPostpone;
use App\Models\JobCategories;
use App\Models\ProductBookmark;
use App\Models\ProfileView;
use App\Models\Review;
use App\Models\Subscription;
use App\Models\UserNote;
use App\Models\WorkBookmark;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use SanderVanHooft\Invoicable\Invoice;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return voidProcessPodcast
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('custom_auth');
    }

    public function index()
    {
        //get profile views
        $user = auth()->user();
        $views = ProfileView::where('viewable_type','App\Models\ProfileViews')
            ->where('viewable_id',auth()->user()->id)
            ->where("created_at",">", Carbon::now()->subYears(1));
            $data = $views->get()
            ->groupBy(function ($query){
                return Carbon::parse($query->created_at)->format('M');
            });
        $month_counts = [];
        $months =[];
        foreach ($data->toArray() as $key=>$value){
            array_push($months,$key);
            array_push($month_counts,count($data->toArray()[$key]));
        }
        $currentMonthNumber = Carbon::now()->month;
        $currentMonth = Carbon::now()->month($currentMonthNumber)->format('M');
        $monthViews = 0;
        foreach ($months as $key=>$month){
            if($month == $currentMonth){
                $monthViews = $month_counts[$key];
            }
        }
       // $invoices
        $subcriptions = auth()->user()->all_subscriptions;
        return view('home',
            [
                'subscriptions'=>$subcriptions,'months'=>json_encode($months),
                'counts'=>json_encode($month_counts),
                'month_views'=>$monthViews,
                'notes'=>$user->notes
            ]);
    }
    //show invoice on the dashboard details
    //dashboard notes
    public function addNote(Request $request){
        $note = new UserNote();
        $note->priority = $request->priority;
        $note->user_id = auth()->user()->id;
        $note->message = $request->message;
        $note->save();
        return $note;
    }
    public function deleteNote($id){
        UserNote::find($id)->delete();
        return response([],200);
    }
    public function updateNote(Request $request,$id){
        $note = UserNote::find($id);
        $note->priority = $request->priority;
        $note->message  = $request->message;
        $note->save();
        return $note;
    }
    //end of dashboard notes
    public function invoice(){
        $reference = Input::get('ref');
        $invoice = Invoice::findByReference($reference);
        if(!$invoice){
            return redirect()->back();
        }
        $subscription = Subscription::find($invoice->invoicable_id);
        $plan = $subscription->plan;
        $user = $subscription->user;
        return view('dashboard.subscriptions.invoice',[
           'subscription'=>$subscription,
            'user'=>$user,
            'plan'=>$plan,
            'invoice'=>$invoice
        ]);
    }
    //return settings
    public  function settings(){
        $company = auth()->user()->profile->company;
        /*echo $company->toJson();
        dd();*/
        $categories = JobCategories::all();
        if(count(auth()->user()->unreadNotifications) > 0){
            foreach (auth()->user()->unreadNotifications as $notification) {
                if($notification->type == 'App\Notifications\UpdateProfile'){
                    $notification->markAsRead();
                }
            }
        }

        return view('dashboard.settings',[
            'skills'=>auth()->user()->profile->skills,
            'tags'=>auth()->user()->profile->tags,
            'user'=>auth()->user(),
            'attachments'=>auth()->user()->profile->attachments,
            'profile'=>auth()->user()->profile,
            'categories'=>$categories,
            'company'=>$company
        ]);
    }
    //return messages
    public function messages(){
        $chat = 0;
        if(Session::has('message_id')){
            $chat = Session::get('message_id');
        }
        $user = auth()->user();
        $chats = User::with(['chats'=>function($query) use($user){
            $query->where('chat_id','<>',$user->id);
        }])->where('id',$user->id)->get();
      //  dd($chats->pluck('chats')->toArray());
        return view('dashboard.messages',[
            'users'=>$chats->pluck('chats'),
            'currentUser'=>$user,
            'audio'=>asset('audios/alert.mp3'),
            'today'=>Carbon::now()->format('M d, Y'),
            'chat'=>$chat
        ]);
    }
    //return bookmarks
    public function bookmarks(){
        $products = ProductBookmark::with('product.avatars')
            ->where('user_id',auth()->user()->id)
            ->where('bookmarked',true)
            ->orderBy('updated_at','desc')
            ->get();
        $works = WorkBookmark::with(['work'])->where('user_id',auth()->user()->id)
            ->where('bookmarked',true)
            ->orderBy('updated_at','desc')
            ->get();
        /*echo $works->toJson();
        dd();*/
        return view('dashboard.bookmark',[
            'products'=>$products,
            'works'=>$works
        ]);
    }
    public function reviews($paginate = null){
        //$works = Contract::with(['work'])->where('status','accepted')->paginate(5)->toArray();

        $works = DB::table('contracts')
            ->where('employer_id',auth()->id())
            ->where('status','accepted')
            ->select('work_id')
            ->distinct()->get()
            ->toArray();
        $work_ids = [];
        foreach ($works as $work){
            array_push($work_ids,$work->work_id);
        }
        $jobs = DB::table('works')->whereIn('id',$work_ids)->select('id','slug','title')->paginate(5);
       // dd($jobs);
        if(!$paginate){
            return view('dashboard.review',[
                'works'=>json_encode($jobs)
            ]);
        }else{
            return $jobs;
        }

    }
    public function loadEmployeeForReview($id){
        $contracts = DB::table('contracts')
            ->where('status','accepted')
            ->where('work_id',$id)
            ->select('employee_id')
            ->distinct()
            ->get();
        $user_ids = [];
        foreach ($contracts as $contract){
            array_push($user_ids,$contract->employee_id);
        }
        $users =  User::whereIn('id',$user_ids)->get()->toArray();
        foreach ($users as $key=>$user){
            $review = Review::where('work_id',$id)->where('reviewable_id',$user['id'])->first();
            if(isset($review)){
                $users[$key]['review'] = $review;
            }else{
                $users[$key]['review'] = ['rating'=>0,'comment'=>'','recommendation'=>0];
            }
        }
        return $users;
    }

    public function saveReview(Request $request){
        $review = new Review();
        $review->comment = $request->comment;
        $review->rating = $request->rating;
        $review->recommendation =  $request->recommendation;
        $review->work_id = $request->work_id;
        $user = User::find($request->user_id);
        $user->reviews()->save($review);
        return $review;
    }
    public function updateReview(Request $request,$id){
        $review = Review::find($id);
        $review->comment = $request->comment;
        $review->rating = $request->rating;
        $review->recommendation =  $request->recommendation;
        $review->save();
        return $review;
    }


    public function postpone(){
        $postpones = InterviewPostpone::with(['interview'])->where('employer_id',auth()->user()->id)
            ->where('status','pending')
            ->get();
       /* echo $postpones->toJson();
        dd();*/
        return view('dashboard.postpone.index',[
            'postpones'=>$postpones
        ]);
    }
    public function contract(){
        $contracts = Contract::with(['work'])->where('employee_id',auth()->id())->where('status','pending')
            ->get();
        /*echo $contracts->toJson();
        dd();*/
        return view('dashboard.contracts.index',[
            'contracts'=>$contracts
        ]);
    }
}
