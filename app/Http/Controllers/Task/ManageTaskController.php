<?php

namespace App\Http\Controllers\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project\Project;
use App\Models\JobCategories;
use App\Models\Project\Project_requirement;
class ManageTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = JobCategories::all();
        return view("dashboard.projects.post_project",[
            'categories'=>$categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
/*
        $project = new Project();
        $project->name = title_case($request->name);
        $project->category_id = $request->category;
        $project->min_budget = $request->min_budget;
        $project->max_budget = $request->max_budget;
        $project->user_id = auth()->user()->id;
        $project->slug = str_slug($request['name']).'-'.uniqid();
        //$project->company_id = auth()->user()->profile->company->id;
        $project->location = $request->location;
        $project->duration = $request->duration_number." ".$request->duration_type;
        $project->expiry = $request->expiry_date;
        $project->budget_type = $request->budget_type;
        $project->description = $request->description;
        $project->save();



        if($request['requirements']){
            foreach ($request['requirements'] as $item){
                $requirement = new Project_requirement();
                $requirement->projects_id = $project->id;
                $requirement->name = $item;
                $requirement->save();
            }
        }*/
        if($request->has('files')){
           return $request;
        }

      return "kio";
        }

        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
