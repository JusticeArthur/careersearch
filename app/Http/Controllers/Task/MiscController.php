<?php

namespace App\Http\Controllers\Task;
use App\Models\Project\Project;
use App\Models\Project\ProjectApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MiscController extends Controller
{
    public function managetask()
    {    
        
        //$id = auth()->user()->id;
        //$project = Project::all()->where('user_id',$id);
         return view('dashboard.projects.manage_project',[
             'project'=>auth()->user()->projects
         ]);
    }

    public function managebidders($id)
    {
        $project = Project::find($id);
        return view('dashboard.projects.manage_bidders',[
            'project'=>$project
        ]);
    }

    public function activebids()
    {
        $application = DB::table('project_applications')
        ->where('project_applications.user_id',auth()->user()->id)
        ->get();
        return $application;
       
    }

    public function project_apply($id){
        $project = Project::find($id);
        return view('dashboard.projects.apply',[
            'project'=>$project
        ]);
    }

    public function apply(Request $request)
    {
        $application = new ProjectApplication;
        $application->project_id = $request->id;
        $application->user_id = auth()->user()->id;
        $application->delivery_time =$request->duration_number." ".$request->duration_type;
        $application->rate = $request->rate;
        $application->save();
    }

    public function accept_project(Request $request)
    {
        $accept_application = ProjectApplication::find($request->id);
        $accept_application->status=2;
        $accept_application->save();
    }
}
