<?php

namespace App\Http\Controllers\Task;
use App\Models\Project\Project;
use App\Models\JobCategories;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\JobCategory;

class FindController extends Controller
{
    public function index(){
        $category = JobCategories::all();
        $project = Project::all();
        return view("dashboard.projects.find",[
            'project'=>$project,
            'category'=>$category
        ]);
    } 


    protected $data;
    protected $item;
    public function filter(Request $request){
        $titles = $request->title;
        $category = $request->category;
        $salary = explode(',',$request->salary);
        //dd($title);
            $now = Carbon::now();
            return Project::orderby('created_at','DESC')
                ->when($titles,function ($query,$titles){
                    //$this->item = $query;
                    foreach ($titles as $title){
                        $this->item = $query->orwhere('name', 'like', '%' . $title . '%');
                        $query = $this->item;
                    }
                    return $query;
                })
                ->when($category,function ($query,$category){
                    //$this->item = $query;
                    foreach ($category as $category){
                        $this->item = $query->orwhere('category_id', 'like', '%' . $category . '%');
                        $query = $this->item;
                    }
                    return $query;
                })

                ->where('location', 'like', '%' . $request['location'] . '%')
            
             
               ->where(function($query) use ($salary){
                    return $query->where('min_budget','>=',$salary[0])
                        ->where('max_budget','<=',$salary[1]);
                })
               // ->whereDate('expiry','>=',$now)
                ->paginate(10);
    }
}
