<?php

namespace App\Http\Controllers\Applicant;

use App\Models\Interview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InterviewController extends Controller
{
    public function getInterviews($id = null,$noty = null){
        $interviews = Interview::where('user_id',auth()->user()->id)
            ->where('status','pending')
            ->get();
        if($id)
        DB::table('notifications')->where('id',$id)->update([
            'read_at'=>Carbon::now()
        ]);
        return view('applicants.interview',[
            'interviews'=>$interviews,
        ]);
    }
}
