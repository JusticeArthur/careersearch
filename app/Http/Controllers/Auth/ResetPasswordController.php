<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    public function showResetForm(){
        return view('passwords.email');
    }
    protected function isTokenValid($token){
        $date = DB::table('reset_passwords')->
        where('token',$token)->pluck('expiry')->first();
        $dateTime = Carbon::parse($date);
        $now = Carbon::now();
        return $now->gte($dateTime);
        /*if($now->lte($dateTime))
            return false;
        return true;*/
    }
    public function reset(Request $request){
        $rules = [
            'token'=>['required',
                function($attribute, $value, $fail) use ($request){
                    if($this->isTokenValid($value))
                        return $fail('Please your reset token has expired');

                }],
            'password'=>'min:6|required|confirmed'
        ];
        $this->validate($request,$rules);
            Auth::logout();
            $token = $request->token;
            $email = DB::table('reset_passwords')->
            where('token',$token)->pluck('email')->first();
            DB::table('users')->where('email',$email)->update([
                'password'=>bcrypt($request->password)
            ]);

    }
}
