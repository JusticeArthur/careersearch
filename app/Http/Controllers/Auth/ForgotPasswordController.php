<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\ResetPasswordNotification;
use App\Services\PasswordService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class ForgotPasswordController extends Controller
{
    public function showLinkRequestForm(){
        return view('passwords.reset');
    }

    public function sendResetLinkEmail(Request $request){
        $rules = ['email' =>'required|email'];
        $this->validate($request,$rules);
        $user = User::where('email', $request->email)->first();
        $now = Carbon::now();
        if($user != null){
            $token = PasswordService::generateRandomString();

            $userToken = DB::table('password_resets')->where('email', $request->email)->first();
            if ($userToken) {
                DB::table('password_resets')->where('email', $request->email)->update([
                    'token' => $token,
                ]);
            } else {
                DB::table('reset_passwords')->insert([
                    'email' => $user->email,
                    'token' => $token,
                    'expiry'=>$now->addMinutes(40)
                ]);
            }
            Notification::send($user,new ResetPasswordNotification($token,$user));
            return response()->json([
                'message'=>'Please check your mail to reset password'
            ],200);
        }
        return response()->json([
            'message'=>'Please the email doesn\'t match our records'
        ],400);
    }
}
