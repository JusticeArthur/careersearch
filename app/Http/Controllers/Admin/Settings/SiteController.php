<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\Country;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        $currencies = file_get_contents(asset('files/currencies.json'),false, stream_context_create($arrContextOptions));
        $countries = Country::all();
        return view('admin.settings.site',[
            'settings'=>$settings,
            'countries'=>$countries,
            'currencies'=>$currencies
        ]);
    }
    public function getStates($id){
        $country = Country::find($id);
        return $country->states;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd(config('app.custom_id'));
        $data['contact'] = $request->contact;
        $data['alt_contact'] = $request->alt_contact;
        $data['city'] = $request->city;
        $data['address'] =$request->address;
        $data['discount'] = $request->discount;
        $data['vat'] = $request->vat;
        $data['discount'] = $request->discount;
        $data['sms_user_name'] = $request->sms_user_name;
        $data['sms_password'] = $request->sms_password;
        $data['sms_display_name'] = $request->sms_display_name;
        $data['country_id'] = $request->country_id;
        $data['state_id'] = $request->state_id;
        $data['currency'] = $request->currency;
        $data['custom_id'] = config('app.customId');

        $settings  = SiteSetting::where('custom_id',config('app.customId'))->first();
        if(isset($settings)){
            SiteSetting::where('custom_id',config('app.customId'))
                ->update($data);
        }else{
            SiteSetting::create($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
