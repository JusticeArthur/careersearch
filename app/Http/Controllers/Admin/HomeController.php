<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ProcessPodcast;
use App\Models\ItemCategory;
use App\Models\ItemSubCategory;
use App\Models\Plan;
use App\Models\Profile;
use App\Models\Work;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function getLogin(){
        return view('admin.login');
    }
    public function adminLogin(Request $request){
        $rules = [
          'email'=>'email|required',
          'password'=>'required'
        ];
        $this->validate($request,$rules);
        $url = null;
        Auth::logout();
        if(Auth::attempt(['email'=>$request['email'],'password'=>$request['password']])){
            $url = '/admin/dashboard';
            if(Auth::user()->isAdmin()){
                return response()->json([
                    'url'=>$url
                ],200);
            }
            Auth::logout();
            return response()->json([
                'error'=>'You are not an Administrator',
            ],422);
        }
        if($url == null){
            return response()->json([
                'error'=>'Your Credentials do not match our records',
            ],422);
        }
    }
    public function getDashboard(){
        $plans = Plan::with(['subscriptions'])->get();
        $plan_subscriptions = [];
        $all_plans =[];
        $all_subs = [];
        foreach ($plans as $key=>$plan){
            array_push($all_plans,$plan->name);
            array_push($all_subs,count($plan->subscriptions));
        }
        $plan_subscriptions['plans'] = $all_plans;
        $plan_subscriptions['subs'] = $all_subs;
       // dd($plan_subscriptions);

        //jobs over view
        $jobs  = Work::withCount('applications')->get()->toArray();
        if(count($jobs) > 0) {
            foreach ($jobs as $key => $job) {
                $shortlist_count = 0;
                $interview_count = 0;
                $employed_count = 0;
                foreach ($job['applications'] as $application) {
                    if ($application['status'] === 'shortlisted') {
                        $shortlist_count++;
                    } else if ($application['status'] === 'interviewed') {
                        $shortlist_count++;
                        $interview_count++;
                    } else if ($application['status'] === 'employed') {
                        $shortlist_count++;
                        $interview_count++;
                        $employed_count++;
                    }
                }
                $jobs[$key]['shortlist_count'] = $shortlist_count;
                $jobs[$key]['interview_count'] = $interview_count;
                $jobs[$key]['employed_count'] = $employed_count;
            }
        }
        $job_overview =[];
        if(count($jobs) > 0){
            $total = 0;
            $applicants = 0;
            $shortlisted = 0;
            $interviewed = 0;
            $employed = 0;
            foreach ($jobs as $job){
                $total ++;
                $applicants += $job['applications_count'];
                $shortlisted += $job['shortlist_count'];
                $interviewed += $job['interview_count'];
                $employed += $job['employed_count'];
            }
            $overview_labels = ['jobs','applicants','shortlisted','interviewed','employed'];
            $overview_data = [$total,$applicants,$shortlisted,$interviewed,$employed];
            $job_overview['labels'] = $overview_labels;
            $job_overview['data'] = $overview_data;
        }

        //highest rated employees
        $profiles = DB::table('profiles')->orderBy('overall_rating','desc')
            ->where('overall_rating','>','2')
            ->leftJoin('users','profiles.user_id','users.id')
            ->select('profiles.overall_rating','users.firstname','users.lastname')
            ->take(5)->get();
        //dd($profiles->toArray());
        $users_count = User::all()->count();
        $item_categories_count = ItemCategory::all()->count();
        $item_sub_categories_count = ItemSubCategory::all()->count();
        return view('admin.home',[
            'item_sub_categories_count'=>$item_sub_categories_count,
            'item_categories_count'=>$item_categories_count,
            'users_count'=>$users_count,
            'plans'=>json_encode($plan_subscriptions),
            'jobOverview' =>json_encode($job_overview),
            'top_ratings'=>$profiles
        ]);
    }
}
