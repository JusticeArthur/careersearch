<?php

namespace App\Http\Controllers\Admin\Company;

use App\Mail\CompanyRegistered;
use App\Models\Company;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ManageCompanyController extends Controller
{
    public function index(){
        $companies = Company::with(['profile','profile.user','state','state.ucountry'])
            ->where('verified',false)
            ->get();
        return view('admin.companies.verify',[
            'companies'=>$companies
        ]);
    }
    // 'https://rapps.gegov.gov.gh/tripsutil/tindetails',
    public function verify(Request $request){
        Company::where('id',$request->companyId)
            ->update([
                'verified'=>true
            ]);
        Mail::to($request->email)->send(new CompanyRegistered($request->company,$request->user));
    }
}
