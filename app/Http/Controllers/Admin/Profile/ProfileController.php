<?php

namespace App\Http\Controllers\Admin\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function index(){
        return view('admin.profile.index',[
            'user'=>auth()->user()
        ]);
    }
    public function update(Request $request){
        $object = json_decode($request->user,true);
        $profile = $object['profile'];
        $user['firstname'] = $object['firstname'];
        $user['lastname'] = $object['lastname'];
        //$user['email'] = $object['firstname'];
        auth()->user()->update($user);
        auth()->user()->profile->update($profile);

        //update avatar
        if($request->hasFile('avatar')){
            $originalExtension = $request->avatar->getClientOriginalExtension();
            Image::make($request->file('avatar'))->fit(150, 150)->save('avatars/admin/'.auth()->user()->profile->url .'.'. $originalExtension);
            auth()->user()->profile->avatar = auth()->user()->profile->url .'.'. $originalExtension;
            auth()->user()->profile->save();
         }
        if($request->hasFile('avatar')){
            $request->session()->flash('success','Profile Updated Successfully');
            return response()->json([],201);
        }
        return response()->json([],200);
    }
}
