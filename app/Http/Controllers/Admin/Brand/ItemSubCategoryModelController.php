<?php

namespace App\Http\Controllers\Admin\Brand;

use App\Models\Brand;
use App\Models\ItemSubCategory;
use App\Models\ItemSubCategoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemSubCategoryModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        $items = ItemSubCategory::all();
        $data = ItemSubCategoryModel::with(['itemSubCategory','brand:id,name'])->orderBy('created_at','desc')->get();
        /*echo $data->toJSON();
        dd();*/
        return view('admin.itemCategoryBrand.index',[
            'data'=>$data,
            'brands'=>$brands,
            'items'=>$items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'brands'=>'required',
            'item_sub_categories'=>'required'
        ];
        $this->validate($request,$rules);
        foreach($request->item_sub_categories as $item_sub_category){
            foreach($request->brands as $model){
                $check = ItemSubCategoryModel::where('brand_id',$model['id'])
                    ->where('item_sub_category_id',$item_sub_category['id'])
                    ->first();
                if($check){
                    continue;
                }else{
                    $put = new ItemSubCategoryModel();
                    $put->item_sub_category_id = $item_sub_category['id'];
                    $put->brand_id = $model['id'];
                    $put->save();
                }
            }
        }
        return response()->json([
            'data'=>$data = ItemSubCategoryModel::with(['itemSubCategory','brand:id,name'])
                ->orderBy('created_at','desc')
                ->get()
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ItemSubCategoryModel::find($id)->delete();
    }
}
