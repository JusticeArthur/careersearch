<?php

namespace App\Http\Controllers\Admin\Brand;

use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\ItemSubCategoryModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BrandModel::with(['itemSubCategory'])->orderBy('created_at','desc')->get();
        $brands = Brand::orderBy('created_at','desc')->get();
        return view('admin.models.model',[
            'data'=>$data,
            'brands'=>$brands
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new BrandModel();
        $model->name = $request->name;
        $model->brand_id = $request->brand_id['id'];
        try{
            $model->save();
        }catch (QueryException $q){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>BrandModel::find($model->id)
        ],200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getModelCategories($id){
        $data = ItemSubCategoryModel::with(['itemSubCategory:id,name'])
            ->where('brand_id',$id)->get();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            BrandModel::where('id',$id)->update([
                'name'=>$request->name,
                'brand_id'=>$request->brand_id
            ]);
        }catch (QueryException $q){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>BrandModel::find($id)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BrandModel::find($id)->delete();
    }
}
