<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Post;
use App\Models\PostTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags  = PostTag::all();
        $posts = Post::all();
        return view('admin.posts.index',[
            'posts'=>$posts,
            'tags'=>$tags
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags  = PostTag::all();
        return view('admin.posts.create',[
            'tags'=>$tags
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store post here
        $data = json_decode($request->post,true);
        $data['user_id'] = auth()->id();
        $data['slug'] = (string) Str::uuid();
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
                    $slug = str_replace('-','',Str::uuid());
                    $extension = $avatar->extension();
                    $name = $avatar->storePubliclyAs('public/blog',$slug.'.'.$extension);
                    $name = explode('/',$name);
                    $data['avatar'] = end($name);
        }
        Post::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::find($id)->delete();
    }
}
