<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Role;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->get();
        //dd($users->toArray());
       return view('admin.users',[
           'users'=>$users,
       ]);
    }
    public function blockUser(Request $request,$id){
        $user = User::with('roles')->find($id);
        $user->blocked = !$user->blocked;
        $user->save();
        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
        'lastname' => 'required|alpha',
        'firstname'=> 'required|alpha',
         'email'=> 'required',
         'role'=>'required'
        ]);
        
        $user = new User;
        
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role_id = $request->role;
        $user->status = $request->role;
        $user->slug = $request->firstname.'-'.$request->lastname;
        $user->password = Hash::make("password");
        $user->save();
        $role = Role::find($request->role);
        $user->attachRole($role);

        $profile = new Profile;
        $profile->user_id  = $user->id;
        $profile->url = $request->firstname.'-'.$request->lastname.'-'.uniqid();
        $profile->save();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return(User::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        User::find($id)->update([
            'firstname'=>$request->firstname,
            'lastname'=>$request->lastname,
            'email'=>$request->email
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::find($id)->delete();
        return response()->json([
            'message'=>'successfully'
        ],200);
    }
}
