<?php

namespace App\Http\Controllers\Admin;

use App\Models\Work;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    protected $jobs;
    protected $sorted;
    function __construct()
    {
        $this->jobs  = Work::withCount('applications')->get()->toArray();
        if(count($this->jobs) > 0){
            foreach ($this->jobs as $key=>$job){
                $shortlist_count = 0;
                $interview_count = 0;
                $employed_count = 0;
                foreach ($job['applications'] as $application){
                    if($application['status'] === 'shortlisted'){
                        $shortlist_count ++;
                    }else if($application['status'] === 'interviewed'){
                        $shortlist_count ++;
                        $interview_count ++;
                    }else if($application['status'] === 'employed'){
                        $shortlist_count ++;
                        $interview_count ++;
                        $employed_count++;
                    }
                }
                $this->jobs[$key]['shortlist_count'] = $shortlist_count;
                $this->jobs[$key]['interview_count'] = $interview_count;
                $this->jobs[$key]['employed_count'] = $employed_count;
            }
            $this->sorted = collect($this->jobs)->sortByDesc(function ($job, $key) {
                return $job['applications_count'];
            });
        }
    }

    public function index(){
      //  dd($this->jobs);
        return view('admin.reports.index',[
            'data'=>$this->sorted->values()->toJson()
        ]);
    }
    public function filterRecords(Request $request){
        $isTitle = isset($request->title) ? true : false;
        $isExpiry = isset($request->expiry) ? true : false;
        $isDate = isset($request->date) ?true :false;
        $isSalary = isset($request->salary);
        $data = $this->sorted;
        if($isTitle){
            $data = $this->sorted
                ->reject(function($element) use ($request) {
                    return mb_strpos(strtolower($element['title']), strtolower($request->title)) === false;
                });
        }
        if($isExpiry){
            $data = $data->reject(function ($element) use ($request){
                $start = Carbon::parse($request->expiry[0]);
                $end = Carbon::parse($request->expiry[1]);
                $expiry = Carbon::parse($element['expiry']);
                return !$expiry->between($start,$end);
            });
        }
        if($isDate){
            $data = $data->reject(function ($element) use ($request){
                $start = Carbon::parse($request->date[0]);
                $end = Carbon::parse($request->date[1]);
                $date = Carbon::parse($element['created_at']);
                return !$date->between($start,$end);
            });
        }
        if($isSalary){
            $data = $data->reject(function ($element) use ($request){
                $min_salary = (float)explode(' ',str_replace(',','',$element['min_salary']))[1];
                $max_salary = (float)explode(' ',str_replace(',','',$element['max_salary']))[1];
                $input = $request->salary;
                //dd($input[0] >= $min_salary);
             //   dd( !($input[0] >= $min_salary && $input[1] <= $max_salary));
                //return ((float)$input[0] >= $min_salary && (float)$input[1] <= $max_salary);
                return !($min_salary >= (float)$input[0] && $max_salary <= (float)$input[1]);
            });
        }
//$date
        return $data->values()->all();
    }
}
