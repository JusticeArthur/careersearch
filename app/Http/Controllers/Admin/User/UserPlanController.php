<?php

namespace App\Http\Controllers\Admin\User;

use App\Models\Plan;
use App\Models\SiteSetting;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class UserPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Plan::all();
        return view('admin.manage_users.plans.index',[
            'data'=>$data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        $plan = new Plan();
        $plan->name = $request->name;
        $plan->monthly_amount = $request->monthly;
        $plan->yearly_amount = (float) $request->yearly * 1 - ((float)$settings->discount / 100);
        $plan->slug = str_replace('-','',Str::uuid());
        try{
            $plan->save();
        }catch (QueryException $e){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>$plan
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            Plan::where('id',$id)->update([
                'name'=>title_case($request->name),
                'monthly_amount'=>$request->month,
                'yearly_amount'=>(float) $request->year
            ]);
        }catch (QueryException $e){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>Plan::find($id)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Plan::find($id)->delete();
    }
}
