<?php

namespace App\Http\Controllers\Admin\User;

use App\Models\Plan;
use App\Models\PlanFeature;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPlanFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Plan::all();
        $data = PlanFeature::with(['plan:id,name'])->get();
       /* echo $data->toJson();
        dd();*/
        return view('admin.manage_users.plans.feature',[
            'data'=>$data,
            'items'=>$items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plan_feature = new PlanFeature();
        $plan_feature->name = $request->name;
        $plan_feature->plan_id = $request->plan_id;
        try{
            $plan_feature->save();
        }catch (QueryException $e){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>PlanFeature::with(['plan:id,name'])->where('id',$plan_feature->id)->first()
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try{
           PlanFeature::where('id',$id)->update([
               'name'=>$request->name,
               'plan_id'=>$request->plan_id
           ]);
       }catch (QueryException $e){
           return response()->json([
               'status'=>'error'
           ],501);
       }
        return response()->json([
            'data'=>PlanFeature::with(['plan:id,name'])->where('id',$id)->first()
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PlanFeature::find($id)->delete();
    }
}
