<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobCategory extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JobCategories::orderBy('created_at','desc')->get();
        return view('admin.job.category',[
            'data'=>$data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required'
        ];
        $this->validate($request,$rules);
        foreach ($request->name as $item){
            $category = new JobCategories();
            $category->name = $item['value'];
            $category->save();
        }
        return response()->json([
            'message'=>'Added Successfully'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return(JobCategories::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        JobCategories::where('id',$id)->update([
            'name'=>$request->name
        ]);
        return response()->json([
            'message'=>'Edited Successfully',
            'data'=>JobCategories::find($id)
        ],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JobCategories::find($id)->delete();
        return response()->json([
            'message'=>'successfully'
        ],200);
    }
}
