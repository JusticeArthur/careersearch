<?php

namespace App\Http\Controllers\Admin\Item;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(){
        return view('admin.items.approve');
    }
    public function approve(){
        $products = Product::with(['category:id,name','subCategory:id,name',
            'brand:id,name','model:id,name','country:id,name','state:id,name'])
            ->where('approved',false)->orderBy('created_at','desc')->get();
        /*echo $products->toJson();
        dd();*/
        return view('admin.items.approve',[
            'products'=>$products
        ]);
    }

    public function approveSingle($id){
        Product::where('id',$id)->update([
            'approved'=>true
        ]);
    }
    public function approveBulk(Request $request){
        foreach ($request->data as $datum){
            Product::where('id',$datum['id'])->update([
               'approved'=>true
            ]);
        }
        return Product::with(['category:id,name','subCategory:id,name','brand:id,name','model:id,name','country:id,name','state:id,name'])
            ->where('approved',false)->orderBy('created_at','desc')->get();
    }

}
