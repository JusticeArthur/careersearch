<?php

namespace App\Http\Controllers\Admin\Item;

use App\Models\ItemSubCategory;
use App\Models\ItemCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ItemCategory::all();

        $data = ItemSubCategory::orderBy('created_at','desc')->get();
        //echo $data->toJSON();
        return view('admin.items.sub',[
            'data'=>$data,
            'items'=>$items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sub = new ItemSubCategory();
        $sub->name = $request->name;
        $sub->item_category_id = $request->item_category_id;
        try{
            $sub->save();
        }catch(QueryException $q){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>ItemSubCategory::find($sub->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            ItemSubCategory::where('id',$id)->update([
                'name'=>$request->name,
                'item_category_id'=>$request->item_category_id
            ]);
        }catch (QueryException $q){
            return response([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>ItemSubCategory::find($id)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ItemSubCategory::find($id)->delete();
    }
}
