<?php

namespace App\Http\Controllers\Admin\Item;

use App\Models\Feature;
use App\Models\ItemSubCategory;
use App\Models\ItemFeature;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $items = Feature::all();
        $cat = ItemSubCategory::all();

        //$data = ItemFeature::orderBy('created_at','desc')->get();
        $data = DB::table('item_features')
        ->leftjoin('features', 'item_features.feature_id','features.id')
        ->leftjoin('item_sub_categories','item_features.item_category_id','item_sub_categories.id')
        ->select('item_features.*','item_sub_categories.name as catname','features.name as featname')
        ->get();
        //echo $data->toJSON();
        return view('admin.items.itemfeatures',[
            'data'=>$data,
            'items'=>$items,
            'cat'=>$cat
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $feat = new ItemFeature();
        $feat->feature_id = $request->feature_id;
        $feat->item_category_id = $request->item_category_id;
        try{
            $feat->save();
        }catch(QueryException $q){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return response()->json([
            'data'=>ItemSubCategory::find($feat->id)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    
       DB::table('item_features')->where('id',$id)->delete();
    }
}
