<?php

namespace App\Http\Controllers\Job;

use App\Models\JobCategories;
use App\Models\Work;
use App\Models\WorkTag;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FindController extends Controller
{
    public function index($paginate = 0){
        //get the jobs first
        $now = Carbon::now();
        $auth_profile = auth()->user()->profile;
        $jobs =  Work::where('status','<>','expired')->orderby('created_at','DESC')->whereDate('expiry','>=',$now)
            ->whereBetween('lat',[$auth_profile->lat - 0.1,$auth_profile->lat + 0.1])
            ->whereBetween('lng',[$auth_profile->lng - 0.1,$auth_profile->lng + 0.1])
            ->paginate(5);
            //get the tags of the jobs
        $tags = WorkTag::all();
        $markers = [];
        foreach ($jobs->items() as $item){
            array_push($markers,
                [
                    'position'=>['lat'=>$item->lat,'lng'=>$item->lng],
                    'title'=>$item->title,
                    'type'=>$item->type
                ]);
        }
        $center = [
          'lat'=>auth()->user()->profile->lat,
          'lng'=>auth()->user()->profile->lng,
        ];
        $categories = JobCategories::all();
        if(!$paginate){
            return view('dashboard.work.find',[
                'jobs'=>$jobs->toJson(),
                'center'=>json_encode($center),
                'markers'=>json_encode($markers),
                'categories'=>$categories,
                'tags'=>$tags
            ]);
        }else{
            return response()->json([
                'jobs'=>$jobs,
                'markers'=>$markers
            ]);
        }

    }
    protected $data;
    protected $item;
    public function filter(Request $request){
        $internship = ($request->intership == null && $request->intership == false) ? false:true;
        $full_time = ($request->full_time == null && $request->full_time == false) ? false:true;
        $part_time = ($request->part_time == null && $request->part_time == false) ? false:true;
        $temporary = ($request->temporary == null && $request->temporary == false) ? false:true;
        //check for the existing salary
        $price = $request->salary != null ?true:false;
        $location = $request->lat != null? true:false;
        $check_categories = $request->categories != null ? true:false;
        $check_keywords = $request->keywords != null ? true:false;
        $salary = explode(',',$request->salary);

        //mutual exclusions
        $exlusive = [];
        if($internship)
            array_push($exlusive,'intership');
        if($full_time)
            array_push($exlusive,'full_time');
        if($part_time)
            array_push($exlusive,'part_time');
        if($temporary)
            array_push($exlusive,'temporary');
        ///check of the length of exclusive in more than one
        $len_check = count($exlusive) > 0 ? true :false;
        $now = Carbon::now();
        $query = '';
        if($check_keywords){
            $query = implode(' ',$request->keywords);
        }
        $seach_data = Work::search($query,null,true)->where('status','<>','expired')->orderby('created_at','DESC')
            /*->when($check_keywords,function ($query) use($request){
                //$this->item = $query;
                foreach ($request->keywords as $keyword){
                    $query->orwhere('title', 'like', '%' . $keyword . '%');
                }
                return $query;
            })*/
            ->when($location,function ($query) use ($request){
                return $query->whereBetween('lat',[(float)$request->lat - 0.2,(float)$request->lat + 0.2])
                    ->whereBetween('lng',[(float)$request->lng - 0.2,(float)$request->lng + 0.2]);
            })
            ->when($check_categories,function ($query) use ($request){
                return $query->whereIn('job_category_id',$request->categories);
            })
            ->when($price,function($query) use ($salary){
                return $query->where('min_salary','>=',(float)$salary[0])->where('max_salary','<=',(float)$salary[1]);
            })
            ->when($len_check,function ($query) use ($exlusive){
                return $query->whereIn('type',$exlusive);
            })
            ->whereDate('expiry','>=',$now)
            ->paginate(5);

        //work with the markers
        $markers = [];
        foreach ($seach_data->items() as $item){
            array_push($markers,
                [
                    'position'=>['lat'=>$item->lat,'lng'=>$item->lng],
                    'title'=>$item->title,
                    'type'=>$item->type
                ]);
        }
            return response()->json([
                'jobs'=>$seach_data,
                'markers'=>$markers
            ]);
    }
}
