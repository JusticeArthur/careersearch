<?php

namespace App\Http\Controllers\Job;

use App\Jobs\SmsJob;
use App\Mail\JobAddedEmail;
use App\Models\Company;
use App\Models\Country;
use App\Models\JobCategories;
use App\Models\LoginLog;
use App\Models\SiteSetting;
use App\Models\State;
use App\Models\Work;
use App\Models\WorkApplication;
use App\Models\WorkBookmark;
use App\Models\WorkRequirement;
use App\Models\WorkTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ManageJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = JobCategories::all();
        $log = LoginLog::where('user_id',auth()->user()->id)->first();
        return view('dashboard.work.post',[
            'categories'=>$categories,
            'log'=>$log
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    public function bookmarkWork($id){
        $check = WorkBookmark::where('user_id',auth()->user()->id)->where('work_id',$id)->first();
        if($check){
            $check->bookmarked = $check->bookmarked ? false : true;
            $check->save();
        }else{
            $productBookmark = new WorkBookmark();
            $productBookmark->user_id = auth()->user()->id;
            $productBookmark->work_id = $id;
            $productBookmark->bookmarked = true;
            $productBookmark->save();
        }
    }
    public function removeBookmark($id){
        $mark = WorkBookmark::find($id);
        if($mark){
            $mark->bookmarked = $mark->bookmarked ? false : true;
            $mark->save();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'category'=>'required',
            'location'=>'required',
            'max_salary'=>[
                'required',
                function($attribute, $value, $fail) use ($request){
                    if($request['min_salary'] && $value <= $request['min_salary'])
                        return $fail('The max salary should be greater than the min salary');

                }
            ],
            'min_salary'=>['required',
                function($attribute, $value, $fail) use ($request){
                    if($request['max_salary'] && $value >= $request['max_salary'])
                        return $fail('The min salary should be less than the max salary');
                }
            ],
            'title'=>'required',
            'type'=>'required',
            'degree'=>'required',
            'expiry_date'=>
                ['required','date',
                    function($attribute, $value, $fail){
                        if($this->validateDeadline($value)){
                            return $fail('The expiry date is invalid.');
                        }
                    }
                ]
        ];
        $this->validate($request,$rules);
        try{
            $work = new Work();
            $work->title = title_case($request->title);
            $work->job_category_id = $request->category;
            $work->slug = str_slug($request['title']).'-'.uniqid();
            $work->min_salary = $request->min_salary;
            $work->max_salary = $request->max_salary;
            $work->user_id = auth()->user()->id;
            if(!isset(auth()->user()->profile->company)){
                $company_name = auth()->user()->firstname .' Company';
                $company = Company::create([
                    'name'=>$company_name,
                    'profile_id'=>auth()->user()->profile->id,
                    'slug'=>str_slug($company_name).'-'.uniqid()
                ]);
                $work->company_id = $company->id;
            }else{
                $work->company_id = auth()->user()->profile->company->id;
            }
            $work->location = $request->location;
            $work->degree = $request->degree;
            $work->type = $request->type;
            $work->expiry = Carbon::parse($request->expiry_date)->toDateString();
            $work->description = $request->description;
            $work->lat =isset($request->center['lat'])?$request->center['lat']:0;
            $work->lng = isset($request->center['lng'])?$request->center['lng']:0;
            if(count($request->address) > 0){
                $length = count($request->address) -1;
                $country = $request->address[$length]['long_name'];
                $country = Country::where('name',$country)->first();
                $work->country_id = $country->id;
                $state_check = explode(' ',$request->address[--$length]['long_name']);
                $state = State::where('country_id',$country->id)->where('name','like','%'.$state_check[0].'%')->first();
                $work->state_id = $state->id;
            }
            $work->save();
        }catch (\Exception $e){
            return response()->json([
                'status'=>'error'
            ],500);
        }

        //addtags if there is
        if($request['requirements']){
            foreach ($request['requirements'] as $item){
                $requirement = new WorkRequirement();
                $requirement->work_id = $work->id;
                $requirement->name = $item;
                $requirement->save();
            }
        }
        if($request['tags']){
            foreach ($request['tags'] as $item){
                $tag = new WorkTag();
                $tag->work_id = $work->id;
                $tag->name = $item;
                $tag->save();
            }
        }
        $cats = JobCategories::find($request->category);
       /*dd($cats->users);*/
        //notify users
        //return $work;
        $company_send_email = auth()->user()->profile->company->name;
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        if(count($cats->users) > 0){
            foreach ($cats->users as $user){
                //send alerts via sms and email
                Mail::to($user->email)->send(new JobAddedEmail($user,$work,$company_send_email));
                if($user->profile->mobile)
                    SmsJob::dispatch($this->formatContact($user->profile->mobile),$this->getJobMessage($user->firstname,$work),$user,$settings);
            }
        }
        return response()->json([
            'message'=>'Job Added Successfully'
        ],200);
    }
    protected function formatContact($contact){
        //replace 0 with +233 that is the country code
        $formatted = str_replace(' ','',$contact);
        if($contact[0] == '0'){
            $pos = strpos($formatted, '0');
            if ($pos !== false) {
                $formatted = substr_replace($formatted, '+233', $pos, 1);
            }
        }
        return $formatted;
    }
    protected function getJobMessage($name,$work){
        return 'Hello '.$name . ', the job ,'. $work->title.
            ' was just added and it matches your skill set. The deadline is '
            .Carbon::parse($work->expiry)->toFormattedDateString() .' . Kindly follow this link to apply '. route('jobs_apply.local',['slug'=>$work->slug]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'work.job_category_id'=>'required',
            'location'=>'required',
            'work.max_salary'=>[
                'required',
                function($attribute, $value, $fail) use ($request){
                    if($request['min_salary'] && $value <= $request['min_salary'])
                        return $fail('The max salary should be greater than the min salary');

                }
            ],
            'work.min_salary'=>['required',
                function($attribute, $value, $fail) use ($request){
                    if($request['max_salary'] && $value >= $request['max_salary'])
                        return $fail('The min salary should be less than the max salary');
                }
            ],
            'work.title'=>'required',
            'work.type'=>'required',
            'work.degree'=>'required',
            'work.expiry'=>
                ['required','date',
                    function($attribute, $value, $fail){
                        if($this->validateDeadline($value)){
                            return $fail('The expiry date is invalid.');
                        }
                    }
                ]
        ];
        $this->validate($request,$rules);
        $data['job_category_id'] = $request->work['job_category_id'];
        $data['title'] = $request->work['title'];
        $data['max_salary'] = $request->work['max_salary'];
        $data['min_salary'] = $request->work['min_salary'];
        $data['type'] = $request->work['type'];
        $data['location'] = $request->location;
        $data['degree'] = $request->work['degree'];
        $data['description'] = $request->work['description'];
        $data['expiry'] = Carbon::parse($request->work['expiry'])->toDateString();
        $data['lat'] =isset($request->center['lat'])?$request->center['lat']:0;
        $data['lng'] = isset($request->center['lng'])?$request->center['lng']:0;
        if(count($request->address) > 0){
            $length = count($request->address) -1;
            $country = $request->address[$length]['long_name'];
            $country = Country::where('name',$country)->first();
            $data['country_id'] = $country->id;
            $state_check = explode(' ',$request->address[--$length]['long_name']);
            $state = State::where('country_id',$country->id)->where('name','like','%'.$state_check[0].'%')->first();
            $data['state_id'] = $state->id;
        }
        Work::where('id',$request->work['id'])->update($data);
        $request->session()->flash('success', 'Updated Successfully!');
        return response([],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WorkTag::where('work_id',$id)->delete();
        WorkApplication::where('work_id',$id)->delete();
        WorkRequirement::where('work_id',$id)->delete();
        Work::find($id)->delete();
        return response([],200);
    }
    protected function validateDeadline($deadline){
        $deadline = Carbon::parse($deadline);
        $now = Carbon::now();
        if($now->lte($deadline))
            return false;
        return true;
    }

}
