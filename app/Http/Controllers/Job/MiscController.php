<?php

namespace App\Http\Controllers\Job;

use App\Models\JobCategories;
use App\Models\Profile;
use App\Models\Tag;
use App\Models\Work;
use App\Models\WorkApplication;
use App\Models\WorkRequirement;
use App\Models\WorkTag;
use App\Notifications\JobApplication;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class MiscController extends Controller
{
    function __construct()
    {
        $this->middleware('custom_auth');
    }
    public function jobCandidates(){
        return view('dashboard.work.manage_candidates');
    }
    public function manageJobs(Request $request,$paginate = null){
        if(!$paginate){
            $slug = $request->query->get('slug');
            if(isset($slug)){
                return view('dashboard.work.edit-work',[
                    'job'=>Work::where('slug',$slug)->first(),
                    'categories'=>JobCategories::all()
                ]);
            }
            $jobs  = Work::where('user_id',\auth()->user()->id)->latest()->paginate(8)->toArray();
            return view('dashboard.work.manage_job',[
                'jobs'=>json_encode($jobs)
            ]);
        }else{
            return Work::where('user_id',\auth()->user()->id)->latest()->paginate(8);
        }

    }
    public function getApply($slug){
        $data = Work::where('slug',$slug)->first();
        $similar = Work::where('id','<>',$data->id)
            ->where('status','active')
            ->where('user_id',$data->user_id)
            ->take(2)
            ->get();
        //dd($similar->toArray());
        /*echo $data->toJson();
        dd();*/
        return view('dashboard.work.apply',[
            'data'=>$data,
            'similar'=>$similar
        ]);
    }
    public function applyJob(Request $request){
        $apply = new WorkApplication();
        $user = Auth::user();
        $apply->user_id = $user->id;
        $apply->work_id = $request['id'];
        $apply->save();
        $job = Work::find($request->id);
        Notification::send($job->user,new JobApplication($user,$job));
    }
    public function getJobCandidates($slug){
        if(count(auth()->user()->unreadNotifications) > 0){
            foreach (auth()->user()->unreadNotifications as $notification) {
                if($notification->type == 'App\Notifications\JobApplication'){
                    $urls = explode('/',$notification->data['job_url']);
                    if($slug == end($urls)){
                        $notification->markAsRead();
                    }
                }
            }
        }
        $work = Work::where('slug',$slug)->first();
        return view('dashboard.work.manage_candidates',[
            'jobs'=>$work
        ]);
    }
    public function downloadCv($id){
        $user = User::where('id',$id)->first();
        //dd($user);
        return response()->download('storage/cvs/'.$user->profile->cv);
    }
    public function showApplication($job_slug,$applicant_slug){
        $profile = Profile::where('url',$applicant_slug)->first();
        //get job history
        $employments = DB::table('work_applications')
            ->where('work_applications.status','employed')
            ->where('work_applications.user_id',$profile->user->id)
            ->leftJoin('users','users.id','work_applications.user_id')
            ->leftJoin('works','works.id','work_applications.work_id')
            ->leftJoin('reviews',function ($join){
                $join->on('reviews.work_id','work_applications.work_id')
                    ->on('reviews.reviewable_id','work_applications.user_id');
            })
            ->select('work_applications.work_id','work_applications.user_id','users.firstname',
                'users.lastname','works.title','reviews.reviewable_id','reviews.comment','reviews.rating','work_applications.updated_at')
            ->get();
        // dd($employments->toArray());
        $jobs_done = count($employments);
        // dd($employments->toArray());
        //format employment data before sending to the front
        $formattedEmployments = [];
        foreach ($employments as $key=>$employment){
            $formattedEmployments[$key]['title'] = $employment->title;
            $formattedEmployments[$key]['comment'] = isset($employment->comment)?$employment->comment : 'No Review Yet';
            $formattedEmployments[$key]['name'] = $employment->firstname . ' '.$employment->lastname;
            $formattedEmployments[$key]['rating'] = isset($employment->rating)?number_format(6 - (float) $employment->rating,1): 0.5;
            $formattedEmployments[$key]['date'] = Carbon::parse($employment->updated_at)->toFormattedDateString();
        }
        $work = Work::where('slug',$job_slug)->first();
        $applied = WorkApplication::where('work_id',$work->id)
            ->where('user_id',$profile->user->id)
            ->first();
        //dd($profile->toArray());
        return view('dashboard.work.application_details',[
            'profile'=>$profile,
            'job_slug'=>$job_slug,
            'applied'=>$applied,
            'applicant_slug'=>$applicant_slug,
            'work'=>$work,
            'employment_history'=>json_encode($formattedEmployments)
        ]);
    }
    public function acceptApply($job_slug,$applicant_slug){
        $job = Work::where('slug',$job_slug)->first();
        $profile = Profile::where('url',$applicant_slug)->first();

        WorkApplication::where('work_id',$job->id)
            ->where('user_id',$profile->user->id)
            ->update([
            'status'=>'shortlisted',
            ]);
        return redirect()->route('jobs.manage_candidates',[
            'job_slug'=>$job_slug,
        ])->with([
            'success'=>'Accepted Successfully'
        ]);
    }

    //add work requirement
    public function addRequirement(Request $request){
        $requirement = new WorkRequirement();
        $requirement->name = $request->name;
        $requirement->work_id = $request->work_id;
        try{
            $requirement->save();
        }catch (QueryException $q){
            return response()->json([
                'message'=>'error'
            ],422);
        }
        return $requirement;
    }
    public function removeRequirement($id){
        WorkRequirement::find($id)->delete();
    }
    public function addTag(Request $request){
        $tag = new WorkTag();
        $tag->name = $request->name;
        $tag->work_id = $request->work_id;
        try{
            $tag->save();
        }catch (QueryException $q){
            return response()->json([
                'message'=>'error'
            ],422);
        }
        return $tag;
    }
    public function removeTag($id){
        WorkTag::find($id)->delete();
    }
    //search for jobs
    public function search(Request $request){
        return Work::where('user_id',\auth()->user()->id)
            ->where('title','like','%'.$request->search.'%')
            ->latest()
            ->paginate(8);
    }
}
