<?php

namespace App\Http\Controllers\Interview;

use App\Models\Contract;
use App\Models\ContractAttachment;
use App\Models\Interview;
use App\Models\InterviewPostpone;
use App\Models\Profile;
use App\Models\Work;
use App\Models\WorkApplication;
use App\Notifications\AcceptInterview;
use App\Notifications\InterviewNotification;
use App\Notifications\PostponeInterview\PostponeStatus;
use App\Notifications\PostponeInterview\RequestPostpone;
use App\Notifications\SendContract;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class InterviewController extends Controller
{
    public function index($job_slug,$applicant_slug){
        return view('dashboard.work.interview',[
            'job_slug'=>$job_slug,
            'applicant_slug'=>$applicant_slug
        ]);
    }
    public function store(Request $request){
        $rules = [
            'time'=>['required',function($attribute, $value, $fail){
                if($this->validateDeadline($value)){
                    return $fail('The interview date and time is invalid.');
                }
            }],
            'deadline'=>['required',function($attribute, $value, $fail){
                if($this->validateDeadline($value)){
                    return $fail('The deadline date is invalid.');
                }
            }],
            'location'=>'required',
            'description'=>'nullable|string',
            'contact'=>['required',function($attribute, $value, $fail){
                if(!$this->validteTel($value))
                    return $fail('Invalid telephone number');
            }]
        ];
        $this->validate($request,$rules);

        $job = Work::where('slug',$request->job)->first();
        $profile = Profile::where('url',$request->applicant)->first();
        if($request->hasFile('attachment')){
            $ima = $request->file('attachment')
                ->storePubliclyAs('public/interview',auth()->user()->profile->url .time().'.'. $request->attachment->getClientOriginalExtension() );
            $data = explode('/',$ima);
            $request['file'] = end($data);
        }
        try{
            $interview = new Interview();
            $interview->time = Carbon::parse($request->time)->toDateTimeString();
            $interview->description = $request->description;
            $interview['attachment'] = $request['file'];
            $interview->location = $request->location;
            $interview->work_id = $job->id;
            $interview->user_id = $profile->user->id;
            $interview->deadline = Carbon::parse($request->deadline)->toDateString();
            $interview->contact = $request->contact;
            $interview->save();
        }catch(\Exception $e){
            return response()->json([
                'error'=>'This action has already been taken'
            ],430);
        }
        //interviewed
        WorkApplication::where('work_id',$job->id)
            ->where('user_id',$profile->user->id)
            ->update([
                'status'=>'interviewed',
            ]);
        Notification::send($profile->user,new InterviewNotification($interview));
        Session::flash('success','Interview Invitation Sent to Applicant');
    }
    protected function validateDeadline($deadline){
        $deadline = Carbon::parse($deadline);
        $now = Carbon::now();
        if($now->lte($deadline))
            return false;
        return true;
    }
    protected function validteTel($number){

            if(preg_match('/((^[\+][0-9][0-9][0-9])|([0]{1}))[0-9]{9}+$/', $number))
            {
                return true;
            }
            return false;
    }
    public function downloadAttachment($id){
        $interview = Interview::find($id);
        return response()->download('storage/interview/'.$interview->attachment);
    }
    public function acceptInterviewInvite($id){
        $interview = Interview::where('id',$id)->first();
        if(isset($interview) && $interview->user_id == auth()->id()){
            Interview::where('id',$id)->update([
               'status'=>'accepted'
            ]);
            WorkApplication::where('work_id',$interview->work->id)
                ->where('user_id',$interview->user->id)
                ->update([
                    'accepted'=>true
                ]);
            Notification::send($interview->work->user,new AcceptInterview($interview->user,$interview->work));
            return redirect()->back()->with([
                'success'=>'Interview Invitation Accepted'
            ]);
        }else{
            return redirect()->back()->with([
                'error'=>'Action Unauthorised'
            ]);
        }

    }
    public function declineInterviewInvitation($id){
        $interview = Interview::where('id',$id)->first();
        if(isset($interview) && $interview->user_id == auth()->id()){
            Interview::where('id',$id)->update([
                'status'=>'declined'
            ]);
            return redirect()->back()->with([
                'success'=>'Interview Invitation Declined'
            ]);
        }else{
            return redirect()->back()->with([
                'error'=>'Action Unauthorised'
            ]);
        }
    }
    public function sendContract(Request $request){
        $rules = [
            'contracts'=>'required'
        ];
        $this->validate($request,$rules);
        try{
            $contract = new Contract();
            $contract->employee_id = $request->employee_id;
            $contract->employer_id = $request->employer_id;
            $contract->work_id = $request->work_id;
            $contract->status = 'pending';
            $contract->save();
        }catch (QueryException $q){
            return response()->json([
                'status'=>'duplicate'
            ],501);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'error'
            ],500);
        }
        //store image
        $files = [];
        if(count($request->contracts) > 0){
            foreach($request->contracts as $avatar){
                $slug = str_replace('-','',Str::uuid());
                $extension = $avatar->extension();
                $extensions = ['docx','pdf','doc','ppt','txt'];
                if(!in_array(strtolower($extension),$extensions))
                    continue;
                $name = $avatar->storePubliclyAs('public/contracts',$slug.'.'.$extension);
                $name = explode('/',$name);

                array_push($files,glob(public_path('storage\\contracts\\'.end($name))));
            }
        }
        $slug = str_replace('-','',Str::uuid());
        //return public_path();
        \Zipper::make(public_path('storage\\contracts\\zips\\'.$slug.'.zip'))->add($files)->close();
        $attachment = new ContractAttachment();
        $attachment->name = $slug.'.zip';
        $attachment->contract_id = $contract->id;
        $attachment->save();
        //send notification
        Notification::send(User::find($request->employee_id),new SendContract(Work::find($request->work_id)));
    }
    public function postPoneInterview(Request $request){
        if(Carbon::now()->lt(Carbon::parse($request->period))){
            try{
                $postpone = new InterviewPostpone();
                $postpone->interview_id = $request->interview_id;
                $postpone->employer_id = $request->employer_id;
                $postpone->employee_id = $request->employee_id;
                $postpone->period = Carbon::parse($request->period)->toDateTimeString();
                $postpone->work_id = $request->work_id;
                $postpone->save();
            }catch (QueryException $q){
                return response()->json([
                    'status'=>'duplicate'
                ],501);
            }
            //send notification
            Notification::send(User::find($request->employer_id),
                new RequestPostpone(User::find($request->employee_id),$postpone,Work::find($request->work_id)));
        }else{
            return response()->json([
                'status'=>'Invalid Interview Date'
            ],506);
        }

    }
    public function acceptPostpone($id){
        $postpone = InterviewPostpone::where('id',$id)->first();
        $postpone->status = 'accepted';
        $postpone->save();
        Notification::send(User::find($postpone->employee_id),new PostponeStatus('accepted',Work::find($postpone->work_id)));
    }
    public function declinePostpone($id){
        $postpone = InterviewPostpone::where('id',$id)->first();
        $postpone->status = 'declined';
        $postpone->save();
        Notification::send(User::find($postpone->employee_id),new PostponeStatus('declined',Work::find($postpone->work_id)));
    }
    //accept / decline contract
    public function acceptContract($id){
        Contract::where('id',$id)->update([
            'status'=>'accepted'
        ]);
    }
    public function declineContract($id){
        Contract::where('id',$id)->update([
            'status'=>'declined'
        ]);
    }
}
