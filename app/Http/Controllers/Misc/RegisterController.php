<?php

namespace App\Http\Controllers\Misc;

use App\Models\Company;
use App\Models\Country;
use App\Models\LoginLog;
use App\Models\Plan;
use App\Models\Product;
use App\Models\Profile;
use App\Models\Project\Project;
use App\Models\VerifyUser;
use App\Models\Work;
use App\Notifications\UpdateProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Laravel\Socialite\Facades\Socialite;
class RegisterController extends Controller
{

    public function home(){
        $latestProducts = Product::whereIn('status',['expiring','active'])->latest()->take(4)->get();
        $country = Country::where('name','Ghana')->first()->states;
        $sorted = $country->sortByDesc('jobs_count')->take(4);
        $sorted_data = $sorted->values()->all();
        $featured_jobs = Work::where('status','<>','expired')->latest()->take(5)->get();
        $countOfJobs = Work::count();
        $countOfEmployees = User::withRole('employee')->count();
        $countOfEmployers = User::withRole('employer')->count();
        $plans = Plan::with(['features'])->get();
        $employees = User::with(['profile'=>function($query){
            $query->where('overall_rating','>',0);
        }])->withCount('profile')->withRole('employee')->get();
        $filteredEmployees = [];
        foreach ($employees as $employee){
            if(isset($employee->profile)  && $employee->profile->overall_rating > 3.5){
                array_push($filteredEmployees,$employee);
            }
        }
        return view('welcome',[
            'featured_jobs'=>$featured_jobs,
            'featured_states'=>$sorted_data,
            'plans'=>$plans,
            'products'=>$latestProducts,
            'jobs'=>$countOfJobs,
            'employees'=>$countOfEmployees,
            'employers'=>$countOfEmployers,
            'rated_employees'=>$filteredEmployees
        ]);
    }
    public function cmp($a, $b) {
        if (count($a->jobs) == count($b->jobs)) {
            return 0;
        }
        return (count($a->jobs) > count($b->jobs)) ? -1 : 1;
    }
    public function register(Request $request)
    {
        $rules =  [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
        $this->validate($request,$rules);

        $role = Role::where('name',strtolower($request['status']))->first();
        $user = User::create([
            'firstname' => $request['firstname'],
            'lastname'=>$request['lastname'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'status'=>$request['status'],
            'slug'=>$request['firstname'].'-'.$request['lastname'],
            'role_id'=>$role->id,
        ]);
        $user->attachRole($role);
        //Notification::send($user,new UpdateProfile());
        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->url = $user->slug .'-'.uniqid();
        $profile->save();
        if($request['status'] == 'employer'){
            Company::create([
                'name'=>$request['company'],
                'profile_id'=>$profile->id,
                'slug'=>str_slug($request['company']).'-'.uniqid()
            ]);
        }
        $verify = VerifyUser::create([
            'user_id'=>$user->id,
            'token'=>str_random(40)
        ]);
        try{
            Notification::send($user,new \App\Notifications\VerifyUser($user));
        }catch (\Exception $e){
            $user->profile->delete();
            $user->delete();
            return response()->json([
                'status'=>'error',
                'message'=>'email couldn\'t be sent please contact the administrator'
            ],501);
        }

        return response()->json([
            'message'=>'Registered Successfully'
        ],200);
    }

    public function login(Request $request){
        $rules =  [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
        ];
        $this->validate($request,$rules);
        $url = null;
        Auth::logout();
        if(Auth::attempt(['email'=>$request['email'],'password'=>$request['password']])){
            $url = '/dashboard';
            if(\auth()->user()->verified == 0) {
                \auth()->logout();
                return response()->json([
                    'status' => 'unverified',
                    'message' => 'Email Not Verified'
                ],301);
            }
            $temp_url = isset($request->returnUrl) ? $request->returnUrl : $url;
            $check = LoginLog::where('user_id',\auth()->user()->id)->first();
            if(isset($check)){
                if($request->lat != null || $request->lng != null){
                    $check->lat = $request->lat;
                    $check->lng = $request->lng;
                    $check->save();
                }
            }else{
                if(!$request->lat || !$request->lng){
                    $log = new LoginLog();
                    $log->lat = 6.674453799999999;
                    $log->lng = -1.5424930000000359;
                    $log->user_id = \auth()->user()->id;
                    $log->save();
                }else{
                    $log = new LoginLog();
                    $log->lat = $request->lat;
                    $log->lng = $request->lng;
                    $log->user_id = \auth()->user()->id;
                    $log->save();
                }

            }
            return response()->json([
                'url'=>$temp_url
            ],200);
        }
        if($url == null){
            return response()->json([
                'error'=>'Your Credentials do not match our records',
            ],422);
        }
    }
    public function socialLogin($social){
        return Socialite::driver($social)->redirect();
    }
    public function handleProviderCallback($social){
        $socialUser = Socialite::driver($social)->user();
        $user = User::where('email',$socialUser->getEmail())->first();
        if($user){
            Auth::login($user);
            return redirect()->route('home');
        }else{
            return redirect()->back();
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
    public function verifyUser($token){
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your email is verified. You can now login.";
            }else{
                $status = "Your email is already verified. You can now login.";
            }
        }else{
            return redirect('/')->with('error', "Sorry your email cannot be identified.");
        }

        return redirect('/')->with('success', $status);
    }
}
