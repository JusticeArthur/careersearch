<?php

namespace App\Http\Controllers\Misc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageNotification extends Controller
{
    public function makeAllNotifications(){
        if(count(auth()->user()->unreadNotifications) > 0){
            foreach (auth()->user()->unreadNotifications as $notification) {
                        $notification->markAsRead();
            }
        }
        return redirect()->back();
    }

}
