<?php

namespace App\Http\Controllers\Misc;

use App\Models\Work;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EndPointController extends Controller
{
    public function exposeJobs(){
        $now = Carbon::now();
        return Work::orderby('created_at','DESC')->whereDate('expiry','>=',$now)->paginate(10);
    }
}
