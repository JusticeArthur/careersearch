<?php

namespace App\Http\Controllers\Misc;

use App\Events\ChatConnect;
use App\Events\MessageEvent;
use App\Model\UserChat;
use App\Models\Chat;
use App\Notifications\ChatNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class ChatController extends Controller
{
    public function markMessageAsRead(Request $request,$id){
        Chat::where('id',$id)->update([
            'read_at'=>Carbon::now()->toDateTimeString()
        ]);
    }
    public function getUserChatsById($id){
        $authUser = auth()->user()->id;
        //first update all unread chats
         Chat::whereNull('read_at')
             ->where('receiver_id',$authUser)
             ->where('sender_id',$id)
             ->update([
                 'read_at'=>Carbon::now()->toDateTimeString()
             ]);
        $data = Chat::whereIn('sender_id',[$authUser,$id])
            ->whereIn('receiver_id',[$authUser,$id])
               ->orderby('created_at','asc')
            ->get()
            ->groupBy(function($query) {
                //return $query->created_at->format('d-M-y');
                return $query->created_at->format('M d, Y');
            });
        $today = Carbon::now()->format('M d, Y');
        if(!array_key_exists($today,$data->toArray())){
            $data[$today] = [];
        }
        return response()->json([
            'chats'=>$data
        ],200);
    }
    public function send(Request $request){

        $chat = new Chat();
        $chat->sender_id = auth()->user()->id;
        $chat->receiver_id = $request->receiver;
        $chat->chat = $request->message;
        $chat->save();


        UserChat::whereIn('user_id',[$request->receiver,auth()->user()->id])
            ->whereIn('chat_id',[$request->receiver,auth()->user()->id])
            ->update([
                'updated_at'=>Carbon::now()->toDateTimeString(),
                'last_message'=>$request->message
            ]);
        event(new ChatConnect($chat,auth()->user(),$request->receiver));
        //fire the notification event here
        //the receiver should receive this notification
        event(new MessageEvent($chat,$request->receiver));
        //update chats of both users
        return response()->json(['message'=>'sent'],200);
    }
    public function createChat(Request $request){
        try{
            $chat = new UserChat();
            $chat->user_id = auth()->id();
            $chat->chat_id = $request->sender_id;
            $chat->save();
        }catch (QueryException $e){

        }catch (\Exception $e){

        }
        return User::with(['chats'=>function($query) use ($request){
            return $query->where('user_id',auth()->id())
                ->where('chat_id',$request->sender_id)->first();
        }])->where('id',auth()->user()->id)->get()->pluck('chats');
    }
    public function addChat($id){
        try{
            $chat = new UserChat();
            $chat->user_id = auth()->id();
            $chat->chat_id = $id;
            $chat->save();
        }catch (\Exception $e){}
        try{
            //save the reverse of it
            $new_chat = new UserChat();
            $new_chat->user_id = $id;
            $new_chat->chat_id = auth()->id();
            $new_chat->save();
        }catch (\Exception $e){}
        Session::put(['message_id'=>$id]);
        return redirect()->route('dashboard.messages');
    }
}
