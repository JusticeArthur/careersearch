<?php

namespace App\Http\Controllers\Search;

use App\Models\Product;
use App\Models\Work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function homeSearch(Request $request){
        $works = Work::search($request->title,null,true)->take(5)->get();
        $products =  Product::search($request->title,null,true)->take(5)->get();
        $merged_data = array_merge($works->toArray(),$products->toArray());
        $final_search = [];
        foreach($merged_data as $key=>$value){
            array_push($final_search,['id'=>$value['id'],'title'=>$value['title']]);
        }
        return collect($final_search)->unique('title')->values()->all();
    }
    public function getResults(){
        $query = str_replace('-',' ',Input::get('q'));
        $lat = Input::get('lat');
        $lng = Input::get('lng');
        //conduct search here
        $works = Work::search($query)
            ->when(($lat != null),function ($query) use ($lat,$lng){
                return $query->whereBetween('lat',[$lat - 0.2,$lat + 0.2])
                    ->whereBetween('lng',[$lng - 0.2,$lng + 0.2]);
            })
            ->take(5)
            ->get();
        $products =  Product::search($query)
            ->when(($lat != null),function ($query) use ($lat,$lng){
                return $query->whereBetween('lat',[$lat - 0.2,$lat + 0.2])
                    ->whereBetween('lng',[$lng - 0.2,$lng + 0.2]);
            })
            ->take(5)
            ->get();
        /*dd([
            'works'=>$works,
            'products'=>$products
        ]);*/
        return view('search.index',[
            'works'=>json_encode($works),
            'products'=>json_encode($products),
            'keywords'=>json_encode(explode(' ',$query)),
        ]);
    }
    public function filterResults(Request $request){
        $query = implode(' ',$request->keywords);
        $lat = $request->lat;
        $lng = $request->lng;
        $works = Work::search($query)
            ->when(($lat != null),function ($query) use ($lat,$lng){
                return $query->whereBetween('lat',[$lat - 0.2,$lat + 0.2])
                    ->whereBetween('lng',[$lng - 0.2,$lng + 0.2]);
            })
            ->take(5)
            ->get();
        $products =  Product::search($query)
            ->when(($lat != null),function ($query) use ($lat,$lng){
                return $query->whereBetween('lat',[$lat - 0.2,$lat + 0.2])
                    ->whereBetween('lng',[$lng - 0.2,$lng + 0.2]);
            })
            ->take(5)
            ->get();
        return response()->json([
            'jobs'=>$works,
            'products'=>$products,

        ]);
    }
}

