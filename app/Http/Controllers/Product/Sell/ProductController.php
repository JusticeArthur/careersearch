<?php

namespace App\Http\Controllers\Product\Sell;

use App\Models\Brand;
use App\Models\Country;
use App\Models\ItemCategory;
use App\Models\ItemSubCategory;
use App\Models\Product;
use App\Models\ProductAvatar;
use App\Models\State;
use App\Models\UserContact;
use App\Notifications\ProductAdded;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user()->profile;
        $mobile = $user->mobile;
        $itemCategories = ItemCategory::all()->toJSON();
        return view('products.sell.sell',[
            'itemCategories'=>$itemCategories,
            'mobile' => $mobile,
            'user'=>$user
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product' => 'required',
        ]);

        $validator->after(function($validator) use ($request){
            $product = json_decode($request->product,true);
            $brands = json_decode($request->brands,true);
            $models = json_decode($request->models,true);
            $item_sub_categories = json_decode($request->item_sub_categories,true);
                if(!isset($product['item_category_id'])){
                    $validator->errors()->add('Item Category', 'The Item Category Field is required');
                }else{
                    if(!filter_var($product['price'],FILTER_VALIDATE_FLOAT)){
                        $validator->errors()->add('Price', 'The Price is invalid');
                    }
                    if(!isset($product['item_sub_category_id']) && count($item_sub_categories) > 0){
                        $validator->errors()->add('Item Sub Category', 'The Item Sub Category Field is required');
                    }else{
                        if(!isset($product['brand_id']) && count($brands) > 0){
                            $validator->errors()->add('Brand', 'The Brand Field is required');
                        }else{
                            if(!isset($product['model_id']) && count($models) > 0){
                                $validator->errors()->add('Model', 'The Model Field is required');
                            }
                        }
                    }
                }
        });
        if($validator->fails()){
            return response()->json([
                'errors'=>$validator->errors()
            ],422);
        }
        //save product

        $center = json_decode($request->center,true);
        $product = json_decode($request->product,true);
        $product['user_id'] = auth()->user()->id;
        $product['lat'] = isset($center['lat'])?$center['lat']:0;
        $product['lng'] = isset($center['lng'])?$center['lng']:0;
        $product['slug'] = str_replace('-','',Str::uuid());
        $product['expiry'] = Carbon::now()->addDays(15)->toDateString();

        //add the country id
        $address = json_decode($request->address,true);
        if(count($address) > 0){
            $length = count($address) -1;
            $country = $address[$length]['long_name'];
            $country = Country::where('name',$country)->first();
            $product['country_id'] = $country->id;
            $state_check = explode(' ',$address[--$length]['long_name']);
            $state = State::where('country_id',$country->id)->where('name','like','%'.$state_check[0].'%')->first();
            $product['state_id'] = $state->id;
        }
        $product = Product::create($product);

        //create contacts
        $contacts = json_decode($request->contacts,true);
        if(count($contacts) > 0){
            foreach ($contacts as $mobile){
                $check = UserContact::where('contact',$mobile)
                    ->where('user_id',auth()->user()->id)
                    ->first();
                if(isset($check))
                    continue;
                $phone = new UserContact();
                $phone->contact = $mobile;
                $phone->verified = true;
                $phone->user_id = auth()->user()->id;
                $phone->save();
            }
        }
        //save center
        //save avatars
         if(count($request->avatars) > 0){
             foreach($request->avatars as $avatar){
                 $slug = str_replace('-','',Str::uuid());
                 $extension = $avatar->extension();
                 if(!isset($extension))
                     continue;
                // $name = $avatar->storePubliclyAs('public/product_avatars',$slug.'.'.$extension);
                 Image::make($avatar)->fit(400,400)->save('storage/product_avatars/'.$slug.'.'.$extension);
                 //$name = explode('/',$name);
                 $product_avatar = new ProductAvatar();
                 $product_avatar->name = $slug.'.'.$extension;
                 $product_avatar->product_id = $product->id;
                 $product_avatar->save();
             }
         }
         //notify admins of the systems
        $admins = User::withRole('admin')->get();
        Notification::send($admins,new ProductAdded($product));
        return $product;

    }
    public function getItemSubCategories($id){
        $category = ItemCategory::find($id);
        return $category->itemSubCategories;
    }
    public function getItemSubBrands($id){
        $subs = ItemSubCategory::find($id);
        return $subs->brands;
    }
    public function getBrandModels($id){
        $brand = Brand::find($id);
        return $brand->models;
    }
    //get my posts
    public function myposts($paginate = null){
        $slug = Input::get('slug');
        if($slug){
            $product = Product::where('slug',$slug)->first();
            //dd($product->toArray());
            $itemCategories = ItemCategory::all()->toJSON();
            return view('products.sell.edit-post',[
                'itemCategories'=>$itemCategories,
                'product'=>$product
            ]);
        }
        $products = Product::where('user_id',auth()->user()->id)
            ->orderBy('created_at','desc')
            ->paginate(4)
            ->toArray();
        /*echo $products->toJson();
        dd();*/
        if($paginate){
            return $products;
        }
        return view('products.sell.posts',[
            'products'=>json_encode($products)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'product' => 'required',
        ]);
        $product = json_decode($request->product,true);
        $validator->after(function($validator) use ($request,$product){
            $brands = json_decode($request->brands,true);
            $models = json_decode($request->models,true);
            $item_sub_categories = json_decode($request->item_sub_categories,true);
            if(!isset($product['item_category_id'])){
                $validator->errors()->add('Item Category', 'The Item Category Field is required');
            }else{
                if(!filter_var($product['price'],FILTER_VALIDATE_FLOAT)){
                    $validator->errors()->add('Price', 'The Price is invalid');
                }
                if(!isset($product['item_sub_category_id']) && count($item_sub_categories) > 0){
                    $validator->errors()->add('Item Sub Category', 'The Item Sub Category Field is required');
                }else{
                    if(!isset($product['brand_id']) && count($brands) > 0){
                        $validator->errors()->add('Brand', 'The Brand Field is required');
                    }else{
                        if(!isset($product['model_id']) && count($models) > 0){
                            $validator->errors()->add('Model', 'The Model Field is required');
                        }
                    }
                }
            }
        });
        if($validator->fails()){
            return response()->json([
                'errors'=>$validator->errors()
            ],422);
        }
        $center = json_decode($request->center,true);
      //  $product = json_decode($request->product,true);
        $product['lat'] = isset($center['lat'])?$center['lat']:$product['lat'];
        $product['lng'] = isset($center['lng'])?$center['lng']:$product['lng'];
        $product['expiry'] = Carbon::now()->addDays(15)->toDateString();
        $product['status'] = 'active';

        $address = json_decode($request->address,true);
        if(count($address) > 0){
            $length = count($address) -1;
            $country = $address[$length]['long_name'];
            $country = Country::where('name',$country)->first();
            $product['country_id'] = $country->id;
            $state_check = explode(' ',$address[--$length]['long_name']);
            $state = State::where('country_id',$country->id)->where('name','like','%'.$state_check[0].'%')->first();
            $product['state_id'] = $state->id;
        }
        unset($product['user']);
        unset($product['avatars']);
        unset($product['brand']);
        unset($product['expired']);
        unset($product['human_diff']);
        unset($product['bookmarked']);
        unset($product['bookmark']);
        unset($product['posted_on']);
        Product::where('id',$id)->update($product);

        return response([],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
    }
    //more on the update of a product
    public function addAvatar(Request $request){
        $slug = str_replace('-','',Str::uuid());
        $extension = $request->avatar->extension();
        Image::make($request->avatar)->fit(400,400)->save('storage/product_avatars/'.$slug.'.'.$extension);
        $product_avatar = new ProductAvatar();
        $product_avatar->name = $slug.'.'.$extension;
        $product_avatar->product_id = $request->id;
        $product_avatar->save();
        return $product_avatar;
    }
    public function removeAvatar($id){
        ProductAvatar::find($id)->delete();
        return response([],200);
    }
    public function addContact(Request $request){
        $check = UserContact::where('contact',$request->contact)
            ->where('user_id',auth()->user()->id)
            ->first();
        if(isset($check))
            return response([],501);
        $phone = new UserContact();
        $phone->contact = $request->contact;
        $phone->verified = true;
        $phone->user_id = auth()->user()->id;
        $phone->save();
        return $phone;
    }
    public function removeContact($id){
        UserContact::find($id)->delete();
        return response([],200);
    }
}
