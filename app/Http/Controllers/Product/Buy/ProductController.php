<?php

namespace App\Http\Controllers\Product\Buy;

use App\Models\Brand;
use App\Models\ItemCategory;
use App\Models\Product;
use App\Models\ProductBookmark;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $item;
    public function index(){
        $user = auth()->user();
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        $products = Product::with(['category:id,name','subCategory:id,name',
            'brand:id,name','model:id,name','country:id,name','state:id,name'])
            ->whereIn('status',['expiring','active'])
            ->where('user_id','<>',$user->id)
            ->whereBetween('lat',[$user->log->lat - 0.2,$user->log->lat + 0.2])
            ->whereBetween('lng',[$user->log->lng - 0.2,$user->log->lng + 0.2])
            ->orderBy('created_at','desc')
            ->get();
        $categories = ItemCategory::all();
        $brands = Brand::all();
        /*echo $products->toJson();
        dd();*/
/*        dd($settings->toArray());*/
        return view('products.buy.buy',[
            'products'=>$products,
            'settings'=>$settings,
            'categories'=>$categories,
            'brands'=>$brands,
            'log'=>$user->log
        ]);
    }
    public function getProductByLocation(Request $request){
        $products = Product::with(['category:id,name','subCategory:id,name',
            'brand:id,name','model:id,name','country:id,name','state:id,name'])
            ->whereIn('status',['expiring','active'])
            ->where('user_id','<>',auth()->user()->id)
            ->whereBetween('lat',[$request->lat - 0.2,$request->lat + 0.2])
            ->whereBetween('lng',[$request->lng - 0.2,$request->lng + 0.2])
            ->orderBy('created_at','desc')
            ->get();
        return $products;
    }
    public function filterData(Request $request){
        $center = count($request->center) > 0 ? true:false;
        $keywords =  $request->keywords;
        $price = explode(',',$request->price);
        $category = isset($request->item_category_id) ? true:false;
        $sub_category = isset($request->item_sub_category_id) ? true:false;
        $brand = isset($request->brand_id) ? true:false;
        $model = isset($request->model_id) ? true:false;
        $products = Product::with(['category:id,name','subCategory:id,name',
            'brand:id,name','model:id,name','country:id,name','state:id,name'])
            ->whereIn('status',['expiring','active'])
            ->where('user_id','<>',auth()->user()->id)
            ->when($center,function ($query) use ($request){
                return $query->whereBetween('lat',[$request->center['lat'] - 0.2,$request->center['lat'] + 0.2])
                    ->whereBetween('lng',[$request->center['lng'] - 0.2,$request->center['lng'] + 0.2]);
            })
            ->whereBetween('price',[(float)$price[0],(float)$price[1]])
            ->when($category,function ($query) use ($request){
                return $query->where('item_category_id',$request->item_category_id);
            })
            ->when($sub_category,function ($query) use ($request){
                return $query->where('item_sub_category_id',$request->item_sub_category_id);
            })
            ->when($brand,function ($query) use ($request){
                return $query->where('brand_id',$request->brand_id);
            })
            ->when($model,function ($query) use ($request){
                return $query->where('model_id',$request->model_id);
            })
            ->when($keywords,function ($query,$keywords){
                //$this->item = $query;
                foreach ($keywords as $title){
                    $this->item = $query->orwhere('title', 'like', '%' . $title . '%');
                    $query = $this->item;
                }
                return $query;
            })
            ->orderBy('created_at','desc')
            ->get();
            return $products;
    }
    public function productDetail($slug){
        $product = Product::where('slug',$slug)->first();
        $similar = Product::where('id','<>',$product->id)
            ->where('status','active')
            ->where('brand_id',$product->brand_id)
            ->take(2)
            ->get();
       // dd($similar->toArray());
        $settings = SiteSetting::where('custom_id',config('app.customId'))->first();
        return view('products.buy.details.detail',[
            'product'=>$product,
            'settings'=>$settings,
            'similar'=>$similar
        ]);
    }
    public function bookmarkProduct($id){
        $check = ProductBookmark::where('user_id',auth()->user()->id)->where('product_id',$id)->first();
        if($check){
            $check->bookmarked = $check->bookmarked ? false : true;
            $check->save();
        }else{
            $productBookmark = new ProductBookmark();
            $productBookmark->user_id = auth()->user()->id;
            $productBookmark->product_id = $id;
            $productBookmark->bookmarked = true;
            $productBookmark->save();
        }
    }
}
