<?php

namespace App\Http\Controllers\Blog;

use App\Models\Comment;
use App\Models\Post;
use App\Models\PostTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function index(){
        $featured = Post::where('featured',true)->orderBy('created_at','desc')->get();
        $tags = PostTag::all();
        return view('blog.index',[
            'featured'=>$featured,
            'tags'=>$tags
        ]);
    }
    public function getRecentPost(){
        return Post::orderBy('created_at','desc')->paginate(4);
    }
    public function getPostBySlug($slug){
        $post = Post::with(['relatedPosts'=>function($query) use($slug){
            $query->where('slug','<>',$slug);
        },'comments'=>function($query){
            $query->orderBy('created_at','desc');
        },'comments.user'])->where('slug',$slug)->first();
        $previousPost = Post::where('id','<',$post->id)->orderBy('created_at','desc')->first();
        $nextPost = Post::where('id','>',$post->id)->orderBy('created_at')->first();
        $tags = PostTag::all();
        return view('blog.detail',[
            'post'=>$post,
            'previous'=>$previousPost,
            'next'=>$nextPost,
            'tags'=>$tags
        ]);
    }
    public function saveComment(Request $request){
        $comment = new Comment();
        $comment->body = $request->body;
        if(auth()->check()){
            $comment->user_id = auth()->id();
        }else{
            $comment->name = $request->name;
            $comment->email = $request->email;
        }
        $comment->slug = (string) Str::uuid();
        $post = Post::find($request->get('post_id'));
        $post->comments()->save($comment);
        return Comment::find($comment->id);
    }
    public function saveReply(Request $request){
        $comment = new Comment();
        $comment->body = $request->body;
        if(auth()->check()){
            $comment->user_id = auth()->id();
        }else{
            $comment->name = $request->name;
            $comment->email = $request->email;
        }
        $comment->slug = (string) Str::uuid();
        $comment->parent_id = $request->comment_id;
        $post = Post::find($request->get('post_id'));
        $post->comments()->save($comment);
        return Comment::find($comment->id);
    }
}
