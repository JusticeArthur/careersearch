<?php

namespace App\Http\Controllers\Profile;

use App\Models\Profile;
use App\Models\ProfileView;
use App\Models\WorkApplication;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function index($slug){
        $currentUser = auth()->user();
        $profile = Profile::with(['user','skills','state','state.ucountry:id,name'])->where('url',$slug)->first();
        $employments = DB::table('work_applications')
            ->where('work_applications.status','employed')
            ->where('work_applications.user_id',$profile->user->id)
            ->leftJoin('users','users.id','work_applications.user_id')
            ->leftJoin('works','works.id','work_applications.work_id')
            ->leftJoin('reviews',function ($join){
                $join->on('reviews.work_id','work_applications.work_id')
                    ->on('reviews.reviewable_id','work_applications.user_id');
            })
                ->select('work_applications.work_id','work_applications.user_id','users.firstname',
              'users.lastname','works.title','reviews.reviewable_id','reviews.comment','reviews.rating','work_applications.updated_at')
            ->get();
       // dd($employments->toArray());
        $jobs_done = count($employments);
       // dd($employments->toArray());
        //format employment data before sending to the front
        $formattedEmployments = [];
        foreach ($employments as $key=>$employment){
            $formattedEmployments[$key]['title'] = $employment->title;
            $formattedEmployments[$key]['comment'] = isset($employment->comment)?$employment->comment : 'No Review Yet';
            $formattedEmployments[$key]['name'] = $employment->firstname . ' '.$employment->lastname;
            $formattedEmployments[$key]['rating'] = isset($employment->rating)?number_format(6 - (float) $employment->rating,1): 0.5;
            $formattedEmployments[$key]['date'] = Carbon::parse($employment->updated_at)->toFormattedDateString();
        }
        //dd($formattedEmployments);
        return view('profile.index',[
            'profile'=>$profile,
            'currentUser'=>$currentUser,
            'employments'=>json_encode($formattedEmployments),
            'jobs_done'=>$jobs_done
        ]);
    }
    //views of profiles
    public function countViews(Request $request){
        if($request->id != $request->currentId)
            ProfileView::create([
                'viewee_id'=>$request->id,
                'viewable_id'=>$request->currentId,
                'viewable_type'=>'App\Models\ProfileViews'
            ]);
       // return 'hiiiiiiiiii';
    }
    public function getProfileViewCount(Request $request){
            $views = ProfileView::where('viewable_type','App\Models\ProfileViews')
               ->where('viewable_id',auth()->user()->id);
               if($request->data === 'last-six'){
                   $views->where("created_at",">", Carbon::now()->subMonths(6));
               }else{
                   $views->where("created_at",">", Carbon::now()->subYears(1));
               }
               $data = $views->get()
               ->groupBy(function ($query){
                   return Carbon::parse($query->created_at)->format('M');
               });
           $month_counts = [];
           $months =[];
           foreach ($data->toArray() as $key=>$value){
                array_push($months,$key);
                array_push($month_counts,count($data->toArray()[$key]));
           }
           return [
               'months'=>$months,
               'counts'=>$month_counts
           ];
    }

    public function updateProfileIntroduction(Request $request){
        $profile = Profile::find($request->id);
        $profile->introduction = $request->data;
        $profile->save();
        return response([],200);
    }
}
