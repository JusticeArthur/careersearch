<?php

namespace App\Http\Controllers\Profile;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FindEmployeeController extends Controller
{
    //
     public function index()
    {
        $user = auth()->user();
        $employees = User::withRole('employee')->where('id','<>',$user->id)->get();
       // dd($employees->toArray());
        return view('Profile.find.index',[
            'employees'=>$employees
        ]);
    }
}
