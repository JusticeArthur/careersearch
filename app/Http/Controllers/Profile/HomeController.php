<?php

namespace App\Http\Controllers\Profile;

use App\Models\Profile;
use App\User;
use App\Models\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index($slug){
        $profile = Profile::where('url',$slug)->first();
        $skills = Profile::find($profile->id)->skills;
        return view('dashboard.profile',[
            'profile'=>$profile,
            'skills'=>$skills
        ]);
    }
}
