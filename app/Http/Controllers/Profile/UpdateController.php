<?php

namespace App\Http\Controllers\Profile;

use App\Models\Company;
use App\Models\Country;
use App\Models\JobCategories;
use App\Models\Profile;
use App\Models\State;
use App\Models\Tag;
use App\Models\UserCategory;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UpdateController extends Controller
{
    public function update(Request $request){
       // dd($request);
        $user = auth()->user();
        $rules = [
            'email'=>'email|unique:users,email,'.$user->id,
            'firstname'=>'required',
            'lastname'=>'required',
            'status'=>'required',
            'categories'=>[function($attribute, $value, $fail) use($request){
            if($request->status == 'employee'){
                $data = json_decode($value);
                if(sizeof($data) < 1){
                    return $fail('The categories field is required');
                }
            }
            }],
            'currentPass'=>[
                function($attribute, $value, $fail) use ($request){
                    if(isset($value))
                        if(!Hash::check($value,auth()->user()->password)){
                            return $fail('The Password is Incorrect');
                        }
                }
            ],
            'password'=>'nullable|min:6|confirmed',
        ];
        $this->validate($request,$rules);
        if($request->ajax()){
            DB::table('users')->where('id',$user->id)->update([
                'email'=>$request->email,
                'lastname'=>$request->lastname,
                'firstname'=>$request->firstname,
                'status'=>$request->status,
            ]);
            if(isset($request->password)){
                DB::table('users')->where('id',$user->id)->update([
                    'password'=>bcrypt($request->password),
                ]);
            }
            if($request->hasFile('file')){
                $image = $request->file('file')
                    ->storePubliclyAs('public/cvs',auth()->user()->profile->url .'.'. $request->file->getClientOriginalExtension() );
                $data = explode('/',$image);
                $request['cv'] = end($data);
            }
            if($request->hasFile('image')){
                //update settings to store images here
                $originalExtension = $request->image->getClientOriginalExtension();
               /* $ima = $request->file('image')
                    ->storePubliclyAs('public/avatars',auth()->user()->profile->url .'.'. $originalExtension );
                $data = explode('/',$ima);*/

                Image::make($request->file('image'))->fit(300, 300)->save('avatars/profile/'.auth()->user()->profile->url .'.'. $originalExtension);
                Image::make($request->file('image'))->fit(100, 100)->save('avatars/status/'.auth()->user()->profile->url .'.'. $originalExtension);
                $request['avatar'] = auth()->user()->profile->url .'.'. $originalExtension;
            }

            $previos_role = auth()->user()->roles->first();
            DB::table('role_user')->where('user_id',$user->id)
                ->where('role_id',$previos_role->id)
                ->delete();
            $newRole = Role::where('name',$request->status)->first();
            auth()->user()->attachRole($newRole);
            //data starts
            $updateData = $request->all();
            $center = json_decode($request->center,true);
            if(isset($request->center)){
                $updateData['lat'] = isset($center['lat'])?$center['lat']:0;
                $updateData['lng'] = isset($center['lng'])?$center['lng']:0;
            }
            $address = json_decode($request->address,true);
            $country_id = 0;
            $state_id = 0;
            if(count($address) > 0){
                $length = count($address) -1;
                $country = $address[$length]['long_name'];
                $country = Country::where('name',$country)->first();
                $updateData['country_id'] = $country_id = $country->id;
                $state_check = explode(' ',$address[--$length]['long_name']);
                $state = State::where('country_id',$country->id)->where('name','like','%'.$state_check[0].'%')->first();
                $updateData['state_id']= $state_id = $state->id;
            }
            $updateData['dob'] = Carbon::parse($request->dob)->toDateString();
            auth()->user()->profile->update($updateData);
            if(isset($request['categories'])){
                $test_data = UserCategory::where('user_id',auth()->user()->id);
                if(isset($test_data)){
                    $test_data->delete();
                }
            }
            //update company

            //check and update company logo
            $logo = null;
            if($request->hasFile('logo')){
                $originalExtension = $request->logo->getClientOriginalExtension();
                $name = str_replace('-','',Str::uuid());
               /* $ima = $request->file('logo')
                    ->storePubliclyAs('public/logo',str_replace('-','',Str::uuid()) .'.'. $request->logo->getClientOriginalExtension() );
                $data = explode('/',$ima);*/
                Image::make($request->file('logo'))->fit(400, 400)->save('avatars/logos/'.$name .'.'. $originalExtension);
                $logo = $name. '.'.$originalExtension;
            }
            //more update coming
            if($request->status == 'employer'){
                $company = json_decode($request->company,true);
                if(isset(auth()->user()->profile->company)){
                    $comp = auth()->user()->profile->company;
                    $data = explode('/',$comp->logo);
                    $temp_logo = end($data);
                    $id = auth()->user()->profile->company->id;
                    Company::where('id',$id)
                    ->update([
                        'location'=>isset($request->location)?$request->location:$comp->location,
                        'description'=>$company['description'],
                        'tag_line'=>$company['tag_line'],
                        'logo'=>$logo != null ? $logo : $temp_logo,
                        'lat'=>isset($center['lat'])?$center['lat']:0,
                        'lng'=>isset($center['lng'])?$center['lng']:0,
                        'country_id'=>$country_id == 0? $comp->country_id:$country_id,
                        'state_id'=>$state_id == 0? $comp->state_id:$state_id,
                    ]);
                }else{
                    Company::create($company);
                }
            }
            if($request->status == 'employee'){
                $job_cats  = json_decode($request['categories'],true);
                foreach ($job_cats as $category){
                    $new_category = new UserCategory();
                    $new_category->user_id = auth()->user()->id;
                    $new_category->job_category_id = $category['id'];
                    $new_category->save();
                }
            }
        }
    }

    public function removeCV(){
        auth()->user()->profile->cv = null;
        auth()->user()->profile->save();
    }
}
