<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function markSingle($id){
            DB::table('notifications')->where('id',$id)
            ->update([
            'read_at'=>Carbon::now()->toDateTimeString()
            ]);
    }
    public function markAllAsRead(Request $request){
        foreach(auth()->user()->unreadNotifications as $notification){
            $notification->read_at = Carbon::now()->toDateTimeString();
            $notification->save();
        }
        return response([],200);
    }
}
