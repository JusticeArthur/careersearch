<?php

namespace App\Http\Controllers\Company;

use App\Models\Company;
use App\Models\ReviewCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index(){
        $companies = Company::orderBy('name')->get();
        $grouped = $companies->groupBy(function ($item, $key) {
            return substr($item->name, 0, 1);
        });
        return view('companies.index',[
            'companies'=>$grouped
        ]);
    }
    public function profile($slug){
        $company = Company::with('country:id,name')->where('slug',$slug)->first();
        $countreview = ReviewCompany::where('company_id',$company->id)->count('rate');
        if ($countreview>2) {
           $rate = ReviewCompany::where('company_id',$company->id)->avg('rate');
           $rate = number_format((float)$rate, 1, '.', '');
        $company->rate = $rate;
        } else {
           $company->rate = 0;
        }
        
        
        $comp = Company::with('review')->where('slug',$slug)->first();
        /*echo $company->toJson();
        dd();*/
        //dd($company->toArray());
        return view('companies.profile',[
            'company'=>$company->toArray(),
            'comp'=>$comp
        ]);
    }
}
