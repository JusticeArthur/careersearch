<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 26/11/2018
 * Time: 13:50
 */

namespace App\Sms;


use infobip\api\client\GetSentSmsLogs;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\logs\GetSentSmsLogsExecuteContext;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;

class Sms
{
    public static function send($to,$text,$user,$settings){
        $configuration = new BasicAuthConfiguration($settings->sms_user_name, $settings->sms_password, 'http://api.infobip.com/');
        $client = new SendMultipleTextualSmsAdvanced($configuration);
        $destination = new Destination();
        $destination->setTo($to);
        // $destination->setMessageId('Hi Justice');

        $message = new Message();
        $message->setDestinations([$destination]);
        $message->setFrom($settings->sms_display_name);
        $message->setTo($to);
        $message->setText($text);
        $message->setNotifyUrl('NotifyUrl');
        $message->setNotifyContentType('application/json');
        //$message->setCallbackData('');
        $requestBody = new SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);
        $response = $client->execute($requestBody);
    }
    public static function response($messageId,$to){
        $configuration = new BasicAuthConfiguration('myeschoolgh', 'Ghana2@sch', 'http://api.infobip.com/');
        $client = new GetSentSmsLogs($configuration);
        $context = new GetSentSmsLogsExecuteContext();
        $context->setMessageId($messageId);
        $context->setTo($to);
        $context->setLimit(5);
        $apiResponse = $client->execute($context);
        return $apiResponse;
    }
}