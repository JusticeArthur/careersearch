<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function profileViews()
    {
        return $this->morphToMany(ProfileView::class,'viewable');
    }
    protected $with = ['company'];
    protected $appends = ['logo'];
    public function skills(){
        return $this->hasMany(Skill::class);
    }
    public function tags(){
        return $this->hasMany(Tag::class);
    }
    public function getLogoAttribute(){
        if(auth()->check()){
            if($this->avatar){
                if(auth()->user()->hasRole('admin'))
                    return asset('avatars/admin/'.$this->avatar);
                return asset('avatars/profile/'.$this->avatar);
            }
        }else{
            if($this->avatar){
                return asset('avatars/profile/'.$this->avatar);
            }
        }

    }
    public function attachments(){
        return $this->hasMany(Attachment::class);
    }
    public function company(){
        return $this->hasOne(Company::class);
    }
   /* protected $hidden = [
        'url',
    ];*/
    protected $fillable = ['introduction','dob','gender','mobile','alt_mobile',
        'location','cv','avatar','rate','nationality','state_id','country_id','lat','lng'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function getOverallRatingAttribute($value){
        if((float) $value <= 1){
            return number_format(1,1);
        }
        return number_format((float) $value,1);
    }
    public function getIntroductionAttribute($value){
        if(strlen($value) > 4){
            return $value;
        }
        return null;
    }
}
