<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSubCategoryModel extends Model
{
    public function itemSubCategory(){
        return $this->belongsTo(ItemSubCategory::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
}
