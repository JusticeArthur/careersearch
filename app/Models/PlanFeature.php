<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanFeature extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name'] = title_case($value);
    }
    public function plan(){
        return $this->belongsTo(Plan::class);
    }
}
