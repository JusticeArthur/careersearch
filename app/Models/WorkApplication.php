<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class WorkApplication extends Model
{
    protected $with = ['user'];
    protected $table = 'work_applications';
    public function user(){
        return $this->belongsTo(User::class);
    }
}
