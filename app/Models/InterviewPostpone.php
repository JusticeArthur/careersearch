<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class InterviewPostpone extends Model
{
    public function interview(){
        return $this->belongsTo(Interview::class);
    }
    public function getPeriodAttribute($value){
        return Carbon::parse($value)->toDayDateTimeString();
    }
}
