<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Plan extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name'] = title_case($value);
    }
    public function getMonthlyAmountAttribute($value){
        return number_format($value,2);
    }
    public function getYearlyAmountAttribute($value){
        return number_format($value,2);
    }
    public function features(){
        return $this->hasMany(PlanFeature::class,'plan_id');
    }
    public function subscriptions(){
        return $this->hasMany(Subscription::class);
    }

}
