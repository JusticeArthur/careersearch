<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkBookmark extends Model
{
    public function work(){
        return $this->belongsTo(Work::class);
    }
}
