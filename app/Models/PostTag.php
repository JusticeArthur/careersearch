<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    public function getNameAttribute($value){
        return title_case($value);
    }
}
