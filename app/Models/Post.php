<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];
    protected $with = ['user','tag'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
    public function tag(){
        return $this->belongsTo(PostTag::class,'post_tag_id');
    }
    public function getPostedOnAttribute(){
        return Carbon::parse($this->created_at)->toFormattedDateString();
    }
    protected $appends = ['posted_on','short_cut'];
    public function getShortCutAttribute(){
        $value = $this->body;
        if(!$value){
            return $value;
        }else{
            $end = '...';
            $max_length = 115;
            if (strlen($value) > $max_length || $value == '') {
                $words = preg_split('/\s/', $value);
                $output = '';
                $i      = 0;
                while (1) {
                    $length = strlen($output)+strlen($words[$i]);
                    if ($length > $max_length) {
                        break;
                    }
                    else {
                        $output .= " " . $words[$i];
                        ++$i;
                    }
                }
                $output .= $end;
            }
            else {
                $output = $value;
            }
            return $output;
        }
    }
    public function getAvatarAttribute($value){
        if(isset($value)){
            return asset('storage/blog/'.$value);
        }else{
            return $value;
        }
    }
    public function getTitleAttribute($value){
        if (isset($value)){
            return title_case($value);
        }
        return $value;
    }
    public function relatedPosts(){
        return $this->hasMany(Post::class,'post_tag_id','post_tag_id')->take(2);
    }

}
