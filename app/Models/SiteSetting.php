<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    protected $guarded = [];
    public function setCityAttribute($value){
        $this->attributes['city'] = title_case($value);
    }
}
