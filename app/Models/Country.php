<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $with = ['states'];
    public function states(){
        return $this->hasMany(State::class)->withCount('jobs');
    }
}
