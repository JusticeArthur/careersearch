<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id')->orderBy('created_at','desc');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getPostedOnAttribute(){
        return Carbon::parse($this->created_at)->toFormattedDateString();
    }
    protected $appends = ['posted_on'];
    protected $with = ['replies','user'];
}
