<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    public function getNameAttribute($value){
        return title_case($value);
    }
    public function getCountryAttribute($value){
        return title_case($value);
    }
    public function getCityAttribute($value){
        return title_case($value);
    }
    public function getTagLineAttribute($value){
        return strtoupper($value);
    }
    public function getLogoAttribute($value){
        if(isset($value))
            return asset('avatars/logos/'.$value);
        return null;
    }
    public function profile(){
        return $this->belongsTo(Profile::class);
    }
    public function getAdminAttribute(){
        return $this->belongsTo(Profile::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function review(){
        return $this->hasMany('App\Models\ReviewCompany');
    }

}
