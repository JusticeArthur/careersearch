<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Nicolaslopezj\Searchable\SearchableTrait;
use Laravel\Scout\Searchable;

class Work extends Model
{
    /*Searchable*/
    use SearchableTrait;
    protected $searchable = [
        'columns' => [
            'works.title' => 10,
        ],

    ];

    protected $with=['tags','requirements','company','applications','user'];
    public function getMinSalaryAttribute($value){
        $slug = Input::get('slug');
        if(isset($slug)){
            return $value;
        }
        $setting  = SiteSetting::where('custom_id',config('app.customId'))->first();
        if(isset($setting)){
            return $setting->currency .' '.number_format($value,2);
        }
        return 'GHS '.number_format($value,2);
    }
    public function getMaxSalaryAttribute($value){
        $slug = Input::get('slug');
        if(isset($slug)){
            return $value;
        }
        $setting  = SiteSetting::where('custom_id',config('app.customId'))->first();
        if(isset($setting)){
            return $setting->currency .' '.number_format($value,2);
        }
        return 'GHS '.number_format($value,2);
    }
    public function tags(){
        return $this->hasMany(WorkTag::class);
    }
    public function requirements(){
        return $this->hasMany(WorkRequirement::class);
    }
    public function company(){
        return $this->belongsTo(Company::class);
    }
    public function applications(){
        return $this->hasMany(WorkApplication::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getTypeAttribute($value){
        $slug = Input::get('slug');
        if(isset($slug)){
            return $value;
        }
        if($value == 'intership'){
            return 'Internship';
        }else{
            return title_case(str_replace('_',' ',$value));
        }
    }
    public function bookmark(){
        return $this->hasOne(WorkBookmark::class);
    }
    public function getBookmarkedAttribute(){
        if(isset($this->bookmark)){
            if($this->bookmark->bookmarked)
                return true;
            return false;
        }
        return false;
    }
    public function getPostedOnAttribute(){
        return Carbon::parse($this->created_at)->diffForHumans();
    }
    protected $appends = ['bookmarked','posted_on'];
}
