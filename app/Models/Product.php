<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait;
    protected $searchable = [
        'columns' => [
            'products.title' => 10,
            'brands.name'=>10,
            'models.name'=>10
        ],
        'joins' => [
            'brands' =>['products.brand_id','brands.id'],
            'models' =>['products.model_id','models.id'],
        ],
    ];
    protected $guarded = [];
    public function category(){
        return $this->belongsTo(ItemCategory::class,'item_category_id');
    }
    public function subCategory(){
        return $this->belongsTo(ItemSubCategory::class,'item_sub_category_id');
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function model(){
        return $this->belongsTo(BrandModel::class,'model_id');
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }
    public function getTitleAttribute($value){
        if(isset($value)){
            if(isset($this->brand)){
                if(!Str::is('other*',strtolower($this->brand->name)))
                return $this->brand->name .' '.title_case($value);
            }
            return title_case($value);
        }else{
            return $this->brand->name .' '.$this->model->name;
        }
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getExpiryAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    public function getPostedOnAttribute(){
        return Carbon::parse($this->created_at)->toDayDateTimeString();
    }
    public function getExpiredAttribute(){
        if(Carbon::now()->gt(Carbon::parse($this->expiry))){
            return true;
        }
        return false;
    }
    public function avatars(){
        return $this->hasMany(ProductAvatar::class);
    }
    protected $with = ['user','avatars'];
    protected $appends = ['posted_on','expired','human_diff','bookmarked'];

    public function getPriceAttribute($value){
        $slug = Input::get('slug');
        if(isset($slug)){
            return $value;
        }
        $setting  = SiteSetting::where('custom_id',config('app.customId'))->first();
        if(isset($setting)){
            return $setting->currency .' '.number_format($value,2);
        }
        return 'GHS '.number_format($value,2);
    }
    public function getHumanDiffAttribute(){
        return Carbon::parse($this->created_at)->diffForHumans();
    }
    public function bookmark(){
        return $this->hasOne(ProductBookmark::class);
    }
    public function getBookmarkedAttribute(){
        if(isset($this->bookmark)){
            if($this->bookmark->bookmarked)
                return true;
            return false;
        }
        return false;
    }
}
