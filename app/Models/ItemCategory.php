<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name'] = title_case($value);
    }

    public function itemSubCategories(){
        return $this->hasMany(ItemSubCategory::class);
    }
}
