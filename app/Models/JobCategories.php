<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class JobCategories extends Model
{
    public function users(){
        return $this->belongsToMany(User::class, 'user_categories', 'job_category_id', 'user_id');
    }
}
