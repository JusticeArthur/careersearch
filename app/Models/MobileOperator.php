<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobileOperator extends Model
{
    public function getNameAttribute($value){
        return strtoupper($value);
    }
    public function codes(){
        return $this->hasMany(MobileOperatorCode::class);
    }
}
