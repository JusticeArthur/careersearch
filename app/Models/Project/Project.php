<?php

namespace App\Models\Project;

use App\User;


use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $with=['tags','requirements','applications','user'];

    public function tags(){
        return $this->hasMany('App\Models\Project\Project_tag','projects_id');
    }
    public function requirements(){
        return $this->hasMany('App\Models\Project\Project_requirement','projects_id');
    }
    public function applications(){
        return $this->hasMany('App\Models\Project\ProjectApplication');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
