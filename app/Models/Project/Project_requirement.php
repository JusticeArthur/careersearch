<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Project_requirement extends Model
{
    //
    public function project(){
        return $this->belongsTo('App\Model\Project\Project');
    }
}
