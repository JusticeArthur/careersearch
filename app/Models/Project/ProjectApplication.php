<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectApplication extends Model
{
    protected $with = ['user']; 
    protected $table = 'project_applications';
    public function user(){
        return $this->belongsTo('App\User');
    }
}
