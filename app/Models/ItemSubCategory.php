<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSubCategory extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name']  = title_case($value);
    }
    protected $with = ['item'];
    public function item(){
        return $this->belongsTo(ItemCategory::class,'item_category_id');
    }
    public function brands(){
        return $this->belongsToMany(Brand::class,'item_sub_category_models');
    }
}
//, $relatedKey = null, $relation = null