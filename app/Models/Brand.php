<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name']  = title_case($value);
    }
    public function models(){
        return $this->hasMany(BrandModel::class);
    }
}
