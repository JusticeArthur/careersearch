<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $with = ['sender','receiver'];
    public function  receiver(){
        return $this->belongsTo(User::class,'receiver_id');
    }
    public function sender(){
        return $this->belongsTo(User::class,'sender_id');
    }
    protected $fillable = ['chat','sender_id','receiver_id','read','read_at'];
    public function getPostedAtAttribute(){
        return Carbon::parse($this->created_at)->diffForHumans();
    }
    protected $appends = ['posted_at'];
}
