<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAvatar extends Model
{
    public function getNameAttribute($value){
        return '/storage/product_avatars/'.$value;
    }
}
