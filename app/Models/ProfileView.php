<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileView extends Model
{
    protected $fillable = ['viewee_id','viewable_id','viewable_type'];
    public function viewable()
    {
        return $this->morphTo();
    }
}
