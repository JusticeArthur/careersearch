<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractAttachment extends Model
{
    public function getNameAttribute($value){
        return asset('storage/contracts/zips/'.$value);
    }
}
