<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name']  = title_case($value);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function itemSubCategory(){
        return $this->belongsTo(ItemSubCategory::class);
    }
    protected $with = ['brand'];
    protected $table = 'models';
}
