<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $with = ['work','user','postpone'];
    public function work(){
        return $this->belongsTo(Work::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getPeriodAttribute(){
        return Carbon::parse($this->time)->toDayDateTimeString();
    }
    public function postpone(){
        return $this->hasOne(InterviewPostpone::class);
    }
    protected $appends = ['period'];
}
