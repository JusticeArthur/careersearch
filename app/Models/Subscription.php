<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use SanderVanHooft\Invoicable\IsInvoicable\IsInvoicableTrait;

class Subscription extends Model
{
    use IsInvoicableTrait;
    protected $fillable = ['user_id','trial_ends_at','end_at','user_id','plan_id','plan_type'];
    protected $hidden = ['id','created_at','updated_at'];
    public function getExpiredAttribute(){
        $now = Carbon::now();
        if(Carbon::parse($this->end_at)->gt($now)){
            return false;
        }
        return true;
    }
    protected $appends = ['expired'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function plan(){
        return $this->belongsTo(Plan::class,'plan_id');
    }
}
