<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNote extends Model
{
    public function getPriorityAttribute($value){
        return (int)$value;
    }
    public function getPrioriAttribute(){
        if($this->priority == 0){
            return 'Low Priority';
        }else if($this->priority == 1){
            return 'Medium Priority';
        }else{
            return 'High Priority';
        }
    }
    protected $appends = ['priori'];
}
