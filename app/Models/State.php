<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $with = ['jobs'];
    public function country(){
        return $this->hasOne(Country::class);
    }
    public function ucountry(){
        return $this->belongsTo(Country::class,'country_id');
    }
    public function jobs(){
        return $this->hasMany(Work::class);
    }
}
