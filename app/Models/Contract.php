<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Contract extends Model
{
    protected $with = ['attachments'];
    public function work(){
        return $this->belongsTo(Work::class);
    }
    public function attachments(){
        return $this->hasOne(ContractAttachment::class);
    }

}
