<?php

namespace App\Mail;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobAddedEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user;
    protected $work;
    protected $auth;
    public function __construct($user,$work, $auth)
    {
        $this->user = $user;
        $this->work = $work;
        $this->auth = $auth;
    }


    public function build()
    {
        return $this->view('emails.JobAddedEmail')
            ->with([
                'name'=>ucfirst($this->user->firstname),
                'slug'=>$this->work->slug,
                'work'=>strtoupper($this->work->title),
                'location'=>strtoupper($this->work->location),
                'auth'=>strtoupper($this->auth)
            ]);
    }
}
