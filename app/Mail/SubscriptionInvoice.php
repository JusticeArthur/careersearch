<?php

namespace App\Mail;

use App\Models\Subscription;
use Barryvdh\DomPDF\PDF;
use Dompdf\Dompdf;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionInvoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $link;
    protected $subscription_id;
    public function __construct($id)
    {
        $this->subscription_id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscription-invoice')->attachData($this->generatePdf(),'subscription.pdf', [
            'mime' => 'application/pdf',
        ]);
    }
    public function generatePdf(){
        $subscription = Subscription::find($this->subscription_id);
        $user = $subscription->user;
        $plan = $subscription->plan;
        $invoice = $subscription->invoices()->first();
        $data = [
            'invoice'=>$invoice,'user'=>$user,'plan'=>$plan,'subscription'=>$subscription
        ];
        $pdf = app('dompdf.wrapper')->loadView('vendor.test1',$data );
        return $pdf->download('invoice.pdf');
    }
}
