<?php

namespace App\Notifications\PostponeInterview;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestPostpone extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($employee,$postpone,$work)
    {
        $this->employee = $employee;
        $this->postpone = $postpone;
        $this->work = $work;
    }
    protected $employee;
    protected $postpone;
    protected $work;
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'employee_name'=>$this->employee->firstname.' '.$this->employee->lastname,
            'employee_id'=>$this->employee->id,
            'period'=>Carbon::parse($this->postpone->period)->toDayDateTimeString(),
            'work'=>$this->work->title
        ];
    }
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'employee_name'=>$this->employee->firstname.' '.$this->employee->lastname,
            'employee_id'=>$this->employee->id,
            'period'=>Carbon::parse($this->postpone->period)->toDayDateTimeString(),
            'work'=>$this->work->title
        ]);
    }
}
