<?php
namespace App\Traits;

use App\Mail\SubscriptionInvoice;
use App\Models\SiteSetting;
use App\Models\Subscription;
use App\Models\SubscriptionTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

trait SubscriptionTrait
{
    public function createSubscription(SubscriptionTransaction $options,$user)
    {
        //dd($user->toArray());
        //first check for previous subscriptions
        $previous = Subscription::where('user_id',$options->user_id)->latest()->first();
        if(isset($previous)){
            $now = Carbon::now();
            if(Carbon::parse($previous->end_at)->gt($now)){
                $expiration = $options->plan_type == 'month'? Carbon::parse($previous->end_at)->addDays(30)->toDateTimeString():Carbon::parse($previous->end_at)->addYears(1)->toDateTimeString();
                $subscription = Subscription::create(
                    [
                        'user_id' => $options->user_id,
                        'plan_id' => $options->plan_id,
                        'plan_type' => $options->plan_type,
                        'end_at'=>$expiration,
                        'trial_ends_at'=>null
                    ]
                );
                //generate invoice
                $invoice = $subscription->invoices()->create([]);
                $invoice = $invoice->addAmountInclTax($options->amount, 'Some description', $options->charges);
                $invoice->receiver_info = $user->name . PHP_EOL . $user->profile->mobile . PHP_EOL . $user->profile->location;
                $invoice->sender_info = 'CareerSearch';
                $invoice->payment_info = 'Momo';
                $invoice->note = 'Thanks for your payment';
                $invoice->save();
                Mail::to($user)->send(new SubscriptionInvoice($subscription->id));
            }
        }else{
                $expiration = $options->plan_type == 'month' ? Carbon::now()->addDays(30)->toDateTimeString() : Carbon::now()->addYears(1)->toDateTimeString();
                $subscription = Subscription::create(
                    [
                        'user_id' => $options->user_id,
                        'plan_id' => $options->plan_id,
                        'plan_type' => $options->plan_type,
                        'end_at'=>$expiration,
                        'trial_ends_at'=>null
                    ]
                );
                //generate invoice
            //generate invoice
            $invoice = $subscription->invoices()->create([]);
            $invoice = $invoice->addAmountInclTax($options->amount, 'Some description', $options->charges);
            $invoice->receiver_info = $user->name . '<br>' . $user->profile->mobile . '<br>' . $user->profile->location;
            $invoice->sender_info = 'CareerSearch';
            $invoice->payment_info = 'Momo';
            $invoice->note = 'Thanks for your payment';
            $invoice->save();
            Mail::to($user)->send(new SubscriptionInvoice($subscription->id));

        }
    }

    public function getCurrentSubscriptionAttribute(){
        $subscription = Subscription::where('user_id',$this->id)->latest()->first();
        if(isset($subscription)){
            return $subscription->toArray();
        }else{
            return false;
        }
    }
    public function getAllSubscriptionsAttribute(){
        return Subscription::with(['plan','invoices'])->where('user_id',$this->id)->orderBy('created_at','desc')->get();
    }
}
