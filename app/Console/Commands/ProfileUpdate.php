<?php

namespace App\Console\Commands;

use App\Models\WorkApplication;
use App\User;
use Illuminate\Console\Command;

class ProfileUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profile:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update the profiles of employees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::withRole('employee')->get();
        foreach ($users as $user){
            //dd($user->reviews->count());
            $total_rating = 0;
            $total_recommendation = 0;
            $counter = 0;
            foreach ($user->reviews as $review){
             //   dd($review->toArray());
                $counter ++;
                $rating = 6 - (int) $review->rating;
                $total_rating += $rating;
                $recommendation = 6 - (int) $review->recommendation;
                $total_recommendation += $recommendation;
            }
            //get job successes
            $applications = WorkApplication::where('user_id',$user->id)->count();
            $success = WorkApplication::where('status','employed')->where('user_id',$user->id)->count();

            if($counter > 0){
                $user->profile->overall_rating = ($total_rating / ($counter * 5)) * 5;
                $user->profile->recommendation = ($total_recommendation / ($counter * 5)) * 100;
                $user->profile->save();
            }
            if($applications > 0){
                $user->profile->job_success = ($success / $applications) * 100;
              /*  dd([
                    'a'=>$success,
                    'b'=>$applications,
                    'c'=>$success,
                ]);*/
                $user->profile->save();
            }


        }
    }
}
