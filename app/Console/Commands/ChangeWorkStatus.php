<?php

namespace App\Console\Commands;

use App\Models\Work;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeWorkStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'work:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'changes the statuses of the posted jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $works = Work::all();
        foreach ($works as $work){
            $expiry = Carbon::parse($work->expiry);
            $expiring = Carbon::parse($work->expiry)->subDays(3);
            if($expiry->lt($now)){
                $work->status = 'expired';
                $work->save();
            }else if($now->between($expiring,$expiry)){
                $work->status = 'expiring';
                $work->save();
                //send email alerts here
            }else{
                $work->status = 'active';
                $work->save();
            }
        }
    }
}
