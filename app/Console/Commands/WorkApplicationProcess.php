<?php

namespace App\Console\Commands;

use App\Models\Contract;
use App\Models\WorkApplication;
use Illuminate\Console\Command;

class WorkApplicationProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'updates a work application process';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contracts = Contract::where('status','accepted')->get();
        foreach ($contracts as $contract){
            $application = WorkApplication::where('work_id',$contract->work_id)
                ->where('user_id',$contract->employee_id)
                ->first();
            if(isset($application)){
                $application->status = 'employed';
                $application->save();
            }
        }
    }
}
