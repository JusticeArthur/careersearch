<?php

namespace App\Console\Commands;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeProductStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'changes the statuses of the posted products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $products = Product::all();
        foreach ($products as $product){
            if($product->approved){
                $expiry = Carbon::parse($product->expiry);
                $expiring = Carbon::parse($product->expiry)->subDays(3);
                if($expiry->lt($now)){
                    $product->status = 'expired';
                    $product->save();
                }else if($now->between($expiring,$expiry)){
                    $product->status = 'expiring';
                    $product->save();
                    //send email alerts here
                }else{
                    $product->status = 'active';
                    $product->save();
                }
            }

        }
    }
}
