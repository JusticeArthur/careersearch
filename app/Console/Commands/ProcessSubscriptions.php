<?php

namespace App\Console\Commands;

use App\Mail\SubscriptionInvoice;
use App\Models\SiteSetting;
use App\Models\SubscriptionTransaction;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ProcessSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'process and update subscriptions transactions in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = SubscriptionTransaction::where('status','pending')->get();
        $client = new Client(
            ['base_uri' => 'https://client.teamcyst.com', 'verify' => false]

        );
        foreach ($subscriptions as $subscription){
            $order = $subscription->order_id;
            try{
                $response =  $client->request('GET','checktransaction.php',
                    [
                        'query'=>[
                            'orderID'=>$order
                        ],
                        'headers' => [
                            'User-Agent' => 'Mozilla/5.0',
                        ]
                    ]
                );
                $status =  json_decode($response->getBody(),true);
                if(array_key_exists('status',$status)){
                    if(strtolower($status['status']) == 'successful'){
                        $user = User::find($subscription->user_id);
                        $user->createSubscription($subscription,$user);
                        $subscription->status = 'success';
                        $subscription->save();
                    }
                }
            } catch (GuzzleException $e) {
                continue;
            }
        }
    }
}
