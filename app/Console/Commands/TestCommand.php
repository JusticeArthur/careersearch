<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Work;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Intervention\Image\Facades\Image;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*$products = Product::all();
        foreach ($products as $product){
            $product->status = 'expired';
            $product->save();
        }*/
        $products = Work::all();
        foreach ($products as $product){
            $product->expiry = Carbon::now()->addDays(20)->toDateString();
            $product->save();
        }
       // $user = User::withRole('admin')->get();
       // dd($user->toArray());

    }
}
