<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('php artisan queue:work --tries=3')->everyMinute()->withoutOverlapping();
        $schedule->command('php artisan product:status')->everyMinute();
        $schedule->command('php artisan work:status')->everyMinute();
        $schedule->command('php artisan subscription:process')->everyMinute();
        $schedule->command('php artisan profile:update')->everyMinute();
        $schedule->command('php artisan application:process')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
