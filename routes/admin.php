<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 06/11/2018
 * Time: 13:22
 * Protect all with middleware pls don't forget
 */
Route::get('/','Admin\HomeController@getLogin')->name('admin.getLogin');
Route::post('/login','Admin\HomeController@adminLogin')->name('admin.Login');
Route::group(['middleware'=>'admin_auth'],function (){
    Route::get('/dashboard','Admin\HomeController@getDashboard')->name('admin.dashboard');
    Route::resources([
        'job-categories'=>'Admin\JobCategory',
        'item-categories'=>'Admin\Item\CategoryController',
        'item-sub-categories'=>'Admin\Item\SubCategoryController',
        'brands'=>'Admin\Brand\BrandController',
        'models'=>'Admin\Brand\ModelController',
        'item-sub-category-model'=>'Admin\Brand\ItemSubCategoryModelController',
    ]);

    Route::prefix('models')->group(function (){
        Route::get('categories/{id}','Admin\Brand\ModelController@getModelCategories');
    });
    //manage users
    Route::group(['prefix'=>'manage-users'],function (){
        Route::resources([
            'plans'=>'Admin\User\UserPlanController',
            'plan-features'=>'Admin\User\UserPlanFeatureController',
            'users'=>'Admin\UsersController',
        ]);
        //block users
        Route::put('block/{id}','Admin\UsersController@blockUser');
    });
    //settings
    Route::group(['prefix'=>'settings'],function (){
        Route::resources([
            'site'=>'Admin\Settings\SiteController',
        ]);
        Route::get('country/{id}','Admin\Settings\SiteController@getStates');
    });
    //manage companies
    Route::group(['prefix'=>'manage-companies'],function(){
        Route::get('verify','Admin\Company\ManageCompanyController@index')->name('manage.companies.verify');
        Route::post('verify','Admin\Company\ManageCompanyController@verify');
    });
    Route::get('/id',function (){
        dd(str_replace('-','',\Illuminate\Support\Str::uuid()));
    });
    Route::resources([
        'features'=>'Admin\Item\FeaturesController',
        'item-features'=>'Admin\Item\ItemFeaturesController'
    ]);
    Route::group(['prefix'=>'products'],function (){
        Route::get('approve','Admin\Item\ProductController@approve')->name('admin.products.approve');
        Route::get('approve/{id}','Admin\Item\ProductController@approveSingle');
        Route::post('approve-bulk','Admin\Item\ProductController@approveBulk');
    });
    //blog posts
    Route::group(['prefix'=>'blog'],function (){
        Route::resources([
            'posts'=>'Admin\Blog\PostController'
        ]);
    });
    Route::group(['prefix'=>'reports'],function (){
        Route::get('/jobs','Admin\ReportController@index')->name('admin.reports.index');
        Route::post('/filter-jobs','Admin\ReportController@filterRecords');
    });

    Route::get('profile','Admin\Profile\ProfileController@index')->name('admin.profile.index');
    Route::post('profile','Admin\Profile\ProfileController@update');
});
Route::get('/check-email',function(){
    return view('emails.company-registered');
});