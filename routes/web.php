<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use GuzzleHttp\Client;

Route::get('/', 'Misc\RegisterController@home')->name('user.home');
//password resets
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
Route::post('/register','Misc\RegisterController@register');
Route::post('/login','Misc\RegisterController@login');
Route::get('/user/verify/{token}', 'Misc\RegisterController@verifyUser');
Route::post('/home-search','Search\SearchController@homeSearch');
Route::get('/search','Search\SearchController@getResults');
Route::post('/search','Search\SearchController@filterResults');
//social login
Route::get('/login/{social}','Misc\RegisterController@socialLogin');
Route::get('/login/{social}/callback','Misc\RegisterController@handleProviderCallback');
Route::group(['middleware'=>'custom_auth'],function (){
    Route::get('/logout','Misc\RegisterController@logout')->name('user.logout');
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('/settings','HomeController@settings')->name('dashboard.settings');
    Route::get('/messages','HomeController@messages')->name('dashboard.messages');
    Route::get('/messages/add/{id}','Misc\ChatController@addChat');
    Route::get('/bookmarks','HomeController@bookmarks')->name('dashboard.bookmarks');
    //reviews
    Route::get('/reviews/{paginate?}','HomeController@reviews')->name('dashboard.reviews');
    Route::get('/reviews-employees/{work_id}','HomeController@loadEmployeeForReview');
    Route::post('/reviews','HomeController@saveReview');
    Route::put('/reviews/{id}','HomeController@updateReview');
    Route::get('/contracts','HomeController@contract')->name('dashboard.contracts');
    //accept / decline
    Route::get('/contracts/accept/{id}','Interview\InterviewController@acceptContract');
    Route::get('/contracts/decline/{id}','Interview\InterviewController@declineContract');
    //dashboard notes
    Route::post('add-user-note','HomeController@addNote');
    Route::put('edit-user-note/{id}','HomeController@updateNote');
    Route::delete('delete-user-note/{id}','HomeController@deleteNote');
    //end of notes
    Route::post('/profile','Profile\UpdateController@update')->name('profile.update');
    //jobs
    Route::group(['middleware'=>'subscription.protection'],function (){
        Route::get('/manage-jobs/{paginate?}','Job\MiscController@manageJobs')->name('manage_jobs.all');
        Route::get('/manage-candidates/{slug}','Job\MiscController@getJobCandidates')->name('jobs.manage_candidates');
        Route::resources([
            'jobs'=>'Job\ManageJobController',
        ]);
        Route::get('/manage-jobs/search/filter','Job\MiscController@search');
    });

    //end of jobs
    Route::get('/find-jobs/{paginate?}','Job\FindController@index')->name('find_jobs.all');
    Route::post('/find-jobs/filter','Job\FindController@filter');
    Route::get('/cv/download/{id}','Job\MiscController@downloadCv')->name('profile.cv.download');
    Route::post('/apply/job','Job\MiscController@applyJob');
    Route::resources([
        'skills'=>'Profile\SkillController',
        'project'=>'Task\ManageTaskController',
        'subscriptions'=>'Subscription\SubscriptionController',
        'reviewjob'=>'Review\ReviewJobController',
        'reviewcompany'=>'Review\ReviewCompany'
    ]);
    Route::get('/jobs/bookmark/{id}','Job\ManageJobController@bookmarkWork');
    //add tags and requirements
    Route::post('add-work-tag','Job\MiscController@addTag');
    Route::delete('remove-work-tag/{id}','Job\MiscController@removeTag');
    Route::post('add-work-requirement','Job\MiscController@addRequirement');
    Route::delete('remove-work-requirement/{id}','Job\MiscController@removeRequirement');
//subscription checkout
    Route::group(['prefix'=>'subscriptions'],function (){
        Route::get('/checkout/{slug}/{type}','Subscription\SubscriptionController@checkout');
        Route::get('/checkout/order/{slug}/{type}','Subscription\SubscriptionController@processOrder');
        Route::get('/checkout/order/invoice/{slug}/{type}','Subscription\SubscriptionController@invoice')
            ->name('subscription.invoice');
        Route::post('/payments','Subscription\SubscriptionController@processPayment');
    });
    Route::get('/jobs/{slug}/apply','Job\MiscController@getApply')->name('jobs_apply.local');

    Route::get('/manage_task','Task\MiscController@managetask')->name('managetask');
    Route::get('/manage/{slug}/bidders','Task\MiscController@managebidders')->name('managebidders');
    Route::get('/active_bids','Task\MiscController@activebids')->name('activebids');
    Route::get('/project/{slug}/apply','Task\MiscController@project_apply')->name('project_apply');
    Route::post('/project/apply','Task\MiscController@apply')->name('apply_project');
    Route::post('/project/accept/application','Task\MiscController@accept_project')->name('accept_project');
    //endpoints
    Route::get('/endpoint/jobs','Misc\EndPointController@exposeJobs');
//user profile
    Route::get('/profile/{slug}','Profile\HomeController@index')->name('profile.user.in');
    //Route::get('/profile/remove-cv','Profile\HomeController@index')->name('profile.user.in');
    Route::get(str_slug('makeAllNotifications'),'Misc\ManageNotification@makeAllNotifications')->name('mark.all.notification');
    Route::get('/find-projects','Task\FindController@index')->name('find_projects.all');
    Route::get('/find-employees','Profile\FindEmployeeController@index')->name('find_employees.all');
    Route::post('/find-projects/filter','Task\FindController@filter');

    Route::get('/manage-candidates/{job_slug}/{applicant_slug}','Job\MiscController@showApplication')
        ->name('candidate.application.details');
    Route::get('/{job_slug}/{applicant_slug}/apply','Job\MiscController@acceptApply')
        ->name('candidate.application.details.apply');
    Route::get('/manage-candidates/{job_slug}/{applicant_slug}/interview','Interview\InterviewController@index')
        ->name('candidate.application.details.interview');
    Route::post('/manage-candidates/interview/save','Interview\InterviewController@store');
    Route::get('/interviews','Applicant\InterviewController@getInterviews')->name('applicants.interview');
    Route::get('/interviews/{id}','Interview\InterviewController@downloadAttachment');
    Route::get('/interviews/accept/{id}','Interview\InterviewController@acceptInterviewInvite');
    Route::get('/interviews/decline/{id}','Interview\InterviewController@declineInterviewInvitation');
    Route::get('/get-user-conversation/{id}','Misc\ChatController@getUserChatsById');
    Route::put('/mark-single-message-read/{id}','Misc\ChatController@markMessageAsRead');
    Route::post('/message-send','Misc\ChatController@send');
    Route::post('/message-add-chat','Misc\ChatController@createChat');
    Route::post('/interviews/send-contract','Interview\InterviewController@sendContract');
    Route::post('/interviews/postpone','Interview\InterviewController@postPoneInterview');
    //interview postpone
    Route::get('/interview-postpones','HomeController@postpone')
        ->name('employee.interview.postpone');
    Route::get('/interview-postpones/accept/{id}','Interview\InterviewController@acceptPostpone');
    Route::get('/interview-postpones/decline/{id}','Interview\InterviewController@declinePostpone');


//products ---->sell products
    Route::prefix('products')->group(function (){
        Route::resources([
            'sell'=>'Product\Sell\ProductController'
        ]);
        Route::get('my-posts/{paginate?}','Product\Sell\ProductController@myposts')->name('products.sell.my_posts');
        Route::prefix('end-point')->group(function (){
            Route::get('item-sub-categories/{id}','Product\Sell\ProductController@getItemSubCategories');
            Route::get('item-sub-categories-brands/{id}','Product\Sell\ProductController@getItemSubBrands');
            Route::get('brand-models/{id}','Product\Sell\ProductController@getBrandModels');
        });
        Route::get('buy','Product\Buy\ProductController@index')->name('products.buy.index');
        Route::get('buy/{slug}','Product\Buy\ProductController@productDetail')->name('product.buy.spec');
        Route::post('buy/filter','Product\Buy\ProductController@filterData');
        Route::post('buy/filter/map','Product\Buy\ProductController@getProductByLocation');
        Route::get('bookmark/{id}','Product\Buy\ProductController@bookmarkProduct');
        //all the following are methods used for updates
        Route::post('add-avatar','Product\Sell\ProductController@addAvatar');
        Route::delete('remove-avatar/{id}','Product\Sell\ProductController@removeAvatar');
        Route::post('add-contact','Product\Sell\ProductController@addContact');
        Route::delete('remove-contact/{id}','Product\Sell\ProductController@removeContact');
    });
//deal with bookmarks
    Route::get('/job/remove-bookmark/{id}','Job\ManageJobController@removeBookmark');

    //deal with all notifications
    Route::group(['prefix'=>'notifications'],function (){
        Route::get('mark/{id}','NotificationController@markSingle');
    });
    Route::get('check-ip',function (\Illuminate\Http\Request $request){
        dd($request->getClientIp());
    });
    Route::get('preview-job-noty',function(){

        return view('emails.JobAddedEmail',[
            'name'=>'Justice',
            'work'=>'front end dev',
            'location'=>'Ayeduase,Ghana',
            'auth'=>'Trinity Software Center'
        ]);
    });
    Route::group(['prefix'=>'companies'],function (){
        Route::get('/','Company\CompanyController@index')->name('companies.all.index');
        Route::get('/{slug}','Company\CompanyController@profile');
    });
    Route::group(['prefix'=>'cs'],function (){
        Route::get('/{slug}','Profile\ProfileController@index')->name('my.profile.index');
        Route::post('/view-count','Profile\ProfileController@countViews');
        Route::post('/get-view-count','Profile\ProfileController@getProfileViewCount');
        Route::post('update-profile-intro','Profile\ProfileController@updateProfileIntroduction');
    });
    Route::get('invoices','HomeController@invoice')->name('invoice.all.details');

    //mark all notifications as read
    Route::post('/mark-all-notifications','NotificationController@markAllAsRead');
});
//dealing with notifications
//blog
Route::group(['prefix'=>'blog'],function (){
    Route::get('/','Blog\BlogController@index')->name('blog.all.index');
    Route::get('/fetch-recents','Blog\BlogController@getRecentPost');
    Route::get('/{slug}','Blog\BlogController@getPostBySlug')->name('blog.post.detail');
    Route::post('/comments','Blog\BlogController@saveComment');
    Route::post('/replys','Blog\BlogController@saveReply');
});

Route::get('/mailable', function () {
    $subscription = \App\Models\Subscription::find(36);
    $user = $subscription->user;
    $plan = $subscription->plan;
    $invoice = $subscription->invoices()->first();
    return view('vendor.test1',
        [
            'invoice'=>$invoice,'user'=>$user,'plan'=>$plan,'subscription'=>$subscription
        ]
    );
});
