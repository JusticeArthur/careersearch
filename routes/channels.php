<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

/*Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});*/

use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('chatConnect.{userId}', function ($user,$userId) {
    return (int) $user->id === (int) $userId;
});

Broadcast::channel('users.{userId}', function ($user,$userId) {
    return (int) $user->id === (int) $userId;
});
Broadcast::channel('messages.{userId}', function ($user,$userId) {
    return (int) $user->id === (int) $userId;
});