<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAcceptColumToInterview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_applications', function (Blueprint $table) {
            $table->boolean('accepted')->default(false);
            $table->dateTime('another_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_applications', function (Blueprint $table) {
            $table->dropColumn('accepted');
            $table->dropColumn('another_time');
        });
    }
}
