<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dob')->nullable();
            $table->unsignedInteger('user_id');
            $table->string('url');
            $table->string('location')->nullable();
            $table->string('mobile')->nullable();
            $table->string('alt_mobile')->nullable();
            $table->enum('gender',['M','F'])->nullable();
            $table->enum('marital_status',['single','married'])->nullable();
            $table->string('cv')->nullable();
            $table->string('avatar')->nullable();
            $table->string('nationality')->nullable();
            $table->string('cover_image')->nullable();
            $table->double('rate')->default(0);
            $table->longText('introduction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
