<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reviewable_type');
            $table->unsignedInteger('reviewable_id');
            $table->unsignedInteger('work_id');
            $table->string('comment')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('recommendation')->nullable();
            $table->unique(['work_id','reviewable_id','reviewable_type']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
