<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('slug');
            $table->double('price')->default(0);
            $table->unsignedInteger('item_category_id')->default(0);
            $table->unsignedInteger('item_sub_category_id')->default(0);
            $table->unsignedInteger('brand_id')->default(0);
            $table->unsignedInteger('country_id')->default(0);
            $table->unsignedInteger('state_id')->default(0);
            $table->unsignedInteger('model_id')->default(0);
            $table->string('location')->nullable();
            $table->string('title')->nullable();
            $table->enum('condition',['new','used']);
            $table->boolean('negotiable')->default(true);
            $table->double('lat')->default(0);
            $table->double('lng')->default(0);
            $table->string('edition')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
