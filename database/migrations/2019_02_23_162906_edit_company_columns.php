<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCompanyColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            if(Schema::hasColumn('companies','country')){
                $table->dropColumn('country');
            }
            if(Schema::hasColumn('companies','category')){
                $table->dropColumn('category');
            }
            if(Schema::hasColumn('companies','city')){
                $table->dropColumn('city');
            }
            $table->unsignedInteger('country_id')->default(0);
            $table->unsignedInteger('state_id')->default(0);
            $table->double('lat')->default(0);
            $table->double('lng')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('country_id');
            $table->dropColumn('state_id');
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}
