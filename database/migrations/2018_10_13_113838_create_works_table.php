<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->enum('type',['full_time','part_time','intership','temporary'])->default('full_time');
            $table->string('slug')->unique();
            $table->longText('location');
            $table->double('min_salary');
            $table->double('max_salary');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('company_id');
            $table->date('expiry');
            $table->string('degree');
            $table->unsignedInteger('job_category_id');
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
