<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalColumnsToSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('models', function (Blueprint $table) {
            $table->unsignedInteger('item_sub_category_id');
            $table->dropUnique(['name','brand_id']);
            $table->unique(['name','brand_id','item_sub_category_id'],'all_unique_constraints');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('models', function (Blueprint $table) {
            $table->dropColumn('item_sub_category_id');
            $table->dropUnique('all_unique_constraints');
        });
    }
}
