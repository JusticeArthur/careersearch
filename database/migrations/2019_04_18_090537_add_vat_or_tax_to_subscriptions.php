<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVatOrTaxToSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_transactions', function (Blueprint $table) {
            $table->double('charges')->default(17.5);
            $table->unsignedInteger('mobile_operator_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_transactions', function (Blueprint $table) {
            $table->dropColumn('charges');
            $table->dropColumn('mobile_operator_id');
        });
    }
}
