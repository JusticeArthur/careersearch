<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interviews', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time');
            $table->longText('description')->nullable();
            $table->unsignedInteger('work_id');
            $table->unsignedInteger('user_id');
            $table->string('location');
            $table->string('attachment')->nullable();
            $table->date('deadline');
            $table->string('contact');
            $table->unique(['work_id','user_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interviews');
    }
}
