<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('custom_id');
            $table->string('contact');
            $table->string('alt_contact');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('state_id');
            $table->string('city')->nullable();
            $table->string('currency');
            $table->string('address')->nullable();
            $table->double('discount')->nullable();
            $table->double('vat')->nullable();
            $table->string('sms_user_name')->nullable();
            $table->string('sms_password')->nullable();
            $table->string('sms_display_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
