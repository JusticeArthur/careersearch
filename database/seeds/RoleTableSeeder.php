<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Role::count() <= 0){
            DB::table('roles')->insert([
                'display_name'=>'Employer',
                'name'=>'employer'
            ]);
            DB::table('roles')->insert([
                'display_name'=>'Employee',
                'name'=>'employee'
            ]);
            DB::table('roles')->insert([
                'display_name'=>'Administrator',
                'name'=>'admin'
            ]);
            DB::table('roles')->insert([
                'display_name'=>'Freelancer',
                'name'=>'freelancer'
            ]);
        }
    }
}
