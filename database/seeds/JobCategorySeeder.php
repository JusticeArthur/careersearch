<?php

use Illuminate\Database\Seeder;

class JobCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Accounting/Auditing','Advertising','Analyst','Art/Creation','Business Development','Consulting',
            'Customer Service','Distribution','Design','Education','Engineering','Finance','General Business',
            'Health Care','Human resource','Information Technology','Legal','Management','Marketing',
            'Public Relations','Purchasing','Product Management','Quality Assurance','Research','Sales','Strategy/Planning',
            'Supply chain','Training','Writing/Editing','Other'
            ];
        if(\App\Models\JobCategories::count() <= 0){
                foreach($categories as $category){
                    $cat = new \App\Models\JobCategories();
                    $cat->name = $category;
                    $cat->save();
                }
        }
    }
}
