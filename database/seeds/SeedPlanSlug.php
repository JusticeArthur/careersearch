<?php

use Illuminate\Database\Seeder;

class SeedPlanSlug extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = \App\Models\Plan::all();
        foreach ($plans as $plan){
            \App\Models\Plan::where('id',$plan->id)
                ->update([
                   'slug'=>str_replace('-','',\Illuminate\Support\Str::uuid())
                ]);
        }
    }
}
