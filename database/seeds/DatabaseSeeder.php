<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            // RoleTableSeeder::class,
            // CountryStateSeeder::class,
             //JobCategorySeeder::class
             //ModelsSeeder::class
            //BrandModelSeeder::class,
             SeedPlanSlug::class
         ]);
    }
}
