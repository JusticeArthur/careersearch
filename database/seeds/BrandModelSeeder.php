<?php

use Illuminate\Database\Seeder;

class BrandModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //$data = file_get_contents(asset('files/modelBrand.json'));
        $data = file_get_contents(asset('files/vehicleModles.json'));
       $data = json_decode($data,true);
       //dd($data);
        $brands = \App\Models\Brand::all();
        //dd($brands->toArray());
       /*foreach ($data as $item){

       }*/
       // echo $data;
        //previous item_sub_category_id = [10]
        foreach ($brands as $brand){
            $name = strtolower($brand->name);
            foreach ($data as $item){
                $test = explode('-',$item['key']);
                if($test[0] == $name){
                  //  dd($item['label']);
                    $model = new \App\Models\BrandModel();
                    $model->name = title_case($item['label']);
                    $model->item_sub_category_id = 18;
                    $model->brand_id = $brand->id;
                    $model->save();
                }
            }
        }
    }
}
