<?php

use Illuminate\Database\Seeder;

class ModelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    ///typo brands seeder instead
    public function run()
    {
        //$data = file_get_contents(asset('files/models.json'));
        $data = file_get_contents(asset('files/vehicleBrands.json'));
       $data =  json_decode($data,true);
        foreach ($data as $item){
            $test = \App\Models\Brand::where('name',$item['label'])->first();
            if(isset($test)){
                continue;
            }else{
                $brand = new \App\Models\Brand();
                $brand->name = $item['label'];
                $brand->save();
            }

        }
    }
}
