<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\JobCategories::class, function (Faker $faker) {
    return [
        'name'=>title_case($faker->sentence(4))
    ];
});
