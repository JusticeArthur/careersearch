<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Work::class, function (Faker $faker) {
    $types = ['full_time','part_time','intership','temporary'];
    $title = $faker->sentence(random_int(1,4));
    return [
        'title'=>title_case($title),
        'type'=>$types[random_int(1,4)-1],
        'description'=>$faker->paragraph(3),
        'slug'=>str_slug($title).'-'.uniqid(),
        'location'=>$faker->city,
        'min_salary'=>$faker->numberBetween(10000,19000),
        'max_salary'=>$faker->numberBetween(20000,40000),
        'user_id'=>10,
        'company_id'=>1,
        'expiry'=>\Carbon\Carbon::now()->addDay(random_int(3,30)),
        'degree'=>$faker->word,
        'job_category_id'=>$faker->numberBetween(1,30),
        'created_at'=>\Carbon\Carbon::now(),
        'updated_at'=>\Carbon\Carbon::now(),
        'country_id'=>69,
        'state_id'=>$faker->numberBetween(1016,1025)
    ];
});
